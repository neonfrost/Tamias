#|
+------------------------------------------------+
|File |						 |
+------------------------------------------------+
|Formatting/options toolbar			 |
+------------------------------------------------+
|Event Timeline|Forces &  | Detailed Description |
|      	       |Leaders   | of the events that   |
|	       |involved  | took place, i.e.     |
|	       |in the ev-| "The Fall of Tarcha -|
|	       |ent       |    In the 3rd era of |
|	       |	  |  peace, the galactic |
|	       |	  |  council sought pow- |
|	       |	  |  er in the uncoloni- |
|	       |	  |  zed regions of the  |
|	       |	  |  galaxy. However..." |
|--------------|----------|----------------------|
|Add event     |Add Leader| ???			 |
+------------------------------------------------+

Event Timeline
+--------------------------------------+
|[+]The First Great Wars               |
|[-]Conflagration of the colonies      |<----CLicking on different 'entries'
| |-The flare of Cyprus                |     causes the sub-events and other things
| |[+]Massacre of the Gekrian          |     to change 
|-Interim Government                   |     The GUI data structure needs to be able to
|[+]The Second Great Wars              |     handle the clicking of the mouse
+--------------------------------------+

Need to implement scrolling bars for this
The Event timeline will need to be a window with a specialized ui-lister in there
Probably tree-viewer gui element

By default, it shows every child element
However, a tree-viewer, with clickable elements, is a lister of buttons
         BUT, that takes up way too much ram IMO, but it does cause some minor issues
         if not done that way
|#

(defun choose-file ()
  ;;;;file-chooser returns a string
  ;;;;open file-chooser
  ;;;;set *current-file* to what's chosen
  )

;;Theres a way to go through each substate right?
;;I want each substate to *display* but not be *interactable*
;;yes, push sub-state
;;So, when save-file, and no active-file-path, then do save-as-timeline
;;save-as-timeline 


(defun save-as-timeline ()
  (states:push-sub-state :save-as)
  ;;this will also affect the GUI state
  ;;;;(choose-file)
  ;;;;on close, (save-timeline)
  ;;;;This will be handled with the GUI element
  ;;;;also: on close, pop-sub-state
  ;;
  )

(defun save-timeline ()
  ;;;;(if (not *current-file*)
  ;;;;    (choose-file))
  ;;;;(print timeline-events file)
  ;;;;(print eras file)
  )

(defun load-timeline ()
  ;;;;(choose-file)
  ;;;;set *current-file* to what's chosen to open
  ;;;;(setf timeline-events (read file))
  ;;;;(setf eras (read file))
  )
