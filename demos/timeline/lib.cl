#|
Name of the program:
Lore Developer

Secondary App:
Lore Viewer
|#

(t.aux:create-id-system timeline)
(define-symbol-macro timeline-events timeline-entries)
(defmacro new-timeline-event (data)
  `(new-timeline ,data)) ;;data = tl-event

(defstruct tl-event
  (name "" :type string)
  (date (vector 0 0 0 0) :type vector)
  (body "" :type string) ;;maybe actually a tamias-variable type?
  (forces-ids nil :type list)
  (forces (make-hash-table) :type hash-table)
  (parent-id -1 :type integer) ;;used for displaying purposes
  (children-ids nil :type list) ;;if children-ids, then add a [+] to the event entry
  (children (make-hash-table)
            :type hash-table)) ;;e.g. |[+]Battle of the Corridor, vs. |-Corridor Incursion

(defvar eras '())

(defstruct era
  (name "New Era")
  (id (length eras)))

(defmacro tl-event-era (tl-event)
  `(aref (tl-event-date ,tl-event) 0))

(defmacro tl-event-day (tl-event)
  `(aref (tl-event-date ,tl-event) 1))

(defmacro tl-event-month (tl-event)
  `(aref (tl-event-date ,tl-event) 2))

(defmacro tl-event-year (tl-event)
  `(aref (tl-event-date ,tl-event) 3))


#|
(defvar eras '("The First Way"
	       "Civil War"
	       "Corruption"
	       "Revolt"
	       "The 1st Vir Incursion"
	       "The Council"
	       "The 2nd Vir Incursion"
	       "Peace"))
|#

(defvar *active-file* nil)

#|
connected-events = (list "event-name" index)
|#

(defmacro defevent (&key (name "") (date (vector 0 0 0 0)) (body ""))
  `(push (make-tl-event :name ,name
			:date ,date
			:body ,body)
	 timeline-events))

(defun change-event (event-index &key name date body)
  (let ((name (or name (tl-event-name (gethash event-index timeline-events))))
	(date (or date (tl-event-date (gethash event-index timeline-events))))
	(body (or body (tl-event-body (gethash event-index timeline-events)))))
    (macrolet ((tl-event () (gethash event-index timeline-events)))
      (setf (tl-event-name tl-event) name
	    (tl-event-date tl-event) date
	    (tl-event-body tl-event) body))))

(defun sort-timeline ()
  (let ((tmp-list nil)
	(sorted-list nil))
    (loop for event in timeline-events
       do (push (list (position event timeline-events)
		      (tl-event-era event)
		      (tl-event-year event)
		      (tl-event-month event)
		      (tl-event-day event)) tmp-list))
    (setf tmp-list (sort tmp-list #'> :key #'fifth))
    (setf tmp-list (sort tmp-list #'> :key #'fourth))
    (setf tmp-list (sort tmp-list #'> :key #'third))
    (setf tmp-list (sort tmp-list #'> :key #'second))
    (loop for event in tmp-list
       do (push (gethash (car event) timeline-events) sorted-list))
    (setf timeline-events sorted-list)))

