(push *default-pathname-defaults* asdf:*central-registry*)
 ;;(:file "sdl2-ql-reqs" :type "cl")
;;(load "sdl2-ql-reqs.cl")
(ql:quickload :sdl2)
(ql:quickload :sdl2-image)
(ql:quickload :sdl2-ttf)
;;(ql:quickload :sdl2-mixer)
(ql:quickload :harmony)
(ql:quickload :cl-vorbis)
(asdf:load-system :tamias-sdl2)
(asdf:load-system :tamias-gui)

(defun test-suite ()
  (asdf:load-system :tests))

(define-symbol-macro cwd (uiop:getcwd))
(define-symbol-macro main (tamias t))
(define-symbol-macro start! (tamias t))


(defun load-project (project)
  (asdf:load-system project))
(defun load-game (game)
  (asdf:load-system game))

;;(defun load-demos ()
;;  (load-project :demos))

;;(defmacro load-demo (demo)
;;  `(let ((demo-name (string-downcase (write-to-string ',demo))))
;;     (load (concatenate 'string "demos/" demo-name ".cl"))))

