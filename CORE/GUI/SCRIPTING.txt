.t-gui file layout:

ui-definitions:
(definitions state sub-state
  (type :user-set-id :keywords keyword-vals)
  (group-type (:user-set-id :keywords keyword-vals)
  	      (:user-set-id :keywords keyword-vals)
	      (:user-set-id :keywords keyword-vals)))

ui-layout: ;sets the x and y of the various elements
(layout state sub-state
  (frame :x :y :width :height
  	 (:direction-optional-(check-if-vertical,v,h-orhorizontal
		:user-id-1 :user-id-2)
  	 (:vertical :user-id-3 :user-id-4))
  ;;hidden (dock ...)