;;ui-text-editor will be a composite structure of a text editor and a ui-window

;; I see a thousand bugs crawling within this structure, consuming everything in their path, until nothing is left, except the hollowed remains of LISP.

(defstruct (ui-editor (:include ui-entry
			    (type :text-editor)
			    (label (make-tamias-string :text ""))
			    (entry (make-text-entry))
			    (click-offset-w 32)
		       (click-offset-h 32)
		       (color tamias.colors:+black+)
		       ))
  current-file
  (text-bg-color tamias.colors:+chalk-white+)
  (text-color tamias.colors:+black+)
  (cursor (make-ui-cursor))
  (text-area (make-array 4))
  (state :idle)
  (sub-state :new-file)
  scroll-h ;;use these to offset the x and y of the text area
  ;;(make-scroll-bar :horizontal? t))
  scroll-v)

(defmacro ui-editor-view-x (ui-editor)
  `(aref (ui-editor-text-area ,ui-editor) 0))

(defmacro ui-editor-view-y (ui-editor)
  `(aref (ui-editor-text-area ,ui-editor) 1))

(defmacro ui-editor-view-width (ui-editor)
  `(aref (ui-editor-text-area ,ui-editor) 2))

(defmacro ui-editor-view-height (ui-editor)
  `(aref (ui-editor-text-area ,ui-editor) 3))

(defstruct (text-editor (:include ui-editor)))

(defmacro ui.make-text-editor (state sub-state
			       (&optional (x 0) (y 0) (w 200) (h 200))
			       (&key (vertical-scroll-bar t)
				  (horizontal-scroll-bar t)
				  (scroll-bar-color tamias.colors:+chalk-white+)
				  (window-color tamias.colors:+dark-pastel-grey+)
				  (text-bg-color tamias.colors:+chalk-white+)
				  (text-color tamias.colors:+black+)
				  (cursor (make-ui-cursor))
				  (text-area (make-array 4))
				  current-file))
  `(let ((scroll-h (if ,horizontal-scroll-bar
	       (make-scroll-bar :horizontal? t :color ,scroll-bar-color)))
	 (scroll-v (if ,vertical-scroll-bar
		       (make-scroll-bar ,scroll-bar-color)))
	 (ui-tedit (make-text-editor :x ,x :y ,y :width ,width :height ,height
				     :color ,window-color)))
     ;;(push-to-end (ui-id scroll-h) (ui-ids window)) (push-to-end (ui-id scroll-v) (ui-ids window))
     
     ))

