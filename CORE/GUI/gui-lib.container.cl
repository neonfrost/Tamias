(defstruct (ui-container (:include ui-element))
  (border-color tamias.colors:+black+)
  (children (make-hash-table)) ;;eventually, we need to get rid of this - continued at: %children
  children-ids
  (pack-offset #(0 0))
  (child-padding #(0 0)))
;;Pack-offset uses the width and height of ui-elements
;;

#|
%children
I think the best way around this is to have a semi-enviornment variable
a global accessed in a package. where ui:add-state and ui:add-sub-state are used internally by the UI subsystem
Then you can avoid having to retype the state and sub-state for 40 ui-elements

This would also make it easier to integrate with scripting files for defining UI elements and layouts
|#

(def-ui-alias ui-ids ui-container-children-ids)
(def-ui-alias ui-children-ids ui-container-children-ids)
(def-ui-alias ui-ids-list ui-container-children-ids)
(def-ui-alias ui-children ui-container-children)
(def-ui-alias ui-padding ui-container-child-padding)

(defmacro ui-padding-x (instance)
  `(aref (ui-padding ,instance) 0))
(defmacro ui-padding-y (instance)
  `(aref (ui-padding ,instance) 1))

(defmacro ui-pack-offset-x (instance)
  `(aref (ui-container-pack-offset ,instance) 0))
(defmacro ui-pack-offset-y (instance)
  `(aref (ui-container-pack-offset ,instance) 1))



#|
(setf (fdefinition 'ui-children-ids) #'ui-container-children-ids)
(setf (fdefinition 'ui-ids) #'ui-container-children-ids)
(setf (fdefinition 'ui-ids-list) #'ui-container-children-ids)
(setf (fdefinition 'ui-children) #'ui-container-children)
|#

(defstruct (ui-console (:include ui-container
			(children nil))))

#|
Spot where window code was
|#


(defstruct (ui-dock (:include
		     ui-container
		     (type :dock))))


(defstruct (ui-frame (:include ui-container
		      (type :frame)
		      (x 0)
		      (x-init 0)
		      (y 0)
		      (y-init 0)
		      (width tamias:screen-width)
		      (width-init tamias:screen-width)
		      (height tamias:screen-height)
		      (height-init tamias:screen-height))))
(def-ui-alias ui-frame-ids ui-frame-children-ids)

  

;;WHile I recognize having everything in one struct isn't the best idea, mainly spatial concerns, I would rather have 10 megabytes of wasted space than have to deal with RSI

(defstruct (ui-menu (:include ui-container
		     (type :menu)))
  (widest 0)
  active? ;;temp code because I'm a dumb slut
  active-item ;;active? is a symbol
  ;;hash table of ui-menu-item(s)
  )
(def-ui-alias ui-menu-ids ui-menu-children-ids)

(defstruct (ui-menu-item (:include ui-button
			  (type :menu-item)
			  (x 0)
			  (y 0)))
  show-child
  child)
(def-ui-alias ui-menu-item-menu ui-menu-item-child)


;;ui-menu-item-action will be a macro that is just (ui-element-special ui-menu-item)

(defstruct (ui-menu-bar (:include ui-menu
			 (type :menu-bar)
			 (x 0)
			 (x-init 0)
			 (y 0)
			 (y-init 0)
			 (width tamias:screen-width)
			 (width-init 'tamias:screen-width)
			 (height (cadr tamias.string:character-size))
			 (height-init '(cadr tamias.string:character-size))
			 (child-padding #(4 4))
			 ))
  hidden);;Symbol
(def-ui-alias ui-menu-bar-ids ui-menu-bar-children-ids)


(defstruct lister-obj
  str
  type
  callback)

(defun default-ui-lister-cb-sym (obj)
  (format t "lister obj str: ~A type: ~A" (lister-obj-str obj) (lister-obj-type obj)))

(defstruct (ui-lister (:include ui-element
				(type :lister)))
  children
  (selector 0)
  (total-objs 0)
  (callback-sym 'default-ui-lister-cb-sym)
  selector-active?
  (selector-color tamias.colors:+pastel-blue+))

(def-ui-alias ui-lister-current ui-lister-selector)
(def-ui-alias ui-lister-child-objects ui-lister-children)

(defun get-current-lister-object (lister)
  (elt (ui-lister-child-objects lister) (ui-lister-current lister)))

(defun ui.add-child (state sub-state child)
  (manager.init? state sub-state)
  (setf (gethash (ui-id ui-element) (ui-manager-collection (gethash sub-state (gethash state ui-managers))))
	child))

(defun container.add-child (container child)
  (aux:push-to-end (ui-id child) (ui-children-ids container))
#|  to be added back in during ui-children fix
(setf (gethash (ui-id ui-element) (ui-manager-collection (gethash sub-state (gethash state ui-managers))))
	child)
|#
  (setf (gethash (ui-id child) (ui-children container)) child))

