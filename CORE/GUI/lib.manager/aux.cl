(defun get-ui-manager (&optional state sub-state create-man?)
  (let ((s-ui (if (or (keywordp state) (symbolp state))
		  (aux:to-keyword state)
		  (if (states:state-p state)
		      (states:state-symbol state)
		      nil)))
	(s-s-ui (if (or (keywordp sub-state) (symbolp sub-state))
		    (aux:to-keyword sub-state)
		    nil)))
    (when (and s-ui s-s-ui)
      (let ((ret-man (gethash s-ui ui-managers)))
	(if ret-man
	    (let ((ret-man (gethash s-s-ui ret-man)))
	      (when (not ret-man)
		(when create-man? (state.init-ui s-ui s-s-ui)))
	      ret-man)
	    (if create-man?
		(state.init-ui s-ui s-s-ui)
		nil))))))

(defmacro UI-MANAGER-MENU-BAR (GUI-MANAGER)
  `(gethash :menu-bar (ui-manager-collection ,gui-manager)))

(defmacro get-current-menu-bar ()
  `(let ((ui-manager (get-ui-manager (states:state-symbol tamias:state) (states:state-sub-state tamias:state))))
     (if ui-manager
	 (gethash :menu-bar (ui-manager-collection ui-manager)))))

(defmacro get-menu-bar (state sub-state)
  `(gethash :menu-bar (ui-manager-collection (get-ui-manager ,state ,sub-state))))

(defmacro get-menu-bar-item (state sub-state item)
  `(gethash ,item (ui-menu-bar-children (get-menu-bar ,state ,sub-state))))

(defmacro get-menu-bar-children (state sub-state)
  `(ui-menu-bar-children (get-menu-bar ,state ,sub-state)))

(defmacro get-menu-children (item)
  `(ui-menu-children ,item))

(defmacro get-menu-item (item parent-item)
  `(gethash ,item (get-menu-children ,parent-item)))

(defmacro get-current-ui-manager (&key create?)
  `(get-ui-manager (states:state-symbol tamias:state) (states:state-sub-state tamias:state) ,create?))

(defmacro ui.current-manager (&key create?)
  `(get-current-ui-manager :create? ,create?))

(defmacro get-current-hover ()
  `(ui-manager-current-hover (get-current-ui-manager)))

(define-symbol-macro ui.current-hover (get-current-hover))

(defmacro ui.re-init (id state sub-state)
  `(symbol-macrolet ((el (get-ui-el ,id ,state ,sub-state )))
     (setf (ui-x el) (eval (ui-x-init el))
	   (ui-y el) (eval (ui-y-init el))
	   (ui-width el) (eval (ui-width-init el))
	   (ui-height el) (eval (ui-height-init el)))))
