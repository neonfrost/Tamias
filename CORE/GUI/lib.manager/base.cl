(t.utils:define-id-system ui-manager)
(defun ui.get-ui-manager (&optional (state (tamias:state-symbol tamias:state))
			    (sub-state (tamias:state-sub-state tamias:state)))
  (get-ui-manager (t.aux:to-keyword state) (t.utils:to-keyword sub-state)))

;;(defvar ui.manager-state :top)
		  
(defun ui.new-ui-manager (manager &optional
				    (state (tamias:state-symbol tamias:state))
				    (sub-state (tamias:state-sub-state tamias:state)))
  (new-ui-manager manager (t.utils:to-keyword (combine-strings state "-" sub-state))))

(defvar ui-managers-list nil)

(defstruct ui-manager
  (collection (make-hash-table) :type hash-table)
  (collection-ids nil :type list)
  (current-hover -1 :type integer)
  (current-active -1));;waffles between a ui-element and -1 

(def-ui-alias ui-collection-ids ui-manager-collection-ids)
(def-ui-alias ui-manager-ids ui-manager-collection-ids)
(def-ui-alias ui-mng-ids ui-manager-collection-ids)

;;we could always be completely insane and make the UI-manager a child of ui-container
;;                                                                              :O)
;;I'm actually kind of considering this


(defvar ui-managers (make-hash-table))

(define-enum (ui-top-left 0) (ui-top-right 1)
  (ui-center 2) (ui-bottom-left 3)
  (ui-bottom-right 4))


#|
(defmacro def-ui-fun (rep-fun fun-name)
  `(defun ,rep-fun (element)
     (,fun-name `,element)))
|#

(defmacro eval-ui-macro-string (eval-str)
  `(eval (read (make-string-input-stream ,eval-str))))


(defmacro get-ui-el (id &optional (state tamias:state) (sub-state (state-sub-state tamias:state)))
  `(gethash ,id (ui-manager-collection
		 (gethash (aux:if-type (,sub-state keyword)
				       ,sub-state
				       (aux:to-keyword ',sub-state))
			  (gethash (aux:if-type (,state state)
						(state-symbol ,state)
						(aux:to-keyword ',state))
				   ui-managers)))))

(defmacro get-ui-element (id state sub-state)
  `(get-ui-el ,id ,state ,sub-state))


(defmacro get-ui-element.current-state (id)
  `(gethash ,id (ui-manager-collection
		 (gethash (aux:to-keyword
			   (states:state-sub-state tamias:state))
			  (gethash
			   (states:state-symbol tamias:state)
			   ui-managers)))))
