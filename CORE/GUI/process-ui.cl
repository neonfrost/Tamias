(defmacro ui.active-ui ()
  `(ui-manager-current-active (ui.current-manager)))

(define-symbol-macro active-ui-element (ui.active-ui))
(define-symbol-macro current-active-element (ui.active-ui))
;;(defvar active-ui-element nil)
(defun ui.set-active (ui-e)
  (when (ui.current-manager)
    (setf active-ui-element ui-e)))
;;this is some stupid code lol

(defvar ui.auxiliary-render-list nil)

(defun ui.render-elements ()
  (let ((cur-manager (ui.current-manager)))
    (when cur-manager
      (let* ((ui-collection-ids (remove :menu-bar (ui-manager-collection-ids cur-manager))))
	(loop for id in ui-collection-ids
	      do (let ((ui-element (gethash id (ui-manager-collection cur-manager))))
		   (ui.render ui-element
			      (ui-type ui-element)
			      ui.current-hover)))
	(let ((ui-element (gethash :menu-bar (ui-manager-collection
					      cur-manager))))
	  ;;  (gethash sub-state (gethash state ui-managers))))))
	  (when ui-element
	    (ui.render ui-element
		       (ui-type ui-element)
		       ui.current-hover))))
      ;;the fuck is the point of this? Activating children and keeping their
      ;;parents open will be difficult, but this is duct tape code
      ;;There is too much load-bearing duct tape in my code base >---<
      (when ui.auxiliary-render-list
	(loop :for id :in ui.auxiliary-render-list
	      :do (let ((ui-element (gethash id (ui-manager-collection cur-manager))))
		    (ui.render ui-element (ui-type ui-element)
			       ui.current-hover)))))))
		    
  ;;(render:text (write-to-string (ui-manager-current-hover (get-current-ui-manager)))
;;	       40 10)


(defmacro ui.color-check (el)
  `(when (length< (ui-color ,el) 3)
     (setf (ui-color ,el) (vector (elt (ui-color ,el) 0)
				      (elt (ui-color ,el) 1)
				      (elt (ui-color ,el) 2)
				      255))))

(defun gui:state.init-gui (state sub-state &optional ui-elements)
  (aux:push-to-end (list state sub-state) ui-managers-list)
  (unless
      (gethash state ui-managers)
    (setf (gethash state ui-managers) (make-hash-table)))
  (unless
      (gethash sub-state (gethash state ui-managers))
    (setf (gethash sub-state
		   (gethash state ui-managers))
	  (make-ui-manager))) ;;accidentally made it a hash-table and not a ui-manager
  (let ((cur-man
	  (gethash sub-state
		   (gethash state ui-managers))))
    (when ui-elements
	(loop for element in ui-elements
	      do (push (ui-id element)
		       (ui-manager-collection-ids
			cur-man))
		 (setf (gethash (ui-id element)
				(ui-manager-collection
				 cur-man))
		       element)))))

(defmacro state.init-ui (state sub-state &rest ui-elements)
  `(gui:state.init-gui (aux:to-keyword ',state) (aux:to-keyword ',sub-state) ',ui-elements))
(defun state.init-gui (state sub-state &rest ui-elements)
  (state.init-ui state sub-state ui-elements))

;;      (setf (ui-manager-collection (gethash sub-state (gethash state ui-managers))) ui-elements)))

     
(defmacro ui-element.add (state sub-state ui-el)
  `(progn (push (ui-id ,ui-el)
		(ui-manager-collection-ids (get-ui-manager (aux:to-keyword ,state) (aux:to-keyword ,sub-state))))
	  (setf (gethash (ui-id ,ui-el)
			 (ui-manager-collection
			  (get-ui-manager (aux:to-keyword ,state) (aux:to-keyword ,sub-state))))
		,ui-el)))

(defmacro ui-element.add-child (state sub-state ui-el)
  `(setf (gethash (ui-id ,ui-el)
		  (ui-manager-collection
		   (get-ui-manager (aux:to-keyword ,state) (aux:to-keyword ,sub-state))))
	 ,ui-el))

#|
(defun manager.init? (state sub-state)
  (if (gethash state ui-managers)
      (state.init-ui state sub-state)
      (if (not (gethash sub-state (gethash state ui-managers)))
(state.init-ui state sub-state))))|#
(defun manager.init? (state sub-state)
  (state.init-ui state sub-state))

(defmacro ui.add-to-manager (state sub-state ui-element)
  `(progn (manager.init? ,state ,sub-state)
	  (setf (gethash
		 (ui-id ,ui-element) (ui-manager-collection
				      (gethash (aux:to-keyword ,sub-state) (gethash (aux:to-keyword ,state) ui-managers))))
		,ui-element)))


(defun err-str (elements)
  (format nil "~{~A ~}" elements))
;;While imperfect, this is more readable

(defmacro ui-error-check (element &rest err-str)
  `(unless (ui-p ,element)
       (error (err-str ',err-str))))	   
