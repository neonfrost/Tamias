;;:up and :down based on visual up and down
(defun remove-buffer-on-quit (ui-element)
  (let ((id (ui-id ui-element))
	(state (tamias:state-symbol tamias:state))
	(sub-state (tamias:state-sub-state tamias:state)))
    (eval `(push-quit (render:remove-buffer (tamias-string-buffer (ui-label (get-ui-element ,id ,state ,sub-state))))))))

(defmacro ui.destroy-el (state sub-state id)
  `(setf (ui-manager-ids (get-ui-manager ',state ',sub-state))
	 (remove ,id (ui-manager-ids (get-ui-manager ',state ',sub-state)))
	 (get-ui-element ,id ,state ,sub-state) nil))

(defun add-state-ui-element (state sub-state ui-element &key in-container in-window in-frame)
  (manager.init? state sub-state)
  (if (or (not in-container) (not in-window) (not in-frame))
      (push (ui-id ui-element)
	    (ui-manager-collection-ids (gethash sub-state (gethash state ui-managers)))))
  (setf (gethash (ui-id ui-element) (ui-manager-collection (gethash sub-state (gethash state ui-managers))))
	ui-element)
  ;;  (ui.color-check (gethash (ui-id ui-element) (ui-manager-collection (gethash sub-state (gethash state ui-managers)))))
  )

(defmacro ui-element.make (state sub-state (type x y w h &key label entry constructor color (id (incf current-ui-id))))
  ;;Need to add in 'attach-value-to-variable'
  ;;It would make it so that *spider-string* could be attached to a text-box, spin-box, etc.
  `(let ((constructor-string 
	  (concatenate 'string
		       "make-ui-"
		       (remove #\:
			       (write-to-string
				(or ,constructor
				    ,type)))))
	(color ,color)
	(element nil))
    (when (not (listp ,color))
      (setf color (eval ,color)))
    (setf element 
	  (funcall
	   (aux:read-string constructor-string)
	   :type ,type
	   :x ,x :y ,y :width ,w :height ,h
	   :label ,label :color color :entry ,entry
	   :id ,id))
    (push (ui-id element) (ui-manager-collection-ids (get-ui-manager ,state ,sub-state)))
    (setf
     (gethash (ui-id element)
	      (ui-manager-collection
	       (get-ui-manager ,state ,sub-state)))
     element)))


(defmacro ui-element.define (state sub-state (type x y w h &key action custom label entry constructor color))
  ;;This should be for a user defining their own ui-element, not whatever the fuck this is
  ;;ui-element.make is supposed to do this shit
  ;;This is supposed to be a wrapper around defstruct
  `(let ((element nil)
	 (color ,color)
	 (constructor-string
	   (concatenate 'string
			"make-ui-"
			(remove #\:
				(write-to-string
				 (or ,constructor
				     ,type))))))
     (when (not (listp color))
       (setf color (eval color)))
     (setf element (funcall (aux:read-string constructor-string)
			       :type ,type
			       :x ,x :x-init ',x
			       :y ,y :y-init ',y
			       :width ,w :width-init ',w
			       :height ,h :height-init ',h
			       :action ,action :custom ,custom
			       :label ,label :entry ,entry
			       :color color))
     (push (ui-id element) (ui-manager-collection-ids (get-ui-manager ,state ,sub-state)))
     (setf (gethash (ui-id element)  (ui-manager-collection (get-ui-manager ,state ,sub-state))) element)))


(defmacro ui.add-scroll-bar (state sub-state scroll-bar)
  `(ui.add-to-manager ,state ,sub-state ,scroll-bar))

(defmacro ui-manager.add-element (state sub-state ui-element)
  `(progn (manager.init? ,state ,sub-state)
	  (push (ui-id ,ui-element) (ui-manager-collection-ids (gethash ,sub-state (gethash ,state ui-managers))))
	  (setf (gethash (ui-id ,ui-element)
			 (ui-manager-collection (gethash ,sub-state
							 (gethash ,state ui-managers))))
		,ui-element)))

(setf (fdefinition 'ui.add-element) #'add-state-ui-element)


(defmacro ui-manager.define-element (state sub-state (type x y w h &key action special label entry))
  `(add-state-ui-element ,state ,sub-state (make-ui-element :type ,type
							  :x ,x :x-init ',x
							  :y ,y :y-init ',y
							  :width ,w :width-init ',w
							  :height ,h :height-init ',h
							  :action ,action :special ,special
							  :label ,label :entry ,entry)))


(defun add-state-ui-elements (state sub-state &rest ui-elements)
  (loop for ui-element in ui-elements
	do (add-state-ui-element state sub-state ui-element)))

(setf (fdefinition 'ui.add-elements) #'add-state-ui-elements)

(defun menu.set-width (menu)
  (let ((highest 0))
    (loop for id in (ui-menu-ids menu)
       do (setf id (write-to-string id))
	  (when (> (length id) highest)
	    (setf highest (length id))))
    (setf (ui-menu-width menu) (* (car tamias.string:character-size) highest))))

(defun menu.recurssive-width (menu)
  (menu.set-width menu)
  (when (ui-menu-active-item menu)
    (loop for id in (ui-menu-ids menu)
	  do (let ((menu-item (gethash id (ui-menu-children menu))))
	       (menu.recurssive-width menu-item)))))


(defmacro ui.make-label (x y label &key hidden width height
				     (use-buffer t) scale-text
				     text-color
				     color)
  `(let* ((w (or ,width (* (length ,label) (car tamias.string:character-size))))
	  (h (or ,height (cadr tamias.string:character-size))))
     (make-ui-label :x ,x :x-init ',x
		    :y ,y :y-init ',y
		    :width w :width-init (or ',width (list * (length ,label) (car tamias.string:character-size)))
		    :height h :height-init (or ',height '(cadr tamias.string:character-size))
		    :hidden ,hidden :use-buffer ,use-buffer
		    :scale-text ,scale-text :color ,color
		    :text-color ,text-color)))
  
  

(defmacro ui.add-label (state sub-state x y label &key hidden width height
						    (use-buffer t) scale-text
						    (text-color tamias.colors:+white+)
						    (color tamias.colors:+black+))
  `(let ((t-col ',text-color)
	 (ui-col ',color))
     (let ((ui-el (ui.make-label ,x ,y ,label
				 :hidden ,hidden :width ,width :height ,height
				 :use-buffer ,use-buffer :scale-text ,scale-text
				 :text-color t-col :color ui-col)))
       (setf (tamias-string-text (ui-label-label ui-el)) ,label)
       (ui-element.add ',state ',sub-state ui-el)
       )))

(defmacro ui.child-label (state sub-state x y label
			  &key width height
			    scale-text
			    use-buffer
			    (text-color tamias.colors:+white+)
			    (color (tamias.colors:offset tamias.colors:+black+)))
  `(let* ((t-col ',text-color)
	  (ui-col ',color)
	  (ui-el (ui.make-label ,x ,y ,label
				:width ,width :height ,height
				:scale-text ,scale-text
				:use-buffer ,use-buffer
				:text-color t-col :color ui-col)))
     (setf (tamias-string-text (ui-label ui-el)) ,label)
     (ui.add-to-manager ',state ',sub-state ui-el)
     ui-el))


(defmacro ui.add-spin-box (state sub-state (x y w h &key (color '(95 95 95 255)) action))
  `(add-state-ui-element ',state ',sub-state
			 (:spin-box ,x ,y ,w ,h :action ,action :custom (make-number-entry) :label "0" :color ',color)))


#|

Example:
(get-menu-bar-sub-items (get-menu-bar-item '3d-editor 'sculpt 'file))
(get-menu-bar-sub-item 'new (get-menu-bar-item '3d-editor 'sculpt 'file))

|#
(defmacro ui.add-lister (state sub-state (x y w h &optional (bg-color (tamias.colors:offset tamias.colors:+black+)))
			 (&rest children-strings))
  `(let ((lister-objs nil)
	 (ui-lister (make-ui-lister :x ,x :x-init ',x
				    :y ,y :y-init ',y
				    
				    :width ,w :width-init ',w
				    :height ,h :height-init ',h)))
     (loop for child-obj in ',children-strings
	do (if (listp child-obj)
	       (if (length= child-obj 3)
		   (push (make-lister-obj
			  :str (car child-obj)
			  :type (cadr child-obj)
			  :callback (caddr child-obj)) lister-objs)
		   (push (make-lister-obj
			  :str (car child-obj)
			  :type (cadr child-obj)) lister-objs))
	       (push (make-lister-obj :str child-obj) lister-objs)))
     (setf (ui-lister-child-objects ui-lister) (reverse lister-objs)
	   (ui-lister-total-objs ui-lister) (length lister-objs))
     (ui-element.add ',state ',sub-state ui-lister)))

