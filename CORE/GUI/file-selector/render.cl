(defun ui.render-fs-dir-str (files x y width &optional (additional-str ""))
  (loop :for file-str :in files
     :for n :below (length files)
     :for y-acc :below (* (length files) (+ char-height 2)) by (+ char-height 2)
     do (let ((rend-str (concatenate 'string additional-str file-str))
	      (max-len (floor (/ width 16))))
	  (if (> (length rend-str) max-len)
	      (ui.render-text (subseq rend-str 0 max-len)
			      x (+ y y-acc))
	      (ui.render-text rend-str
			      x (+ y y-acc))))))

(defmethod ui.render (ui-element (ui-type (eql :file-selector)) &optional hover (x-offset 0) (y-offset 0))
  (declare (ignore hover))
  (let ((x (+ x-offset (ui-base-x ui-element)))
	(y (+ y-offset (ui-base-y ui-element)))
	(selector (ui-lister-current ui-element))
	(files (ui-fs-files ui-element))
	(subdirectories (ui-fs-subdirectories ui-element))
	(active? (ui-lister-selector-active? ui-element)))
    (let ((ui-fs ui-element))
      (ui.render-lister-bg ui-fs x y)
      (if active?
	  (render.hover-bg x (+ y (* selector (+ char-height 2)))
			   (ui-width ui-element)
			   char-height
			   (tamias.colors:offset (ui-lister-selector-color ui-fs) 80)))
      (ui.render-fs-dir-str subdirectories x y
			    (ui-width ui-element)
			    "/")
      (ui.render-fs-dir-str files x (+ y (* (length subdirectories) (+ char-height 2)))
			    (ui-width ui-element)))))
