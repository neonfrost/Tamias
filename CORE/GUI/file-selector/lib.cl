(defstruct (ui-file-selector (:include ui-lister
			    (type :file-selector)))
  (icons '("<" ">" "^"))
  subdirectories
  files
  back-history
  forward-history
  (current-path (uiop:getcwd))
  path-str
  current-file ;;pathname
  (current-file-contents ""))

(defstruct (ui-fs (:include ui-file-selector))
  )

;;(uiop:getcwd)
;;(uiop:directory-files)
;;(pathname-directory #P"pathname")

(defun path-string (pathname)
  (namestring pathname))

(defmacro ui-fs.get-directories (ui-fs)
  `(uiop:subdirectories (ui-fs-current-path ,ui-fs)))

(defmacro ui-fs.get-files (ui-fs)
  `(uiop:directory-files (ui-fs-current-path ,ui-fs)))
 
(defmacro remove-dir-pathname (files)
  `(map-into ,files
   #'(lambda (file)
       (let ((path-str (path-string file)))
	 (setf file
	       (subseq path-str
		       (1+ (position #\/ path-str
				     :from-end t :end (1- (length path-str))))
		       (length path-str)
		       ))))
   ,files))

(defmacro remove-dir-string ()
  )
(defmacro remove-dir (files directory)
  `(if (pathnamep (car ,files))
       (remove-dir-pathname files)
       (map-into ,files  #'(lambda (file) (remove (concatenate 'string ,directory "/") file))
		 ,files)))

(defmacro ui-fs.change-path (ui-fs path)
  `(setf (ui-fs-current-path ,ui-fs) ,path ;;pathname
	 (ui-fs-path-str ,ui-fs) (path-string (ui-fs-current-path ,ui-fs))
	 (ui-fs-children ,ui-fs) (append '(#P"../") (ui-fs.get-directories ,ui-fs) (ui-fs.get-files ,ui-fs))
	 (ui-fs-subdirectories ,ui-fs) (append '("../") (remove-dir-pathname (ui-fs.get-directories ,ui-fs)))
	 (ui-fs-files ,ui-fs) (remove-dir-pathname (ui-fs.get-files ,ui-fs))))

;;(subseq current-path (position #\/ :from-end t))

(defmacro ui-fs.up-path (ui-fs)
  `(ui-fs.change-path ,ui-fs (uiop:pathname-parent-directory-pathname (ui-fs-current-path ,ui-fs)))
  )

(defmacro ui-fs.set-file (ui-fs)
  `(setf (ui-fs-current-file ,ui-fs)
	 (elt (ui-fs-children ,ui-fs) (ui-fs-current ,ui-fs))))

#|
(defun ui-fs.check-file (file)
  (let ((file-ext nil)
	(valid-extensions '("txt" "cl" "lisp" "asd" ""))
	(ret-val nil))
    (if (find #\. file :from-end t :start (position #\/ file :from-end t))
	(progn (setf file-ext (subseq file (1+ (position #\. file :from-end t))))
	       (loop :for ext :in valid-extensions
		     do (if (string-equal ext file-ext)
			    (return (setf ret-val t)))))
	(setf ret-val t))
    ret-val))
|#
(defmacro case.string (test-key &rest forms)
  (let ((test-clauses
	 (loop :for (strs . fn) :in forms
            collect `((if (listp ',strs)
			  (loop :for str in ',strs
			     do (if (string-equal ,test-key str)
				    (return t)))
			  (if (not (stringp ',strs))
			      (if (eq 'otherwise ',strs)
				  't)
			      (string-equal ,test-key ',strs))) ,@fn))))
    `(cond ,@test-clauses)
    ))
#|
Adapted from https://stackoverflow.com/a/32605920
|#

(defun ui-fs.check-extension (file)
  (let ((file-ext nil))
    (if (find #\. file :from-end t :start (position #\/ file :from-end t))
	(setf file-ext (subseq file (1+ (position #\. file :from-end t))))
	(setf file-ext ""))
    file-ext))

(defmacro ui-fs.write-string (ui-fs obj)
  `(let ((str-obj (if (stringp ,obj)
		      ,obj
		      (write-to-string ,obj))))
     (setf (ui-fs-current-file-contents ,ui-fs)
	   (concatenate 'string (ui-fs-current-file-contents ,ui-fs)
			str-obj))))

(defmacro ui-fs.write-text-from-file (ui-fs file)
  `(with-open-file (input ,file)
     (setf (ui-fs-current-file-contents ,ui-fs) "") 
     (loop for ln = (read-line input nil nil)
	while ln
	do (ui-fs.write-string ,ui-fs ln))))
	  
;;	  (setf (ui-fs-current-file-contents ,ui-fs)
;;              (concatenate 'string (ui-fs-current-file-contents ,ui-fs) ln)))))

(defmacro ui-fs.open-file (ui-fs)
  `(let ((file nil))
     (ui-fs.set-file ,ui-fs)
     (setf file (namestring (ui-fs-current-file ,ui-fs)))
     (case.string
      (ui-fs.check-extension file)
      (("txt" "cl" "lisp" "asd" "") (ui-fs.write-text-from-file ,ui-fs file))
      (("mp3" "ogg") (setf (ui-fs-current-file-contents ,ui-fs) "") (ui-fs.write-string ,ui-fs "This is a music file."))
      (otherwise (catch-value (ui-fs.check-extension file)))
      )))



(defmacro create-ui-file-selector (state sub-state (&key x y width height))
  `(let ((ui-fs (make-ui-fs :x ,x :y ,y :width ,width :height ,height))
	 (path (uiop:getcwd)))
     (ui-fs.change-path ui-fs path)
     (add-ui-to-manager ,state ,sub-state ui-fs)
     ui-fs))
     
;;create-ui-fs will actually create a window with a file-selector in it in the future
;;for now, it'll just be a hard link to create-ui-file-selector
(defmacro create-ui-fs (state sub-state (&key x y width height))
  `(create-ui-file-selector ,state ,sub-state (:x ,x :y ,y :width ,width :height ,height))
  ;;(ui-element.make ,state ,sub-state (:file-selector ,x ,y ,width ,height :constructor :fs))
  )

(defmacro ui.add-file-selector (state sub-state (&key x y width height))
  `(create-ui-fs ,state ,sub-state (:x ,x :y ,y :width ,width :height ,height)))

