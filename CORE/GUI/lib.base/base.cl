(defvar current-ui-id -1)
(defvar ui.command-stack nil)

(defmacro def-ui-alias (alias-fun fun-name)
  `(defmacro ,alias-fun (instance)
     `(,',fun-name ,instance)))
;;Thank god for old code
;;readapted from CORE/engine/logic/old/rpg.cl
#|
  `(let ((eval-str ""))
     (setf eval-str (concatenate 'string "(defmacro " (write-to-string ',alias-fun)
				 " (instance) `(" (write-to-string ',fun-name) " ,instance))"))
     (eval (read (make-string-input-stream eval-str)))))
|#
#|
Big note, unsure if exists already elsewhere:
Development of GUI boils down to, eventually, creating a window manager inside an application
Yes, you end up building some kind of implementation of i3, xfce, stumpwm, etc. inside of the GUI lib
|#

(defstruct ui
  (type :nil :type keyword)
  (ID (incf current-ui-id))
  (x 0 :type integer)
  x-init
  (y 0 :type integer)
  y-init
  (width 0 :type integer)
  width-init
  (height 0 :type integer)
  height-init
  click-offset-w
  click-offset-h
  (color (tamias.colors:offset tamias.colors:+black+))
  image
  image-path
  label
  use-buffer
  buff-func
  scrollable
  scale-text
  (origin ui-top-left)
  parent ;;useful for tabbing
  (mode :released :type keyword))

(def-ui-alias ui-state ui-mode)



(defstruct (ui-base (:include ui)))



(defstruct (ui-element (:include ui-base))
  action ;;stuff to do when element is clicked or otherwise activated, like (emit-message "button clicked!")
  custom ;;generic special (struct) slot, used for anything that's not an entry struct
					;in particaular: spin box values
  entry
  tool-tip ;tamias-string or ui-label, both would be interesting
  movable
  (timer (timer:make :in-seconds? t))) ;Timers may become arrays in the future, so don't :type timer just yet

(def-ui-alias ui-element-callback ui-element-action)
(def-ui-alias ui-callback ui-element-action)
(def-ui-alias ui-activate! ui-element-action)
(def-ui-alias ui-activate-function ui-element-action)


(defstruct number-entry
  (value (make-tamias-variable :type 'int :value 0) :type tamias-variable)
  (maximum 1 :type integer)
  (minimum 0 :type integer))


(defstruct (ui-button (:include ui-element
		       (type :button)
		       (x 0)
		       (y 0)
		       (x-init 0)
		       (y-init 0)
		       (width 32)
		       (width-init 32)
		       (height 32)
		       (height-init 32)
		       (color '(0 0 0 255))))
  ;;0 = released, 1 = pressed, 2 = hover
  )

(setf (fdefinition 'ui.make-button) #'make-ui-button)




(defstruct (ui-scroll-bar (:include ui-element
			   (type :scroll-bar)
			   (x 0)
			   (y 0)
			   (width 32)
			   (height 32)
			   (action 'scroll)
			   (scrollable t)
			   (color tamias.colors:+chalk-white+)))
  (incrementor 1) ;;how much the window goes up or down when the up/down arrows are clicked
  (max 1)
  (current-offset 0)
  (placement :r) ;;right/bottom (default), left/top
  (orientation :vertical) ;;vertical (default),  horizontal
  hidden?)
(def-ui-alias scroll-offset ui-scroll-bar-current-offset)

(defstruct (scroll-bar (:include ui-scroll-bar)))

(defstruct (ui-spin-box (:include ui-element
				  (type :spin-box)
				  (custom (make-number-entry))))
  (min-value 0 :type integer)
  (max-value 10 :type integer)
  (incrementer 1))


(defstruct (text-entry (:include tamias-text
			(edited t)))
  (display t))

(defstruct (ui-cursor (:include ui-element
		       (type :cursor)))
  (max-time 30)
  blinking?)

(defstruct (ui-text (:include ui-element
		     (type :entry)
		     (label (make-tamias-string :text ""))
		     (entry (make-text-entry))
		     (click-offset-w 32)
		     (click-offset-h 32)
		     ))
  (cursor (make-ui-cursor)))

(defstruct (ui-entry (:include ui-text)))

;;MASSIVE NOTE

;;on creation, ui-text/entry needs to have
;;well, no longer massive note
;;...needs to have the click-offset-h/w set to whatever would be apporopite lol
;;fuck me

"Labels are meant to be one-line, non-editable (client side), 'text fields'"
(defstruct (ui-label (:include ui-base
		      (type :label)
		      (label (make-tamias-string :text "Label"))))
  (text-color tamias.colors:+white+)
  hidden)

"Text Fields are meant to be multi-line 'labels'"
(defstruct (ui-text-field (:include ui-label
				    (type :text-field))))

;;(defstruct node-label)

(defstruct (ui-node (:include ui-element
			      (type :node)
			      (x 0)
			      (x-init 0)
			      (y 0)
			      (y-init 0)
			      (width (* 16 32))
			      (width-init (* 16 32))
			      (height (* 16 32))
			      (height-init (* 16 32))))
  (labels (make-hash-table))
  input-nodes
  output-nodes)



