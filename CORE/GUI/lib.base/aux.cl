(defmacro quick-cycle (selection direction)
  (let ((down :down)
	 (up :up))
    `(if (or (eq ,direction 1)
	     (eq ,direction ,down))
	 (incf ,selection)
	 (if (or (eq ,direction -1)
		 (eq ,direction ,up))
	     (decf ,selection)))))

(defmacro selection-cursor-up (selection &optional max-selection reset)
  `(progn (if (< (1- ,selection) 0)
	      (if ,reset
		  (setf ,selection ,max-selection))
	      (quick-cycle ,selection :up))))

(defmacro selection-cursor-down (selection &optional max-selection reset)
  `(progn (if (>= ,selection (or ,max-selection (1+ ,selection)))
	      (if ,reset
		  (setf ,selection 0))
	      (quick-cycle ,selection :down))))


;;ui-frame holds ui-elements

;;Note on future docker implementation:
;;  X-equation and such are just the x and y location.


;;If you are adding a ui-element to a ui-frame, just use numbers in relation to the offset of the ui-frame
;;So, if you want to draw a ui-button inside a ui-frame and the x/y of ui-button is 0/0 in relation to ui-frame, then just use 0/0


(defmacro load-image.ui (ui image-path)
  `(if (probe-file ,image-path);;if it exists, then load it
       (setf (ui-image-path ,ui) ,image-path
	     (ui-image ,ui) (tamias:load-image ,image-path));;(sdl2:create-texture-from-surface tamias:renderer (sdl2-image:load-image image-path)))
       (tamias:console.add-message (combine-strings "Sorry, either the image doesn't exist or the path to it is wrong. Current path: " image-path "
Example: font.png is \"engine/graphics code/font.png\" not Game/font.png or assets/font.png."))))

