(defgeneric ui.input (ui-element ui-type input modifiers &optional x-offset y-offset)
  ;;every ui.input will be called with whatever modifiers are active with the input
  ;;this will cause some interesting behavior if the element uses both keyboard
  ;;and mouse input, i.e. while moving a window, you can scroll it too.
  ;;this also depends on "upper" level code, where it's already
  ;;determined what Ui-element is being affected
  ;;(i.e. the current-active-element or whatever)
  (:method (ui-element ui-type input modifiers &optional x-offset y-offset)
    (declare (ignore ui-element ui-type input modifiers x-offset y-offset))
    nil))

;;(t.input:get-modifiers :move :shift) etc.
;;then check it against the current mod state?

;;for keyboard specific input, the modifiers can be used from the state input pckg
;;the modifiers for, say, a mouse, would be a list such as (:move) and such
;;or a unified modifier for the mouse (such as checking for :shift on the kb mods)
;;so (mouse-modifiers :shift :move ...)

(defgeneric ui.kb-input (ui-element ui-type kb-input)
  (:method (ui-element ui-type kb-input)
    ;;You can do some pretty cool things here :)
    (declare (ignore ui-element ui-type kb-input))
    nil))

(defgeneric ui.render (ui-element ui-type &optional hover x-offset y-offset)
  (:method (ui-element ui-type &optional hover x-offset y-offset)
    (declare (ignore hover x-offset y-offset))
    nil))

(defgeneric ui.mouse.hover (ui-element ui-type)
  (:method (ui-element ui-type)
    (declare (ignore ui-element))
    (declare (ignore ui-type))
    nil))
  ;;draw the element again but lighter? Uhhh, something like that I guess?
  ;;Actually, yeah. Unless it's a light color, then use a darker color (in fact, +/- 40 dependent on </> 127)
  ;;

(defgeneric ui.mouse.click (ui-element ui-type)
  (:method (ui-element ui-type)
    (declare (ignore ui-element))
    (declare (ignore ui-type))
    nil))
  ;;chekc mouse-x mouse-y, if action, funcall action, with ui-element as first arg

(defgeneric ui.mouse.move (ui-element ui-type)
  (:method (ui-element ui-type)
    (declare (ignore ui-element ui-type))
    nil))

(defmethod ui.mouse.click (ui-element (ui-type (eql :cursor)))
  nil)

(defmethod ui.mouse.move (ui-element (ui-type t))
  (when (ui-movable ui-element)
      ;;keep ahold of the original mouse position
    ;;mouse position is held when the ui-timer is 'finished'
    ;;
    (let ((inc-x tamias:*mouse-velocity-x*)
	  (inc-y tamias:*mouse-velocity-y*))
      (incf (ui-x ui-element) inc-x)
      (incf (ui-y ui-element) inc-y)
      )))

#|
;;Need to have a command subsystem in place now :\
;;How the fuck do I do a command subsystem

;;simple, keep the callback stuff in place...
;;well, there may need to be...yeahhh
;;I need to have a command callback stack
;;Lol
;;Essentially, command looks like this: :cancel, :ok, :undo
;;If a gui element gets the :cancel command (which is just a pushed and popped list)
;;then it reverts to its state prior to whatever the action taking place was
;;(sidenote: this is why undo stacks in art apps can be in 100s of megabytes)
|#
