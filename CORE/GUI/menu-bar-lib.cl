(defmacro menu-bar.init (state sub-state)
  `(progn
     (unless (gethash ',state ui-managers)
       (setf (gethash ',state ui-managers) (make-hash-table))
       (setf (gethash ',sub-state (gethash ',state ui-managers)) (make-ui-manager)))
     (setf (get-menu-bar ',state ',sub-state) (make-ui-menu-bar))
     (push :menu-bar (ui-manager-collection-ids (get-ui-manager ',state ',sub-state)))))

(defmacro menu.get-child (child-id &optional (state tamias:state) (sub-state (state-sub-state tamias:state)))
  `(get-ui-el ,child-id ,state ,sub-state))

(defun ui.menu-child (menu child-sym)
  (gethash child-sym (ui-children menu)))

(defun ui.menu-bar-child (menu-bar child-sym)
  (ui.menu-child menu-bar child-sym))

;;When we add a child item to a menu (child is of type menu-item)
;;we need to add it to the top level UI manager, but we need to keep it out of
;;the default rendered UI objects
;;because we assume that the menu is hidden on 'startup'


(defun menu.add-child (state sub-state parent child)
  ;;this assumes that child is a defined ui-element
  (ui.add-child state sub-state child)
  (container.add-child parent child))
(setf (fdefinition 'add-item-to-menu) #'menu.add-child)
(setf (fdefinition 'menu.add-item) #'menu.add-child)

(defun menu.define-child (state sub-state parent &key label action color (id (incf current-ui-id)))
  ;;parent is a menu
  (manager.init? state sub-state)
  (let ((child-item (make-ui-menu-item :id id
				       :height (cadr tamias.string:character-size)
				       :action action :color color :buff-func (pop quit-functions))))
    
    (let ((label? label))
      (unless label?
	(setf label? "UNLABELED-CHILD"))
      (eval `(push-quit (render:remove-buffer (tamias-string-buffer (ui-menu-item-label (get-ui-el (ui-id ',child-item) ',state ',sub-state))))))
      (setf (ui-label child-item) (make-tamias-string :text label?)
	    (ui-width child-item) (* (length label?) (car tamias.string:character-size)))
      (menu.add-item state sub-state parent child-item))))

(defun add-menu-bar-item (state sub-state item &key label action color)
  "Add's a 'top level' menu bar item, like 'File' or 'Edit'. Helper function
Supposed to be for internal GUI use, so don't export it for the package"
  (menu.define-child state sub-state (get-menu-bar state sub-state)
		      :label (or label (string-upcase (string-downcase (write-to-string item)) :end 1))
		      :action action :color color :id (aux:to-keyword item)))

(defmacro menu-bar.add-item (state sub-state item &key label action color)
  "Primary macro to add an item to the menu-bar of state and sub-state.
Example: (menu-bar.add-item 'blender 'sculpt 'tool (:color '(127 127 127 255))"
  `(add-menu-bar-item ',state ',sub-state ',item :label ,label :action ',action :color ,color)
  )

(defun add-menu-to-menu-item
    (menu-item)
  "Creates a menu for a menu item [stored in (ui-menu-item-menu menu-item)]"
  (when (not (ui-menu-item-menu menu-item))
    (let ((child-menu nil))
      (setf child-menu (make-ui-menu))
	    (ui-menu-item-menu menu-item) (ui-id child-menu))))

(setf (fdefinition 'add-item-to-child-menu) #'menu.add-child)

;;This needs to be redone!
;;This is ridiculously hard to follow!
;;Adding menu-items to a menu needs to be generalized, with only menu-bar having minor specialization
;;In fact, the menu-bar grows sideways, that's really the only specialization of mb's

(defun add-item-to-menu (item parent-menu)
  )

(defun menu-bar-item.add-item (item item-parent state sub-state &key label action color)
  "Helper function, add's an item to a menu-bar item (or an item of a menu-bar item). Ex: blender -> menu-bar -> File -> import -> fbx"
  (add-menu-to-menu-item item-parent)
  (let ((label? label)
	(parent-menu (ui-menu-item-menu item-parent))
	(child-item nil)) 
    (unless label?
      (setf label (string-downcase (write-to-string item)))
      (setf label (concatenate 'string (string-upcase (subseq label 0 1)) (subseq label 1))))
    (eval `(push-quit (render:remove-buffer
		       (tamias-string-buffer
			(ui-menu-item-label (get-menu-item ',item (get-menu-bar-item ',state ',sub-state ',item-parent)))))))
    
    (setf child-item (make-ui-menu-item
		      :width (* (length label)
				(car tamias.string:character-size))
		      :height (cadr tamias.string:character-size)
		      :label (make-tamias-string :text label)
		      :action action :color color
		      :buff-func (pop quit-functions)))
    ;;need to add this to the ":FILE" menu child
    (setf (get-menu-item child-item parent-menu)
	  child-item)
    (when (> (ui-menu-item-width
	      (get-menu-item
	       item
	       (get-menu-bar-item state sub-state item-parent)))
	     (ui-menu-widest (get-menu-bar-item state sub-state item-parent)))
      (setf (ui-menu-widest
	     (get-menu-bar-item state sub-state item-parent))
	    (ui-menu-item-width
	     (get-menu-item
	      item
	      (get-menu-bar-item state sub-state item-parent)))))
    (ui.add-mb-child state sub-state
		     (get-menu-item item
				    (get-menu-bar-item state sub-state item-parent)))
    (aux:push-to-end item (ui-menu-children-ids (get-menu-bar-item state sub-state item-parent)))))

(defmacro menu-bar.item.add-item (item item-parent state sub-state &key label action color)
  "add's an item to a menu-bar item (or an item of a menu-bar item). 
Example: (menu-bar.item.add-item import file blender sculpt :label \"import\")"
  `(menu-bar-item.add-item ',item ',item-parent ',state ',sub-state :label ,label :action ',action :color ,color))

(defmacro menu-bar.item.add-child (item item-parent state sub-state &key label action color)
  "add's an item to a menu-bar item (or an item of a menu-bar item). 
Example: (menu-bar.item.add-item import file blender sculpt :label \"import\")"
  `(menu-bar-item.add-item ',item ',item-parent ',state ',sub-state :label ,label :action ',action :color ,color))

;;(defmacro menu-bar.child.add-item (item item-parent state sub-state &key label action color)
;;  `(menu-bar-item.add-item ',item ',item-parent ',state ',sub-state :label ,label :action ',action :color ,color))
;;menu-bar.child.add-item is /supposed/ to be functionally the same as adding an item to a menu

(defmacro menu-bar.add-items (state sub-state &rest items)
  `(loop for item in (reverse ',items)
	 do (menu-bar.add-item ',state ',sub-state item)))

