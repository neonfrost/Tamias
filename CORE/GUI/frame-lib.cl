(defmacro ui-frame.make (state sub-state (x y w h))
  `(let ((frame (make-ui-frame :x ,x :x-init ',x
			       :y ,y :y-init ',y
			       :width ,w :width-init ',w
			       :height ,h :height-init ',h)))
#|
     (loop for child in ',items
	   do (setf (gethash (ui-id child) (ui-children frame)) child)
	      (push (ui-id child) (ui-children-ids frame))
	      (add-ui-to-manager ,state ,sub-state child))
|#
     (add-state-ui-element ,state ,sub-state frame)))

(defmacro ui-frame.element.make (type x y w h &key action special label entry)
  `(let ((element (make-ui-element :type ,type
				   :x ,x :x-init ',x
				   :y ,y :y-init ',y
				   :width ,w :width-init ',w
				   :height ,h :height-init ',h
				   :action ',action :special ,special
				   :label ,label :entry ,entry)))
     element))

(defmacro add-ui-to-frame (frame ui-el state sub-state)
  `(progn ;;(setf (gethash (ui-id ,ui-el) (ui-children frame)) ,ui-el)
	  (push (ui-id ,ui-el) (ui-children-ids ,frame))
	  (ui.add-to-manager ,state ,sub-state ,ui-el)))


"Adds a ui-element to a frame.
Implicit addition of an element will assume that you want it at the top left of a frame.
It grows to the right, then down and right, exactly like text would.
Directions:

direction must be set per element

Frame setup:
E1 - E2

(frame.add-element frame element dir)

nil - aka default - 
Adds to the right of last element of frame. 
If to right of last element will cause element-x to be greater than width of frame, 
    then x-offset is 0, and y-offset is y + height of biggest element in previous line

E1 - E2 - E3 [max width]
E4 - E5      [max width] E5 has 2x width of E2
E6 - E7 - E8 - E9 [max width] E8 and E9 have .5x width of E2

0
Adds below - Switch nil rules for x/width with y/height

dir = 0
E1 - E2 
E3 - E6 - E8
E4
E5 - E7
[m h]


dir = 2
E1 - E2
     E3
"

"note: frames are non-scrolling windows, so to speak"
(defgeneric frame.add-element-wrap (frame element direction)
  (:method (frame element direction)
    nil))

(defmacro ui-frame-let (ui-frame &body body)
  `(let ((frame-width (ui-base-width ,ui-frame))
	 (frame-height (ui-base-height ,ui-frame))
	 (frame-children (ui-frame-children ,ui-frame))
	 (x-offset 0)
	 (y-offset 0))
     (declare (ignorable frame-width frame-height frame-children x-offset y-offset))
     ,@body))

(defgeneric frame.add-elements (frame dir &rest elements)
  (:method (frame dir &rest elements)
    (declare (ignorable elements))
    nil))

(defmethod frame.add-elements (frame (dir (eql nil)) &rest elements)
  "Assumes default direction"
  (ui-frame-let frame
    (loop for element in elements
       do (ui-error-check element "Argument provided to FRAME.ADD-ELEMENTS elements list not an ui-element

object provided: " element "in: " (ui-id frame) "with x and y of: " (ui-x frame) (ui-y frame))
	 (setf (ui-base-x element) x-offset
	       (ui-base-y element) y-offset
	       (gethash (ui-element-id element) frame-children) element)
	 (incf x-offset (1+ (ui-element-width element)))
	 (if (>= x-offset (ui-frame-width frame))
	     (progn (incf y-offset (ui-base-height element))
		    (setf x-offset 0)))
	 )))

(defmethod frame.add-elements (frame (dir (eql 0)) &rest elements)
  (ui-frame-let frame
    (flet ((set-greatest-y (key ui-item)
	     (declare (ignore key))
	     (let ((y (ui-base-y ui-item)))
	       (if (> y y-offset) (progn (setf y-offset y)
					 (setf x-offset (ui-base-width ui-item)))))))
      (if (> (hash-table-count frame-children) 0)
	  (maphash #'set-greatest-y frame-children))) ;;seriously, why is 'greatest-y' even a thing here?
    (loop for element in elements
       do (ui-error-check element "Argument provided to FRAME.ADD-ELEMENTS elements list not an ui-element")
	 (setf (ui-base-x element) x-offset
	       (ui-base-y element) y-offset
	       (gethash (ui-element-id element) frame-children) element)
	 (incf y-offset (ui-element-height element))
	 (if (>= y-offset (ui-frame-height frame))
	     (progn (incf x-offset (ui-base-width element))
		    (setf y-offset 0)))
	 )))
#|

Add single elements

|#

(defmethod frame.add-element-wrap (frame element (direction (eql 'nil)))
  (declare (ignore direction))
  (ui-frame-let frame
    (ui-error-check element "Argument provided to FRAME.ADD-ELEMENT (SINGULAR) not an ui-element")
    (if (> (hash-table-count frame-children) 0)
	(loop for id in (ui-frame-ids frame)
	   do (let ((ui-element (gethash id frame-children)))
		(incf x-offset (ui-element-width ui-element))
		(if (>= x-offset (ui-frame-width frame))
		    (progn (incf y-offset (ui-element-height ui-element))
			   (setf x-offset 0))))))
    (setf (ui-element-x element) x-offset
	  (ui-element-y element) y-offset
	  (gethash current-ui-id frame-children) element)
;;    (incf (ui-frame-current-id frame))
    ))

(defmacro ui-frame.make-with-children (state sub-state (x y w h) items)
  `(let ((frame (make-ui-frame :x ,x :x-init ',x
			       :y ,y :y-init ',y
			       :width ,w :width-init ',w
			       :height ,h :height-init ',h)))
     (loop for child in ,items
	   do (setf (gethash (ui-id child) (ui-children frame)) child)
	      (push (ui-id child) (ui-children-ids frame))
	      (ui.add-to-manager ,state ,sub-state child))
     (add-state-ui-element ,state ,sub-state frame)))


(defmethod frame.add-element-wrap (frame element (direction (eql 0)))
  (ui-frame-let frame
    (setf (gethash (ui-element-id element) frame-children)
	  element)
    (push (ui-element-id element) (ui-frame-children-ids frame))))

(defmethod frame.add-element-wrap (frame element (direction (eql 1)))
  )
(defmethod frame.add-element-wrap (frame element (direction (eql 2)))
  )
(defmethod frame.add-element-wrap (frame element (direction (eql 3)))
  )

(defun frame.add-element (frame element &optional (direction nil))
  (frame.add-element-wrap frame element direction))

(defun ui-manager.add-frame (state sub-state x y w h &rest items)
  (ui-frame.make-with-children state sub-state (x y w h) items))

(setf (fdefinition 'ui.add-frame) #'ui-manager.add-frame)


