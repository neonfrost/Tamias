#|
There will never be a time where scroll-bars are children
|#

(defmethod ui.mouse.click (ui-element (ui-type (eql :scroll-bar)))
  "Check Mouse-y against the ui-element. "
  (if (eq (scroll-bar-orientation ui-element) :h)
      (incf (ui-scroll-bar-current-offset
	     ui-element)
	    tamias:*mouse-velocity-x*)
      (incf (ui-scroll-bar-current-offset
	     ui-element)
	    tamias:*mouse-velocity-y*)))

(defmethod ui.mouse.click (ui-element (ui-type (eql :button)))
  (eval (ui-element-action ui-element)))

(defmethod ui.mouse.click (ui-element (ui-type (eql :check-box)))
  '(setf (ui-check-box-checked ui-element) t)
  (eval (ui-element-action ui-element)))

;;ui.mouse.scroll (ui-element (ui-type (eql :scroll-bar)))
