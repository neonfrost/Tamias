#h rmb-menus
;;(defvar rmb-menus (make-hash-table))

;;On the creation of an rmb-menu
;;
;;When you make an rmb-menu,
;;it will be included into the ui-manager state/sub-state stuff
;;However, it will not be part of the default rendering thing
;;Instead, in order to interact with the rmb-menu, the ID of the right clicked element
;;will be used to get the ID of the rmb-menu to display
;;
;;If there is no ID to menu ID (e.g. 32 -> nil) then it will instead use
;;the parent ui-type (e.g. :toolbox) for the rmb-menu
;;If there is no rmb-menu for the parent type, then it may display a generic menu
;;Not 100% sure where I'm at with that personally tbh

(defstruct (rmb-menu (:include ui-menu
		      (type :rmb-menu))))
;;Do i need to fix the ui-menu subsystem?
;;What happens when ui-menus are rendered?

;;
;;make the rmb-menu to be the current-active element
;;we render the menu, using mouse-x and mouse-y as the anchor point
;;

(defun gui.get-menu-item.click (parent-item &optional (x-offset 0) (y-offset 0)) ;;is a ui-menu-item
  (if (ui-ids parent-item)
      (loop :for id :in (ui-ids parent-item)
	    :do (let ((child-item (gethash id (ui-children parent-item))))
;;		 (print id)
		 (let ((result (gui.get-menu-item.click child-item x-offset y-offset)))
#|		   (format t "child: x y w h ~A ~A ~A ~A"
			   (+ x-offset (ui-x child-item))
			   (+ y-offset (ui-y child-item))
			   (ui-width child-item)
			   (ui-height child-item))|#
		   (if result
		       (return t)
		       (incf y-offset (ui-height child-item)))
		 )))
      (if (mouse.test-position (+ x-offset (ui-x parent-item))
			       (+ y-offset (ui-y parent-item))
			       (ui-width parent-item)
			       (ui-height parent-item)
			       'box)
	  (eval (ui-element-action parent-item)))))


(defmacro menu.rmb.make (state sub-state ui-parent-type-or-id &rest entries)
  `(let ((ret-menu nil))
     (setf ret-menu (make-rmb-menu))
     ;;have to make each entry into a menu-item ui element
     ;;So, how should the entries be handled?
     ;;Maybe by pair?
     ;;maybe keyword?
     ;;e.g. :name callback or (:name callback) (:name-2 c-2)
     ;;then each entrie is a button
     ;;holy shit, a list of buttons that are standardized!
     (loop :for pair :in ',entries
	   :do (let ((ui-button nil))
		 (setf ui-button (ui.make-button :label (write-to-string (car pair)))
		       (ui-callback ui-button) (cadr pair)
		       (gethash (ui-id ui-button) (ui-children ret-menu)) ui-button)
		 (push (ui-id ui-button) (ui-children-ids ret-menu))))
     (setf (gethash ,ui-parent-type-or-id rmb-menus) (ui-id ret-menu))
     (ui-element.add ,state ,sub-state ret-menu)))
	   

(defun prep-rmb-menu (ui-element)
  (let ((rmb-menu (or (get-ui-element.current-state (gethash (ui-id ui-element) rmb-menus))
		      (get-ui-element.current-state (gethash (ui-type ui-element) rmb-menus))
		      (get-ui-element.current-state (gethash :default rmb-menus)))))
    (when rmb-menu
      (setf (ui-x rmb-menu) (mouse.ui-x)
	    (ui-y rmb-menu) (mouse.ui-y))
      (ui.set-active rmb-menu)
      (push rmb-menu ui.auxiliary-render-list)
  ;;push a command to render this menu as the last thing to be rendered.
  ;;actually, this would need to be a sub-state thing, RIP
  ;;or, manager state hmmm
  ;;adding in this functionality will be hell in a hand basket
      )))

(defun deactivate-rmb-menu (ui-element)
  (pop ui.auxiliary-render-list)
  (if ui.auxiliary-render-list
      (ui.set-active (get-ui-element.current-state (car ui.auxiliary-render-list)))
      (ui.set-active -1)))


(defun render.rmb-menu ()
  )
