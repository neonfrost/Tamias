(defun activate-entry (ui-element)
  (declare (ignore ui-element))
  )

(defun deactivate-entry ()
  )

(defmethod ui.mouse.click (ui-element (ui-type (eql :entry)))
  ;;maybe put the position finding up here instead?
  ;;Like....hurm. Option 1: Click to activate entry. Click again to place the cursor
  ;;Option 2: Click to activate AND place cursor (if mouse was inside text area)
  (activate-entry ui-element)
  ;;Then we use mouse-x and mouse-y to figure out where to put the cursor
  ;;requires ensuring that the cursor was inside the
  )
;;set current text stuff to the ui-element-entry
