(defmethod ui.mouse.click (ui-element (ui-type (eql :spin-box)))
  ;;check which region got clicked
  (let* ((x-max (+ (ui-base-x ui-element) (ui-base-width ui-element)))
	 (y-max (+ (ui-base-y ui-element) (ui-base-height ui-element)))
	 (max-val (ui-spin-box-max-value ui-element))
	 (min-val (ui-spin-box-min-value ui-element))
	 (increm (ui-spin-box-incrementer ui-element))
	 (fin-val (tamias-variable-value (number-entry-value (ui-element-custom ui-element)))))
;;    (render:text "+1" (- x-max 16) (ui-base-y ui-element) :width 32 :height 16)
    ;;    (render:text "-1" (- x-max 16) (- y-max 16) :width 32 :height 16)))
    (let ((in-ui-x (and (>= tamias:*mouse-x* (- x-max 32))
			(<= tamias:*mouse-x* x-max)))
	  (in-ui-y (and (>= tamias:*mouse-y* (- y-max 16))
			(<= tamias:*mouse-y* y-max))))
      (if in-ui-x
	  (if in-ui-y
	      (decf fin-val increm)
	      (incf fin-val increm))))
    (if (> fin-val max-val)
	(setf fin-val max-val)
	(if (< fin-val min-val)
	    (setf fin-val min-val)))
    (setf (tamias-variable-value (number-entry-value (ui-element-custom ui-element)))
	  fin-val)))
	
    
