(defun ui.find-current-hover.container (el &optional (x-offset 0) (y-offset 0))
  (if (mouse.test-position
       (+ (ui-x el) x-offset)
       (+ (ui-y el) y-offset)
       (ui-width el)
       (ui-height el) 'box)
      (let ((x-off 0)
	    (y-off 0)
	    ret-val)
	(if (ui-ids el)
	    (loop for id in (ui-ids el)
		  do (let ((child (gethash id (ui-children el))))
		       (if (ui-container-p child)
			   (setf ret-val (ui.find-current-hover.container child
									  (+ x-offset x-off (ui-x el))
									  (+ y-off y-offset (ui-y child))))
			   (when (mouse.test-position
				(or (ui-x child) 0)
				(or (ui-y child) 0)
				(ui-width child)
				(ui-height child) 'box)
			       (setf ret-val (ui-id child)))))
		     (if ret-val (return ret-val)))
	    (ui-id el)))))

(defmethod ui.input (ui-element (ui-type t)
		     input
		     (modifiers (eql (t.input:get-modifiers :hover)))
		     &optional (x-offset 0)  (y-offset 0))
  ;;  (ui.find-current-hover.container ui-element x-offset y-offset)
  nil
  )






(defun ui.find-current-hover.menu-bar-item.child (menu-bar-item m-bar-y)
  (loop for child-id in (ui-ids menu-bar-item)
	for y-acc from 16 to (* (length (ui-ids menu-bar-item))
				char-height) by char-height
	do (let* ((menu-item (ui.menu-child menu-bar-item child-id))
		  (x (+ (ui-x menu-item) (ui-x menu-bar-item)))
		  (y (+ m-bar-y y-acc))
		  (w (ui-menu-widest menu-bar-item)) (h (ui-height menu-item)))
	     (if (mouse.test-position x y w h 'box)
		 ;;Set the menu-item to be highlighted
		 (return (setf (ui-menu-item-show-child menu-item) t
			       ui.current-hover ;;-> (ui-manager-curren-hover (get-current-manager))
			       (ui-id menu-item)))))))
  ;;		       (setf (ui-manager-current-hover (get-current-ui-manager))
  ;;			     nil)))
  

(defun ui.find-current-hover.menu-bar-item (menu-bar)
  (let ((x-offset 0))
    (loop :for menu-bar-item :in (ui-menu-bar-ids menu-bar)
	  :do (let ((mbar-item (gethash menu-bar-item (ui-menu-bar-children menu-bar))))
	       (if (mouse.test-position (+ x-offset (ui-x mbar-item))
					(+ (ui-padding-y menu-bar) (ui-y mbar-item))
					(ui-width mbar-item)
					(ui-height mbar-item)
					'box)
		   (return (setf ui.current-hover ;;->(ui-manager-current-hover (get-current-ui-manager))
				 (ui-id mbar-item))))
	       (incf x-offset (+ (ui-padding-x menu-bar) (ui-width mbar-item)))))))
	    ;;highlight  the current menu-bar the mouse is over ^

(defun ui.find-current-hover.menu-bar (menu-bar)
  (let* ((active-item (ui-menu-bar-active-item menu-bar))
	 (menu-bar-item (gethash active-item
				 (ui-menu-bar-children menu-bar))))
    (if menu-bar-item
	;;the current menu-bar top level active-child
	(ui.find-current-hover.menu-bar-item.child menu-bar-item
						   (ui-y menu-bar))
	;;no active-item-id
	(if (mouse.test-position
	     (ui-menu-bar-x menu-bar) (ui-menu-bar-y menu-bar)
	     (ui-menu-bar-width menu-bar) (ui-menu-bar-height menu-bar)
	     'box)
	    (ui.find-current-hover.menu-bar-item
	     menu-bar)))))
#|
(defmethod ui.input (ui-element (ui-type (eql :menu-item))
		     input
		     (modifiers (eql (t.input:get-modifiers :hover)))
		     &optional (x-offset 0)  (y-offset 0))
;;    (ui.find-current-hover.menu-item ui-element x-offset y-offset)
  )

(defmethod ui.input (ui-element (ui-type (eql :menu))
		     input
		     (modifiers (eql (t.input:get-modifiers :hover)))
		     &optional (x-offset 0)  (y-offset 0))
  (loop :for child-id :in (ui-menu-children-ids ui-element)
	:do nil)
;;  (ui.find-current-hover.menu ui-element x-offset y-offset)
  )
|#
(defun ui.find-current-hover ()
  (let ((ui-message nil)
	(ui-manager (get-current-ui-manager)))
    (loop for id in (ui-manager-collection-ids ui-manager)
	  do (let ((el (gethash id (ui-manager-collection ui-manager))))
	       (case (ui-type el)
		 ((:frame 
		   :window 
		   :container 
		   :dock)
		  (setf ui-message (ui.find-current-hover.container el)))
		 (:menu-bar
		  (setf ui-message (ui.find-current-hover.menu-bar el)))
		 (otherwise (if (mouse.test-position
				 (ui-x el)
				 (ui-y el)
				 (ui-width el)
				 (ui-height el) 'box)
				(return (setf ui-message (ui-id el))))))
	       (when ui-message
		 (return t))))
    (setf (ui-manager-current-hover ui-manager) (or ui-message -2))))
    
(defun tamias.gui.mouse-move ()
  #|
Change current hover (used to highlight buttons and other elements), check if menu-bar is active, if menu-bar is active, then check if the mouse is hovering over a menu-bar-item
If the mouse is hovering over a menu-bar-item, then set the current active menu-bar-item to the one it is hovering over (will change what is dropped down from the bar)
If menu-bar is active and the mouse is hovering over a sub-menu entry, highlight that entry, if not, set ui.current-hover to nil. Do this recursively so long as a menu-item is active
  |#
  (when (ui.current-manager)
    (ui.find-current-hover)
    (when (> (ui-manager-current-hover (get-current-ui-manager))
	     -1)
      (let ((cur-el (get-ui-el (ui-manager-current-hover (get-current-ui-manager))
			       (state-symbol tamias:state)
			       (state-sub-state tamias:state))))
	(ui.input cur-el (ui-type cur-el)
		  *t-mouse-button*
		  (t.input:get-modifiers *t-mouse-state*
					 *t-mouse-moving*
					 *t-mouse-held?*
					 :hover)
		  0 0)))
    (when (ui-p active-ui-element)
	(if (ui-scrollable active-ui-element)
	  "Scroll bitch, check if mouse has been held down (elt *t-mouse* 0). Really, it just checks if the mouse's button is either there or not, elt 0 could be :button-left, middle or right."))))
	  ;;go through each item in the ui-manager and check the mouse against them
