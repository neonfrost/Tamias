(defun ui.render-children (ui-element x-offset y-offset)
  (let ((child-x-offset 0)
	(child-y-offset 0))
    (loop for id in (ui-ids ui-element)
       do (let ((child-element (gethash id (ui-children ui-element))))
	    (ui.render child-element
		       (ui-type child-element) (eq (ui-id child-element)
						   ui.current-hover)
		       (+ child-x-offset x-offset) (+ y-offset child-y-offset))
	    (setf child-x-offset (+ (ui-x child-element) (ui-width child-element)))
	    (setf child-y-offset (ui-y child-element))))))

(defmethod ui.render (ui-element (ui-type (eql :container)) &optional hover x-offset y-offset)
  )
