(defmethod ui.render (ui-element (ui-type (eql :dock))
		      &optional hover (x-offset 0) (y-offset 0))
  ;;render a window bg for the dock
  ;;render a title bar for the dock
  ;;render a drop down arrow and an X
  (render:box (ui-x ui-element) (ui-y ui-element)
	      (ui-width ui-element) (ui-height ui-element)
	      tamias.colors:+orange+)
  (loop for child in (ui-children-ids ui-element)
	do (let ((ui-child (gethash child (ui-children ui-element))))
	     (ui.render ui-child (ui-type ui-child) nil
			(ui-x ui-element) (ui-y ui-element))
  
