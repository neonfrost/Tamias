(defun ui.render-text (str x y &optional w h (color tamias.colors:+white+))
  (declare (ignore w h))
  (if (find #\newline str)
      (let ((cur-text "")
	    (rest-text str)
	    (cur-line 0))
	(loop for n below (1+ (count #\newline str))
	   do (if (find #\newline rest-text)
		  (setf cur-text (subseq rest-text 0 (position #\newline rest-text))
			rest-text (subseq rest-text (1+ (position #\newline rest-text))))
		  (setf cur-text rest-text))
	     (render:text cur-text (1+ x) (+ 1 y (* cur-line 16)) :color color)
	     (setf cur-text "")
	     (incf cur-line)))
      (render:text str (+ x 1) (+ y 1) :color color)))

(defun ui.render-text-chunker (str x y w h &optional (color tamias.colors:+white+))
  (let ((character-per-line-limit (round (/ w (car tamias.string:character-size)))))
    (if (and (< (length str) character-per-line-limit)
	     (not (find #\newline str)))
	(ui.render-text str x y w h color)
	;;if there is more text than can fit inside the ui-element width-wise, then break it up and render each string separately
	;;text chunker breaks the string into strings, where rope-group uses 'ropes' as its base
	;;this is more of a precursor to supporting ropes in tamias
	(loop :for txt
		:in (tamias.string:text-chunker str character-per-line-limit)
	      :for y-acc
		:below (ceiling (/ h (cadr tamias.string:character-size)))
	      :do (ui.render-text txt x
				  (+ y
				   (* y-acc
				      (cadr tamias.string:character-size)))
				  w h color)))))
  
