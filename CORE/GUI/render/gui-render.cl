"the def-render is to render the cursor
It isn't necessary, but it'll help the user track where the cursor is and what they are clicking on

If a macro or function appears directly before a method, that macro/function is there for that method
example: (defun ui.render-spin-box ...) (defmethod ... (eql 'spin-box)...)
a generalized macro/function will be at teh top, before the rendering method for the ui-button
"

