(defun render.menu-children (ids &optional (x-offset 0) (y-offset 0))
  (loop :for child-id :in ids
	:do (render.menu-item
	     (get-ui-el child-id tamias:state
			 (state-sub-state tamias:state))
	      x-offset y-offset)))

(defun render.menu (ui-menu x-offset y-offset)
  (render:box x-offset   y-offset
	      (ui-menu-width   ui-menu)
	      (t.aux:use-<or>
	       (*
		(length   (ui-menu-ids   ui-menu))
		(cadr   tamias.string:character-size))
	       (cadr    tamias.string:character-size))
	      (or (ui-menu-color ui-menu) tamias.colors:+cream+))
  (render.menu-children (ui-children-ids ui-menu) x-offset y-offset))

(defun render.menu-item (ui x-offset y-offset &key width active?)
  (render:box x-offset y-offset
	      (or width (ui-menu-item-width ui))
	      (cadr tamias.string:character-size)
	      (or (ui-menu-item-color ui) tamias.colors:+dark-red+))
  (when (or active?
	    (eq (get-current-hover) (ui-id ui)))
    (render:box x-offset y-offset
		(or width (ui-menu-item-width ui))
		(cadr tamias.string:character-size)
		(tamias.colors:offset tamias.colors:+dark-red+))
    (setf (ui-menu-item-show-child ui)
	  t)
      ;;then render the children of...no...
      ;;jesus fucking christ, this is absolutely ridiculous
      ;;A recursive function that keeps the original menu-item highlighted, while highlighting
      ;;the current item of the child menus
      )
  (unless (tamias-string-buffer (ui-label ui))
    (push (ui-buff-func ui) quit-functions)
    (render:to-buffer (tamias-string-text (ui-label ui))
		      (tamias-string-buffer (ui-label ui))))
  (render:text-buffer
   (tamias-string-buffer (ui-label ui))
   nil
   (list x-offset y-offset)))

(defmethod ui.render (ui-element (ui-type (eql :menu-item)) &optional hover (x-offset 0) (y-offset 0))
  ;;  (when hover
  ;;      (ui.render-hover ui-element x-offset y-offset))
  (render.menu-item ui-element x-offset y-offset)
  (when (ui-menu-item-show-child ui)
    (when (ui-menu-item-child-menu ui)
      (render.menu (get-ui-el (ui-menu-item-child-menu ui)
			      state
			      (state-sub-state state))
		   (+ (ui-width ui) x-offset)
		   y-offset))))

(defmethod ui.render (ui-element (ui-type (eql :menu)) &optional hover (x-offset 0) (y-offset 0))
  ;;
  (render.menu ui-element (ui-x ui-element) (ui-y ui-element))
  (let ((y-offset-mod 0))
    (loop :for child-id :in (ui-children-ids ui-element)
	  :do (render.menu-item (ui.menu.get-child ui-element child-id)
				(+ (ui-x ui-element) x-offset)
				(+ y-offset-mod (ui-y ui-element) y-offset)
				:active? (eq (ui-menu-active-item ui-element)
					     child-id))
	      (incf y-offset-mod (ui-height (ui.menu-child ui-element child-id))))))

(defmethod ui.render (ui-element (ui-type (eql :rmb-menu)) &optional hover (x-offset 0) (y-offset 0))
  (render.menu ui-element (ui-x ui-element) (ui-y ui-element))
  (loop :for child-id :in (ui-children-ids ui-element)
	:do (ui.render (gethash child-id (ui-menu-children ui-element))
		       :menu-item
		       hover (ui-x ui-element) (ui-y ui-element)))
  )
