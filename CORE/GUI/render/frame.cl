(defun ui.render-border (ui-container x-offset y-offset)
  (ui.render-bg ui-container
		(+ x-offset (ui-base-x ui-container))
		(+ y-offset (ui-base-y ui-container))
		nil nil
		0 0
		(ui-container-border-color ui-container)))

(defmethod ui.render (ui-element (ui-type (eql :frame)) &optional hover (x-offset 0) (y-offset 0))
  (ui.render-border ui-element
		    (+ x-offset (ui-base-x ui-element))
		    (+ y-offset (ui-base-y ui-element)))
  (incf x-offset 4)
  (incf y-offset 4)
  (ui.render-bg ui-element
		(+ x-offset (ui-base-x ui-element))
		(+ y-offset (ui-base-y ui-element)))
  (if (ui-label ui-element)
      "(let ((label-x nil)
             (label-y nil) lab-w lab-h)
         (ui.render-bg ui-element ui-label-x ui-label-y label-width label-height)
         (ui.render-text (ui-label ui-element) label-x label-y))")
  ;;loop through each element in the frame and render it.
  (incf x-offset (ui-frame-x ui-element))
  (incf y-offset (ui-frame-y ui-element))
  (ui.render-children ui-element x-offset y-offset))
