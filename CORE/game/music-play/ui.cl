(define-state cl-fm)
(setf state cl-fm)
(defvar icons '((:left-arrow "<")
		(:right-arrow ">")
		(:up-arrow "^")
		(:plus "+")))

(defvar palette (list (parse-hex-color "00b349" :return-type 'list)
		      (parse-hex-color "00787a" :return-type 'list)
		      (parse-hex-color "94ebd8" :return-type 'list)
		      (parse-hex-color "003d34" :return-type 'list)))
(defvar fg-palette (nth 0 palette))
(defvar bg-palette (nth 1 palette))
(defvar font-palette (nth 2 palette))
(setf current-font-color font-palette)
(setf tamias-renderer-clear-color (nth 3 palette))
(defvar fm-cwd (sb-posix:getcwd))
(defvar fm-cwd-files (list-directory fm-cwd :to-var t))
(defvar fm-cwd-files-buffer nil)
(defvar cl-fm-menus nil)
(defvar cur-file-num 0)
(defvar files-pane-offset 0)

(defmacro define-palette (var fg bg fnt clear-color)
  `(defvar ,var (list ,fg ,bg ,fnt ,clear-color)))
(define-palette default-palette "00b349" "00787a" "94ebd8" "003d34")
(define-palette blue-rose "294552" "597884" "acc4ce" "9eb9b3")
(define-palette mountain "d5aaaa" "866d5c" "fff3e3" "414f47")
#|
(define-palette alternative-pastel  "e5b3b7"  "005a5c"  +dark-pastel-grey+ "9eb9b3")
|#
;;(define-palette pink-palette +pastel-pink+  "00b4b8"  +dark-pastel-grey+ "9eb9b3")
(define-palette pink-palette +pastel-pink+  "e5b3b7"  +dark-pastel-grey+ "005a5c")
(defun update-palette ()
  (setf fg-palette (nth 0 palette)
	bg-palette (nth 1 palette)
	font-palette (nth 2 palette)
	current-font-color font-palette
	tamias-renderer-clear-color (nth 3 palette)))
(defmacro change-palette (var)
  `(let ((fg (nth 0 ,var))
	 (bg (nth 1 ,var))
	 (fnt (nth 2 ,var))
	 (clear-color (nth 3 ,var)))
     (if (stringp fg)
	 (setf fg (parse-hex-color fg :return-type 'list)))
     (if (stringp bg)
	 (setf bg (parse-hex-color bg :return-type 'list)))
     (if (stringp fnt)
	 (setf fnt (parse-hex-color fnt :return-type 'list)))
     (if (stringp clear-color)
	 (setf clear-color (parse-hex-color clear-color :return-type 'list)))
     (setf palette (list fg bg fnt clear-color))
     (update-palette)))
(change-palette pink-palette)


(define-menu fm-navigation cl-fm-menus
  0  0
  (round (/ *screen-width*  8))  48
  bg-palette fg-palette)

(define-menu fm-working-directory cl-fm-menus
  (round (/ *screen-width*  8))  0
  (- *screen-width* (round (/ *screen-width*  8)))  48
  bg-palette  fg-palette)

(define-menu bookmarks-pane cl-fm-menus
  0  48
  (round  (/ *screen-width*  8))  (- *screen-height*  48)
  bg-palette fg-palette)

(define-menu files-pane cl-fm-menus
  (round  (/ *screen-width*  8))  48
  (- *screen-width*  (round  (/ *screen-width*  8)))
  (- *screen-height* 48)
  bg-palette fg-palette)
(define-screen fm-screen cl-fm-menus)

(defun set-fm-files ()
  (setf fm-cwd (sb-posix:getcwd)
	fm-cwd-files (list-directory fm-cwd :to-var t)
	fm-cwd-files-buffer (create-text-buffer fm-cwd-files :width 1920
						:height (* (count #\newline fm-cwd-files) (cadr character-size))
						:to-texture t :string-case 'text)))

(defun render-file-manager ()
  (fm-screen)
  (if (not (string-equal (sb-posix:getcwd) fm-cwd))
      (set-fm-files))
  (let ((icons-string (combine-strings (cadr (assoc :left-arrow icons)) " " (cadr (assoc :right-arrow icons)) " " (cadr (assoc :up-arrow icons)) " " (cadr (assoc :plus icons)))))
    (render-string icons-string (menu-x fm-navigation) (menu-y fm-navigation)))
  (render-string fm-cwd (menu-x fm-working-directory) (menu-y fm-working-directory))
  (render-box (menu-x files-pane) (+ (menu-y  files-pane)  (- (* 16  cur-file-num) files-pane-offset))
	      (menu-width files-pane) 18 :color (nth 1 palette))
  (if fm-cwd-files-buffer
      (let ((dest-height (if (> (menu-height files-pane) (sdl2:texture-height fm-cwd-files-buffer))
			     (sdl2:texture-height fm-cwd-files-buffer)
			     (menu-height files-pane))))
	(tex-blit fm-cwd-files-buffer
		  :src (create-rectangle (list 0 files-pane-offset
					       (menu-width files-pane) (menu-height files-pane)))
		  :dest (create-rectangle (list (menu-x files-pane) (menu-y files-pane)
						(menu-width files-pane) dest-height))
		  :color font-palette))
      (set-fm-files)))
(add-to-state-render render-file-manager cl-fm)

#|(defmacro add-key (key state key-state &rest function-name)
  `(setf (gethash ,key (gethash ,key-state (state-keys ,state)))
	 `,(quote ,function-name)))
|#
(add-key :scancode-down cl-fm :down (if (< cur-file-num (1- (+ (round (/ (menu-height files-pane) 16)) (round (/ files-pane-offset 16)))))
					(if (< cur-file-num (1- (count #\newline fm-cwd-files)))
					    (incf cur-file-num 1))
					;;increase offset
					(if (< cur-file-num (count #\newline fm-cwd-files))
					    (progn (incf cur-file-num 1)
						   (incf files-pane-offset 16)))
					))
(add-key :scancode-up cl-fm :down (if (> cur-file-num 0)
				      (decf cur-file-num 1))
	 (if (eq cur-file-num (1- (/ files-pane-offset 16)));;(+ (round (/ (menu-height files-pane) 16)) (round (/ files-pane-offset 16))))
	     (decf files-pane-offset 16)))
				      
