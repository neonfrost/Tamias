(define-state fs-test t 'idle)

(defmacro create-file-browser (state sub-state &optional (x 0) (y 0) (w tamias:screen-width) (h tamias:screen-height))
  `(let* ((label-width (- ,w 8))
	  (label-height (+ 32 8))
	  (ui-fs (create-ui-file-selector ',state ',sub-state (:x (+ ,x 4)
							     :y (+ ,y 4)
							     :width (- (round (/ ',w 4)) 8)
							     :height (- ',h `,label-height 8))))
	  (ui-fs-el (get-ui-el (ui-id ui-fs) ,state ,sub-state))
	  
	  (ui-text-viewer (ui.child-label ,state ,sub-state
					  (+ ,x 4 (round (/ ,w 4)))
					  (+ ,y 4)
					  `(ui-fs-current-file-contents ,ui-fs-el)
					  :width (- (round (* 7 (/ ,w 8))) 8)
					  :height (- ,h label-height 8)
					  :color ,tamias.colors:+black+
					  :use-buffer nil
					  ))
	  (ui-status-bar (ui.child-label ,state ,sub-state
					 4 (+ 4 (- ,h label-height))
					 (list 'concatenate
					       ''string
					       `(namestring (or (ui-fs-current-file ,ui-fs-el) #P"No current file"))
					       ''(#\newline)
					       `(ui-fs-path-str ,ui-fs-el))
					 :width label-width
					 :height (- label-height 8)
					 :color ,tamias.colors:+cobalt+
					 :use-buffer nil)))
     (ui.add-frame ',state ',sub-state ,x ,y ,w ,h ui-status-bar ui-text-viewer ui-fs)))

(create-file-browser fs-test idle)

(push-init (set-active-ui (get-ui-el 0 fs-test idle)))
;;(push-init (setf tamias-current-key #(nil nil 0 nil)))
