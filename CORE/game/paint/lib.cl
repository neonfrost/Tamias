(defmacro blit (src-surface src-rect dest-surface dest-rect)
  `(sdl2:blit-surface ,src-surface ,src-rect ,dest-surface ,dest-rect))

(defun create-surface (width height)
  (sdl2:create-rgb-surface width height 24) ;;bit depth in bits
  )

(defstruct (ui-canvas
	     (:include ui-element
		       (x 40)
		       (y 40)
		       (width 100)
		       (height 100)
	      (type 'canvas)))
  texture)

(defmethod render.ui (ui-element (ui-type (eql 'canvas))
		      &optional hover (x-offset 0) (y-offset 0))
  (declare (ignore hover x-offset y-offset))
  (if (not (ui-canvas-texture ui-element))
      (setf (ui-canvas-texture ui-element)
	    (sdl2:create-texture-from-surface tamias:renderer
					      (ui-canvas-custom ui-element))))
  (let ((render-src (ui-canvas-texture ui-element)))
    (render:tex-blit render-src :src (render:create-rectangle (list 0 0 (sdl2:texture-width render-src) (sdl2:texture-height render-src)))
				:dest (render:create-rectangle (list (ui-base-x ui-element) (ui-base-y ui-element)
								     (ui-base-width ui-element) (ui-base-height ui-element))))))
