(define-state paint)
(set-state paint)
(tamias:set-sub-state 'init)

(state.init-ui 'paint 'paint)


(defvar brush nil)
(defvar canvas nil)
(defvar brush-color '(255 255 255))
(setf brush-color '(0 127 127))
(defun paint-surface (surface ui-element)
  (render:with-rectangle mouse-rect (list (- tamias:*mouse-x* (ui-element-x ui-element))
					  (- tamias:*mouse-y* (ui-element-y ui-element))
					  8 8)
   (let ((r (car brush-color))
	 (g (cadr brush-color))
	 (b (caddr brush-color)))
     (sdl2:fill-rect surface mouse-rect (sdl2:map-rgb (sdl2:surface-format surface) r g b))
    )))

(defun set-brush ()
  (setf brush (create-surface 2 2))
  (sdl2-ffi.functions:sdl-lock-surface brush)
  (loop for byte below 32
     do (setf (cffi:mem-aref (sdl2:surface-pixels brush) :int byte)
	      ;;(ui-canvas-custom canvas)) :int byte)
	      255))
  (sdl2-ffi.functions:sdl-unlock-surface brush)
  )

(defun free-brush ()
  (if brush
      (sdl2:free-surface brush)))

(defun reset-brush ()
  (free-brush)
  (set-brush))
 
(defun set-canvas ()
  (setf canvas (make-ui-canvas :custom (create-surface 100 100)))
  (setf (ui-canvas-action canvas)  '(paint-surface (ui-canvas-custom canvas) canvas))
  (add-state-ui-element 'paint 'paint
			canvas))

(defun reset-canvas ()
  (sdl2:free-surface (ui-canvas-custom canvas))
  (setf (ui-canvas-custom canvas) (create-surface 100 100)))

(defun free-canvas ()
  (if (ui-canvas-custom canvas)
      (sdl2:free-surface (ui-canvas-custom canvas))))

(defun save-paint-surface ()
  )

(defun destroy-canvas ()
  (save-paint-surface)
  (print "Saved")
  (free-brush)
  (print "Brush surface")
  (free-canvas)
  (print "died before ui-el")
  (setf (get-ui-el (ui-id canvas) paint paint) nil)
  (decf current-ui-id)
  (print "died after ui-el"))

(def-logic (paint 'init)
    (set-brush)
    (set-canvas)
    (load-paint-surface)
    (tamias:set-sub-state 'paint)
  )

(defmethod action.click (ui-element (ui-type (eql 'canvas)))
;;  (eval (ui-element-action ui-element))
  (paint-surface (ui-canvas-custom ui-element) ui-element)
  (if (ui-canvas-texture ui-element)
      (sdl2:destroy-texture (ui-canvas-texture ui-element)))
  (setf (ui-canvas-texture ui-element)
	(sdl2:create-texture-from-surface tamias:renderer
					  (ui-canvas-custom ui-element))))

(push-init (tamias:set-sub-state 'init))
(push-quit (destroy-canvas))

