So first off:

Buttons - Very basic UI element, will allow test of: rendering, hover, action

COMPLETED MILESTONE 1 11/22/2020

Labels - Ensures that render:text works correctly and as expected (width & height imply how big you want the text to be, not how big the text is)

Side note: Should the text function use x y as absolute coordinates, or use them as the center? - origin-as-center resolves this issue.

Completed: M2 5/1/2021


Entry - Ensure that keyboard input is working correctly - done
      	Activated entry should have a different color to show that it's in edit mode. (or some kind of symbol indicating it is the active element being edited)
		Branch for whether or not edit mode is on. i.e. keyboard-shortcuts won't work app-wide -- Covered with :textinput in the sdl2 file, need to figure
														        out a way to ensure all backends will adhere
															to this
		       	   -??? Does it Require modifiying keyboard input function in engine/input ???
				No. Not in the current iteration

Semi-Completed : Needs branching for entry mode, needs text wrapping


Once the above is done, work on a stock simulator that can:
     -Sort By:
     	   -Name
	   -Price
	   -Change
	   -Dividends


Completed: M3 5/5/2021


Get menu-bar up and running and ensure that ui-menu works as expected

Basic Menu-bar completed (menu-bar clickable, menu-bar children 1 level deep are clickable and executable)

M4 3/?/2023


Spin box (custom value not setable through keyboard yet)
3/31/2023 (ish)



Eventually I want to make a gui editor, the gui editor will make it infinitely easier to create guis and edit them

Big thing will be creating a node editor

GUI projects:

-Stock Simulator 
TICKER finished

-Basic node editor (like cards with text)

-GUI maker/editor (make the GUI side of programs in this)

-Item creator for S'daia (to be ported to 3d)

