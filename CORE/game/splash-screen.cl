(define-state splash-screen t 'idle)
(set-state splash-screen 'idle)

(state.init-ui splash-screen 'idle)

(set-bg-color tamias.colors:+black+)
(tamias:set-resolution 0)

(ui.add-label splash-screen idle 236 200 "Made with"
	      :width 70 :height 16 :color tamias.colors:+black+)

(ui.add-label splash-screen idle 300 220 "Tamias"
	      :width 300 :height 100 :color tamias.colors:+black+
	      :scale-text t)


;;(add-state-ui-element 'gui-tester 'idle (make-ui-label :x 300 :x-init 300 :y 300 :y-init 300 :width 70 :width-init 70 :height 134 :height-init 134 :label  :hidden `,'rend-text))


