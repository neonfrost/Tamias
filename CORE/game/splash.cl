(define-state splash-screen t 'idle)
(set-state splash-screen 'idle)

(defvar splash-ticks 0)

(defvar splash-seconds 5)

(state.init-ui splash-screen 'idle)

(set-bg-color tamias.colors:+black+)
(tamias:set-resolution 0)

(ui.add-label splash-screen idle 236 200 "Made with"
	      :width 70 :height 16 :color tamias.colors:+black+)

(ui.add-label splash-screen idle 300 220 "Tamias"
	      :width 300 :height 100 :color tamias.colors:+black+
	      :scale-text t)

(states:def-logic (splash-screen idle)
		  (incf splash-ticks)
		  (when (>= splash-ticks (* splash-seconds tamias:fps))
		    (set-state gui-tester)
		    (if (eq (mod splash-ticks 40) 0)
			(print 'succ?))))
