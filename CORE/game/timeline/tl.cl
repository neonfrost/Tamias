(define-state time-line t :idle)

(defstruct tl-actor
  (name "" :type string)
  (children nil :type list)
  (description "" :type string))

(defstruct tl-event
  (name "" :type string)
  (children nil :type list)
  (actors nil :type list)
  (description "" :type string))

;;2 listers, one for events, one for 'actors' aka participants
;;1 text-field, non-editable for the 'viewer', editable for the 'maker'
#|
TL, Editor
+--------------------------------------------------------+
|Major/minor events| 'Actors' during | Descriptions 	 |
|	     	   | these events    |  of the event 	 |						|
|		   | 	   	     |	or actor selected|
+--------------------------------------------------------+
|Add-button Rm-butt| add-butt rm-butt| Current Event     |
|Sub-ev            | sub-actor       |
|Mv up Mv down  P/C| up   down  P/C  |   Current Actor   |
+--------------------------------------------------------+
Current-Event - default = "None Selected"
Current-Actor - default = "None Selected"

P/C - parent / child buttons, moves up/down a level

Buttons: New Event , Remove Event , New Actor , Remove Actor
         Add sub-event, remove sub-event
...still need to make that GUI maker lol
...and the style sheets

|#
