Timeline is a somewhat simple, but not trivial, program:

There are at least three sections of the 'viewer'

+--------------------------------------------------------+
|Major/minor events| 'Actors' during | Descriptions 	 |
|	     	   | these events    |  of the event 	 |						|
|		   | 	   	     |	or actor selected|
+--------------------------------------------------------+

Version Alpha can just be a crappy list that doesn't look great


Version "1.0" will have collapsable labels and stuff

So, ex:

[-] World War 2
   [+]Molotov-Ribbentrop Pact
   [+]Battle of Stalingrad
   [+]Pearl Harbor
[+]Invasion of Iraq
[-]Financial Crisis of 2008
   [+]Subprime mortgages

for the "Events" Section
and similar for the actors

[using WW2 - Molotov-Ribbentrop Pact]

[-]Soviets
   -Joseph Stalin
[+]Nazis
[+]Polish

And then, assuming Financial Crisis of 2008 - Lehman Brothers

The Lehman Brothers was a financial firm in the United States of America.
They [description of the actions that Lehman Brothers took that helped instigate the financial crisis]
