The variable itself is a structure?
So that (defvar x 10)
creates a variable structure named X with the slot of :value 10?
...That...sounds like both a good idea and an incredibly stupid idea




(define-hash-symbol ...) requires a character and a corresponding body to be evaluated

Has UTF-8 from the getgo
would allow for
(defmacro определить-переменную (вар валю)
   `(defvar ,вар ,валю))
(defmacro деф-вар (вар валю)
   `(defvar ,вар ,валю))

after comments
Unless enclosed in quotes, Rangifer scrubs all newlines
Delimit by space and ) __right parentheses__
so that (defun x
yz () (print "Hi #\newline"))
would declare the function xyz, which would print:
Hi

So insane
So stupid
I love it

Garbage collection: (again, not until after versoin 0.1, where the compiler is written in ELK)
static vs dynamic (need more info on it)
Heap analysis:
Check if an object, i.e. structure, is allocated, accessed, and deleted
if allocation occurs but there is no way for the structure to be deleted, raise an error
if allocatoin occurs and access occurs with one "run through", but no deletion, raise an error
allocate, run through, and exit, but no deletoin, raise error

(defstruct elk-object
  allocated)
(defstruct memory-table
  (max-size 512)
  allocated-objects)


assembly:

array_bound_check:
	mov rax [rbp + 16] ;size of array
	mov rbx [rbp + 24] ;access index
	cmp rax, rbx ;rax ?= rbx 
	jl array_bound_check.success
	jge array_bound_check.fail
	.fail:
		jmp bound_check_fail_message ; write "Out of bounds access!" to stdout
	.success: 
		pop rax ; pop return address into rax
		push 0
		jmp rax


In-function macros
(defun make-character (char-name)
       #Mname-character (if (find-if #'num-char-p char-name)
              		    (let ((positions (get-positions #'num-char-p char-name)))
			    	 (loop for position in (reverse positions)
				       do (setf (aref char-name position) "")))
			    (else char-name))   M#
    (return (name-character char-name)))


Optimizing
On Function entry:
Gather variables as they appear and keep a "state" for each of them
(defstruct function-variable
  name
  state
  changed)
On function exit, check if each variable has changed
If a variable has changed, do nothing
If a variable has NOT changed, remove variable declaration (excluding function args), replace each occurence of variable with it's state
(defun xyz (l m n o)
       #iVarA 10
       #iVarB 10
       (set VarA (+ l m n o VarB))
       (set l (+ l VarB)))
Would remove #iVarB 10
and (set...) would become
(set VarA (+ l m n o 10))
and 
(set l (+ l 10))
if VarB was 0, it would simply remove all references of VarB and remove (set l (+ l))
