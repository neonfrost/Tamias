There will be a file that contains the current files being used with a project

It'll be a 'gimped' version of ASDF

(D "World"
 (D "Cave 1 area"
  (D "Minotuar"
   (t "minotaur.define"
      "minotaur.render"
      "minotaur.logic"))
  (D "Armored Minotuar"
   (t "armored minotaur.minotaur"
      "armored minotaur.render"
      "armored minotaur.logic"))
  (D "Ninja sombrero"
   (t "ninja sombrero.define"
      "ninja sombrero.render"
      "ninja sombrero.logic"))
  (D "Scenes"
   (t "room-1.scene"
      "ninja.cutscene"
      "room-2.scene"))))
 (D "Area 2"
  (t "f1" "f2" "f3" "f4")))

D -> directory, t -> tamias script file, l -> common lisp file, i -> image/sprite sheet, s -> sound, m -> music, and so on
