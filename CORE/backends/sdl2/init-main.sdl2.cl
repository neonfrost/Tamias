#|
starting sequence
(asdf:load-system "Kitchen Craze") ;;note: in future projects, use hyphens always, no spaces

(ql:quickload :sdl2) (ql:quickload :sdl2-image) (ql:quickload :sdl2-mixer) 
(load "Main.lisp") (main)

(proclaim '(optimize (speed 3) (debug 0)))
|#
#|(defun init-engine ()
  (load "engine.cl")
)|#

(defun init ()
  (when tamias:started?
    (when quit-functions
      (loop for func in quit-functions
	    do (eval func)
	       (when (eq (car func) 'render:remove-buffer)
		 (setf quit-functions (remove func quit-functions)))
	    )))
  (setf tamias:started? t)
  (when init-functions
    (loop for func in init-functions
	  do (eval func)))
  (tamias:load-font)
  ;;skeleton code
  (initialize-assets))

(defun tamias:set-window-size (window width height)
  (sdl2:set-window-size window width height))

(defun tamias:set-default-window-size (&optional width height)
  (tamias:set-window-size tamias:default-window width height))

(defun tamias:change-resolution (by-method indice-or-width &optional height (window tamias:default-window))
  (case by-method
    (:list (tamias:set-resolution indice-or-width window))
    (:dimensions (tamias:set-window-size window indice-or-width height))))

(defun tamias:set-window-title (title)
  (let ((g-title title))
    (when (not (stringp title))
      (setf g-title (write-to-string title)))
    (setf tamias:game-title g-title))
  (sdl2:set-window-title tamias:default-window tamias:game-title))

(defun tamias:set-scale-factor (&optional (width 640) (height 480))
  (setf tamias:scale-factor (vector width height)
	tamias:screen-width width
	tamias:screen-height height
	tamias:target-resolution-changed nil)
  (sdl2-ffi.functions:sdl-render-set-logical-size tamias:renderer
						  width height)
  (sdl2-ffi.functions:sdl-render-set-scale tamias:renderer
					   (* 1.0 (/ tamias:window-width width))
					   (* 1.0 (/ tamias:window-height height))))
;;https://gamedev.stackexchange.com/questions/126395/how-to-handle-different-resolutions-in-sdl2
;;had to use set-scale because it wasn't working as expected

;;Lem uses SDL2, so looking though their source code to see how they handled various
;;things with SDl2 is a great option :)
;;https://github.com/lem-project/lem/tree/main/frontends/sdl2

(defun tamias:main ()
  (timer:make :end 1600 :in-seconds? t)
  (make-random-state)
  (setf tamias:using-gui t)
;;  (if tamias:using-gui
  ;;      (tamias:load-gui))
  (setf (modifiers-control modifiers) nil)
  (sdl2:with-init (:everything)

    #|
    (sdl2-mixer:init :ogg)
    (sdl2-mixer:open-audio tamias.audio:khz
			   :s16sys
			   tamias.audio:channels
			   tamias.audio:buffer-size)
|#
;;    (sdl2-mixer:allocate-channels 2)
    (setf tamias:default-window (sdl2:create-window :title tamias:game-title
					     :w (elt (nth tamias:resolution tamias:resolution-list) 0)
					     :h (elt (nth tamias:resolution tamias:resolution-list) 1)
					     :flags '(:shown :resizable)))
    (sdl2:with-renderer (tamias:renderer tamias:default-window :flags '(:accelerated))
      ;;add in conditional after Oreortyx is finished, ie (if not probe-file oreortyx, blah blah)
      ;;    (setf tamias:renderer (sdl2:get-renderer tamias:default-window))
      (update-window-size)
      (sdl2:set-render-draw-blend-mode tamias:renderer 1)
      (tamias:set-scale-factor)
      (init)

      (sdl2:stop-text-input)
      ;;check if using GUI
      (tamias:fps tamias:fps)
      (setf tamias-key-queue nil)
      (event-loop)
      ;;(sdl2:destroy-renderer tamias:renderer)
      ;;(sdl2:destroy-window tamias:default-window)
      )))

(defvar garbage-timer (timer:make :end 120 :reset? t :timers nil))
(defun idle-loop ()
  (sdl2:set-render-draw-color
   tamias:renderer
   (elt tamias:render-clear-color 0)
   (elt tamias:render-clear-color 1)
   (elt tamias:render-clear-color 2) 255)
  (sdl2:render-clear tamias:renderer)
  (engine-loop)
  (setf t.process-input t)
  ;;process input-timers
  (sdl2:render-present tamias:renderer)
  (tmp-proc)
  (when (timer:process garbage-timer)
    (gc :full t))
  (sdl2:delay tamias:update-time)
  )
  

(defun event-loop ()
  (sdl2:with-event-loop (:method :poll)
    (:idle ()
	   (idle-loop))
    (:windowevent (:event event)
		  (declare (ignore event))
		  nil)
    ;;https://wiki.libsdl.org/SDL2/SDL_WindowEventID
    ;;https://github.com/lem-project/lem/tree/main/frontends/sdl2
    (:keyup (:keysym keysym)
	    (let ((key (sdl2:scancode keysym)))
	      (when (not (modifier-key? key :up))
		(keyup-check (sdl2:get-key-name (sdl2:sym-value keysym))))))
    (:keydown (:keysym keysym)
	      (let ((key (sdl2:scancode keysym)))
		(when (not (modifier-key? key :down))
		  (keydown-check (sdl2:get-key-name (sdl2:sym-value keysym))))))
    (:mousebuttondown (:button m-button)
		      (mouse-button-check m-button)
		      (print *t-mouse*))
    (:mousebuttonup (:button m-button)
		    (mouse-button-release-check m-button))
    (:mousemotion (:state b-state :x x :y y :xrel xrel :yrel yrel)
		  (mouse-move b-state x y xrel yrel))
    (:textinput (:text text)
		(setf tamias-text-insert text)
		;;needs a mild amount of debugging, e.g. what does the entire text look like?
		;;also need to add in ctrl, shift, alt, etc. input for text fields (e.g. ctrl + v)
		;;HAHA! When a non-shift modifier is in play, it defaults to :keydown event
		;;THIS means that (text-key-check key) can handle any non-shift modifier
		;;or, a combo shift + mod + character is also used!
		)
    (:quit ()
;;	   (tamias.audio:quit-audio)
	   (when quit-functions
	     (loop for func in quit-functions
		   do (eval func)
		      (when (eq (car func) 'render:remove-buffer)
			(setf quit-functions (remove func quit-functions)))
		   ))
	   (setf tamias:started? nil)
	   (kill-textures)
	   t)))


(defun kill-textures ()
  (loop for texture in tamias:text-buffers
     do (when texture
	  (render:remove-buffer texture)))
  (setf tamias:text-buffers nil)
  (loop for asset in tamias-assets
     do (case (cadr asset)
	  ((image texture) (sdl2:destroy-texture (eval (car asset))))
	  ((sprite-sheeet tile-sheet) (sprite:free-sheet (eval (car asset))))))
  (when tamias:font
    (sdl2:free-surface tamias:font)
    (setf tamias:font nil)))
