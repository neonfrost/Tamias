#|
==============================================================================
                              TEXT AND STRINGS
==============================================================================
|#
(in-package :render)


(defvar font-color (vector 255 255 255 0))

(defmacro define-buffer (buffer)
  `(progn (push ,buffer tamias:text-buffers)
	  (defvar ,buffer nil)))

(export 'remove-buffer)

(defmacro reset-text-buffer (buffer)
  `(if ,buffer
       (progn (sdl2:destroy-texture ,buffer)
	      (setf ,buffer nil))))

(defmacro remove-buffer (buffer)
  `(progn (reset-text-buffer ,buffer)
;;	  (setf tamias:text-buffers (remove ,buffer tamias:text-buffers))
	  ))

(defun set-buffer (str buffer)
  (let ((dim (text-dimensions str)))
    (reset-text-buffer buffer)
    (when (eq (car dim) 0)
      (setf (car dim) 1))
    (tamias.string:create-text-buffer str :width (car dim) :height (cadr dim))))

(defmacro to-buffer (str buffer)
  `(setf ,buffer (set-buffer ,str ,buffer)))
  
(defun text-buffer (buffer source destination &optional color)
  (when color
    (sdl2:set-texture-color-mod buffer (elt color 0) (elt color 1) (elt color 2)))
  (unless source
    (setf source (list 0 0 (sdl2:texture-width buffer) (sdl2:texture-height buffer))))
  (when (< (length destination) 4)
    (setf destination (append destination (list (sdl2:texture-width buffer) (sdl2:texture-height buffer)))))
  (with-rectangle src source
    (with-rectangle dest destination
      (sdl2:render-copy tamias:renderer
			buffer
			:source-rect src
			:dest-rect dest))))


#|
This was for the roguelike I was/am working on
|#

#|
Pointless redefinition, commented out for quick reference

(defun text-width (text)
  (* (length text) (car tamias.string:character-size))
  )
(defun text-height (text)
  (let ((new-line-count (1+ (count #\newline text))))
    (* new-line-count (cadr tamias.string:character-size))))
(defun text-dimensions (text)
  (if (symbolp text)
      (setf text (write-to-string text)))
  (if (stringp text)
      (let ((width (text-width text))
	    (height (text-height text)))
	(list width height))
      (list 0 0)))
|#

(defun render-string (str x y &key width height string-width string-height (rotate 0) (color font-color) anti-alias)
  (let ((buffer-width
	  (or width
	      (when (find #\newline str)
		(* (position #\newline str) (car tamias.string:character-size)))
	      (* (length str) (car tamias.string:character-size))))
	(buffer-height
	  (or height
	      (* (1+ (count #\newline str))
		 (cadr tamias.string:character-size))))
	(width (or width
		   string-width
		   0))
	(height (or height
		    string-height
		    0)))
    (if (> (length str) 0)
	(let* ((string-buffer (tamias.string:create-text-buffer str
								:width buffer-width
								:height buffer-height))
	       (source-width (or
			      width
			      (sdl2:texture-width string-buffer)))
	       (source-height (or
			       height
			       (sdl2:texture-height string-buffer)))
	       (destination-width (or string-width
				      source-width))
	       (destination-height (or string-height
				       source-height)))
	  (tex-blit string-buffer :src (create-rectangle (list 0 0 source-width source-height))
		    :dest (create-rectangle (list x y destination-width destination-height))
		    :color color
		    :angle rotate)
	  (when anti-alias
	    (let ((xy-mods '((-1 0) (1 0) (0 -1) (0 1)))
		  (alpha-color (vector (elt color 0) (elt color 1) (elt color 2) 125)))
	      (loop :for pr :in xy-mods
		    :do (tex-blit string-buffer :src (create-rectangle (list 0 0 source-width source-height))
						:dest (create-rectangle (list (+ x (car pr)) (+ (cadr pr) y) destination-width destination-height))
						:color alpha-color
						:angle rotate)
#|		(tex-blit string-buffer :src (create-rectangle (list 0 0 source-width source-height))
			  :dest (create-rectangle (list (1- x) y destination-width destination-height))
			  :color alpha-color
			  :angle rotate)
		(tex-blit string-buffer :src (create-rectangle (list 0 0 source-width source-height))
			  :dest (create-rectangle (list (1+ x) y destination-width destination-height))
			  :color alpha-color
			  :angle rotate)
		(tex-blit string-buffer :src (create-rectangle (list 0 0 source-width source-height))
			  :dest (create-rectangle (list x (1- y) destination-width destination-height))
			  :color alpha-color
			  :angle rotate)
		(tex-blit string-buffer :src (create-rectangle (list 0 0 source-width source-height))
			  :dest (create-rectangle (list x (1+ y) destination-width destination-height))
			  :color alpha-color
			  :angle rotate)
|#
		    )))
	  (reset-text-buffer string-buffer)))))

(defun determine-string-width-nlc (str)
  (let ((h (count #\newline str))
	(longest (length str))
	(current-line-length 0))
    (if (> h 0)
	(loop for idx below (length str)
	   do (if (char= (aref str idx) #\newline)
		  (if (> current-line-length longest)
		      (setf longest current-line-length
			    current-line-length 0)
		      (setf current-line-length 0))
		  (incf current-line-length)))
	(incf h))
    ;;ensures that longest does not return 0, if there is no newline character
    (if (> current-line-length longest)
	(setf longest current-line-length))
    (values longest h)))

(defun text (str x y &key width height (rotate 0) (color font-color) scale origin-is-center)
  (let ((src-width 0)
	(src-height 0))
    (when tamias:ttf-font
	(let* ((str-surf (sdl2-ttf:render-text-solid tamias:ttf-font str
						     255 255 255 0))
	       (str-tex (sdl2:create-texture-from-surface tamias:renderer
							  str-surf)))
	  (sdl2:free-surface str-surf)
	  (tex-blit str-tex :dest (create-rectangle (list x y width height))
		    :color color
		    :angle rotate)
	  (sdl2:destroy-texture str-tex)))
    (unless tamias:ttf-font
      (let ((dim (text-dimensions str)))
	(setf src-width (car dim)
	      src-height (cadr dim)))
      (unless width
	(setf width src-width))
      (unless height
	(setf height src-height))
      (when scale
	(let ((string-width width)
	      (string-height height))
	  ;; stretches the text to the scale
	  (setf width (round (* string-width scale))
		height (round (* string-height scale))
		x (- x (round (/ width 2)))
		y (- y (round (/ height 2))))
	  ))
      (when origin-is-center
	;;Imagine a box of text
	;;Take a pin and put it in the center.
	;;This is the destination of the /center/ of the text
	;;To determine where it would be rendered at, you take the width and height
	;;    divide by 2, round, and then reduce the x and y respecitively
	;;So, a text "object" of 120 width and 32 height would have the
	;;  X and Y at 60 and 16 respectively. This makes text placement easier
	(decf x (round (/ width 2)))
	(decf y (round (/ height 2)))))
      ;;(render-string str x y :width width :height height :dest-width dest-width :dest-height dest-height :rotate rotate :color color :anti-alias t))
      ;;      (render-string str x y :width width :height height :dest-width dest-width :dest-height dest-height :rotate rotate :color color :anti-alias t)))
    (render-string str x y :width src-width :height src-height :string-width width :string-height height :rotate rotate :color color :anti-alias t)))


