(defun update-window-size ()
  (setf tamias:screen-width (elt (nth tamias:resolution tamias:resolution-list) 0)
	tamias:screen-height (elt (nth tamias:resolution tamias:resolution-list) 1))
  (sdl2:set-window-size tamias:default-window tamias:screen-width tamias:screen-height)
;;  (gl:viewport 0 0 tamias:screen-width tamias:screen-height)
  )

(defun tamias:load-image (file-path &optional surface?)
  (if surface?
      (sdl2-image:load-image file-path)
      (sdl2:create-texture-from-surface tamias:renderer (sdl2-image:load-image file-path))))

(defun tamias:make-rect (x y width height)
  (sdl2:make-rect x y width height))

(defmacro tamias:free-image (img)
  `(sdl2:destroy-texture ,img))


(in-package :render)
(export 'screenshot)
#|
==============================================================================
                                  GENERAL
==============================================================================
|#

(defun line (x y x2 y2 &optional (color '(255 255 255 255)))
  (let ((r (elt color 0))
	(g (elt color 1))
	(b (elt color 2))
	(a (elt color 3)))
    (sdl2:set-render-draw-color tamias:renderer r g b a)
    (sdl2:render-draw-line tamias:renderer x y x2 y2)))


(defun create-texture (&key (format sdl2:+pixelformat-rgba8888+) (access 0) (width 16) (height 16) (color tamias.colors:+white+))
  (sdl2:set-render-draw-color tamias:renderer (elt color 0) (elt color 1) (elt color 2) (elt color 3))
  (sdl2:render-clear tamias:renderer)
  (sdl2:create-texture tamias:renderer format access width height))

(defmacro with-rectangle (name rect-parameters &body body)
  `(let ((,name (sdl2:make-rect (car ,rect-parameters)
				(cadr ,rect-parameters)
				(caddr ,rect-parameters)
				(cadddr ,rect-parameters))))
     ,@body
     (sdl2:free-rect ,name)))

(defun screenshot ()
  (let* ((w tamias:window-width)
	 (h tamias:window-height)
	 (s-shot (sdl2:create-rgb-surface w h 32)))
    (with-rectangle screen-rect (list 0 0 w h)
      (sdl2-ffi.functions:sdl-render-read-pixels
       tamias:renderer screen-rect
       ;;sdl2:+pixelformat-rgba8888+
       sdl2:+pixelformat-argb8888+
       (sdl2:surface-pixels s-shot) (sdl2:surface-pitch s-shot))
      (sdl2-image:save-png s-shot "screenshot.png"))
    (sdl2:free-surface s-shot)
  ))

#|
usage: (with-rectangle bat-rect (list (bat-x bat) (bat-y bat) (bat-width bat) (bat-height bat))
(render:rectangle bat-rect))
|#

(defmacro create-rectangle (rect-vals)
  `(sdl2:make-rect (car ,rect-vals)
		   (cadr ,rect-vals)
		   (caddr ,rect-vals)
		   (cadddr ,rect-vals)))
#|
usage: (defun some-func ()
          (let ((my-rect (create-rectangle '(x y width height))))
            (render:rectangle my-rect)
            (render:free-rectangle my-rect)))
|#

(defmacro free-rectangle (rect)
  `(sdl2:free-rect ,rect))

(defun image (img &optional (x 0) (y 0) w h)
  (when img
    (let ((src (sdl2:make-rect 0 0 (sdl2:texture-width img) (sdl2:texture-height img)))
	  (dest (sdl2:make-rect x y
				(or w (sdl2:texture-width img))
				(or h (sdl2:texture-height img)))))
      (sdl2:render-copy-ex tamias:renderer
			   img
			   :source-rect src
			   :dest-rect dest)
      (free-rectangle src)
      (free-rectangle dest))))

(defun tex-blit (tex &key src dest color angle center (flip :none))
  (when color
    (sdl2:set-texture-color-mod tex (elt color 0) (elt color 1) (elt color 2))
    (when (eq (length color) 4)
      (sdl2:set-texture-alpha-mod tex (elt color 3))))
  (let ((src (or src (sdl2:make-rect 0 0 (sdl2:texture-width tex) (sdl2:texture-height tex)))))
    (sdl2:render-copy-ex tamias:renderer
			 tex
			 :source-rect src
			 :dest-rect dest
			 :angle angle
			 :center center
			 :flip (list flip))
    (sdl2:free-rect src)
    (sdl2:free-rect dest))
#|
      (progn (sdl2:render-copy-ex tamias:renderer
				  tex
				  :source-rect src
				  :dest-rect dest
				  :angle angle
				  :center center
				  :flip (list flip))
	     (sdl2:free-rect src)
	     (sdl2:free-rect dest))
|#
  )

(defun box (x y w h &optional (color (vector 255 255 255 255)) (filled t))
  (unless (or (vectorp color)
	      (listp color))
    (setf color (eval color)))
  (let* ((r (elt color 0))
	 (g (elt color 1))
	 (b (elt color 2))
	 (a (or (elt color 3)
		255))
	 (rect (sdl2:make-rect x y w h))) ;; I still want to figure out a way to better manage memory with Tamias, currently, a rectangle is made and then freed every frame
    ;;set the render draw color to the color
    (sdl2:set-render-draw-color tamias:renderer r g b a)
    (if filled
	(sdl2:render-fill-rect tamias:renderer rect) ;;fills the area of x -> w and y -> h with color (default white)
	(sdl2:render-draw-rect tamias:renderer rect)) ;;draws 4 lines, x -> , x -> w, y -> w, h -> w with color (default white)
    (sdl2:free-rect rect))) ;;frees memory taken up by the rect. Not doing this will cause a segfault

(defun render-rectangle (rect &optional (color (vector 255 255 255 255)) filled)
  (box (elt rect 0) (elt rect 1)
       (elt rect 2) (elt rect 3)
       color filled))

(defun rectangle (w y width height &optional (color (vector 255 255 255 255)) filled)
  (box x y
       width height
       color filled))


(defun pixel (x y &optional (color (vector 255 255 255 255)))
  (box x y 1 1 :color color))

