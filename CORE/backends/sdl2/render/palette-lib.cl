#|
======================================================================================
                       PALETTE & PALETTE ACCESSORIES
======================================================================================
|#
(defmacro define-palette (var fg bg fnt clear-color)
  `(defvar ,var (list ,fg ,bg ,fnt ,clear-color)))
(define-palette default-palette "d5aaaa" "866d5c" "fff3e3" "114f47")
(define-palette fields tamias.colors:+brown+ tamias.colors:+green+ tamias.colors:+white+ tamias.colors:+pastel-blue+)
(define-palette icy-wind "294552" "597884" "acc4ce" "9eb9b3")
(define-palette pink! tamias.colors:+pastel-pink+  tamias.colors:+black+ tamias.colors:+pastel-pink+ "005a5c")
(defvar palettes '(default-palette fields icy-wind pink!))
(defvar palette nil)
(defvar current-palette 0)
(defvar fg-palette (nth 0 palette))
(defvar bg-palette (nth 1 palette))
(defvar font-palette (nth 2 palette))

(defun update-palette ()
  (setf fg-palette (nth 0 palette)
	bg-palette (nth 1 palette)
	font-palette (nth 2 palette)
	render:font-color font-palette
	tamias:render-clear-color (nth 3 palette)))
(defun change-palette (var)
  (let ((fg (nth 0 var))
	(bg (nth 1 var))
	(fnt (nth 2 var))
	(clear-color (nth 3 var)))
    (when (stringp fg)
      (setf fg (tamias.string:parse-hex-color fg :return-type 'list)))
    (when (stringp bg)
      (setf bg (tamias.string:parse-hex-color bg :return-type 'list)))
    (when (stringp fnt)
      (setf fnt (tamias.string:parse-hex-color fnt :return-type 'list)))
    (when (stringp clear-color)
      (setf clear-color (tamias.string:parse-hex-color clear-color :return-type 'list)))
    (setf palette (list fg bg fnt clear-color))
    (update-palette)))

(change-palette default-palette)

