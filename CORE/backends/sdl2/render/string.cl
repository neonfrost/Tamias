#|
On rendering strings, either use a ttf file and use a 'buffer' with the strings (rendering a string through sdl-ttf requires many calculations and procedures to take place, resulting in /massive/ slowdown if done every frame update) or use a tile sheet with 256 characters on it, take the ascii character code of each character of the string (i.e. a = 58), divide by 16, with the quotient being used for the row and the remainder being used for the column (60/16 = 3 12/16 = R:3 C:12) cell and then render it to either a buffer (recommended) or to the renderer
|#
(in-package :tamias.string)


(defmacro blit (src-surface src-rect dest-surface dest-rect)
  `(sdl2:blit-surface ,src-surface ,src-rect ,dest-surface ,dest-rect))



#|
(defun start-string (str &rest strs)
  (loop for s in strs
       do (setf str (with-output-to-string (stream)
		      (write-string str stream)
		      (terpri stream)
		      ))
       (setf str (concatenate 'string str s)))
  str)
|#
(defun render-character-to-buffer (cell x y buffer &key (color (list 255 255 255)))
  (let ((src-rect (sdl2:make-rect (* (cadr cell) (car character-size))
				  (* (car cell) (cadr character-size))
				  (car character-size)
				  (cadr character-size)))
	(dest-rect (sdl2:make-rect x
				   y
				   (car character-size)
				   (cadr character-size)))
	(temporary-surface (sdl2:create-rgb-surface (car character-size) (cadr character-size) 32))
	(tmp-rect (sdl2:make-rect 0 0 (car character-size) (cadr character-size))))
    (blit tamias:font src-rect temporary-surface tmp-rect)
    (sdl2:set-color-mod temporary-surface (car color) (cadr color) (caddr color))
    (blit temporary-surface tmp-rect buffer dest-rect)
    (sdl2:free-surface temporary-surface)
    (sdl2:free-rect src-rect)
    (sdl2:free-rect dest-rect)
    (sdl2:free-rect tmp-rect)
    ))



(defun create-text-buffer (string &key (width 256) (height 256) to-surface);;yeah, this needs to be changed significantly
  ;;some of the code below, that will be gone by the time somebody else reads this, was being used for a rogue-like I was developing
  ;;or rather still developing, but not very much has been developed for a while
  (when (= width 0)
    (setf width 1))
  (let ((buff (sdl2:create-rgb-surface width height 32))
	(cell-row 0)
	(cell-column 0)
	(texture nil)
	(x 0)
	(y 0))
    #|
Make a text buffer, character by character
When the current character is a newline, don't render, incf mod-y and set x to 0
;; what the fuck are mod-y and mod-x for?
;;mod-x is the current x I think?
|#
    (dotimes (n (length string))
      (let ((cur-char (aref string n)))
	(setf (values cell-row cell-column) (truncate (char-code cur-char) 16))
	;;Get the row and column for the font sprite-sheet
	(when (eq cur-char #\newline)
	  (progn (setf x 0)
		 (incf y)))
	(when (not (eq cur-char #\newline))
	  ;;truncate has a whole number and a remainder
	  ;;basically long divisiion
	  (when (and (eq (mod n (/ width (car character-size))) 0)
		     (> x 0))
	    (setf x 0)
	    (incf y 1))
	  ;;why is...what? what the fuck does this do?
	  
	  ;;Now we actually render the character
	  (render-character-to-buffer (list cell-row cell-column)
				      (* x (car character-size))
				      (* y (cadr character-size))
				      buff
				      :color (list 255 255 255))
	  (incf x))))
    (sdl2:set-color-key buff 1 0)
    (if to-surface
	buff
	(progn (setf texture (sdl2:create-texture-from-surface tamias:renderer buff))
	       (sdl2:free-surface buff)
	       (setf buff nil)
	       texture))))

(defun font-to-surface (font)
  ;;to be implemented: making ttf fonts easier and better to use
  ;;Idea: loop through each ascii character (loop for char below 256) and push the rendered character to a surface that is then used as the font-sheet for the program
  )
;;this will need to be updated to support utf-8

