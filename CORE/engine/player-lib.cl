(defstruct tamias-player
  name
  (input (make-hash-table))
  (number 1))
#h tamias-players

(defmacro create-tamias-player (&optional (name "tamias-player-n") (number 1))
  `(let ((player (make-tamias-player :name ,name :number ,number)))
     (setf (gethash :down (tamias-player-input player)) (make-hash-table))
     (setf (gethash :up (tamias-player-input player)) (make-hash-table))
     (setf (gethash ,number tamias-players) player)))

(defun get-player-key (key (key-state &key ctrl alt shift))
  (let ((mask (t.input:kb-key-masks)))
    (eval (gethash key (gethash mask (gethash key-state (tamias-player-input player)))))))
    

(defmacro add-player-key (player key (key-state &key ctrl alt shift) &body body)
  `(let ((mask (t.input:kb-key-masks)))
     (setf (gethash key (gethash mask (gethash key-state (tamias-player-input player)))) `,',@body)))
