(defpackage vector-3d
  (:use :cl)
  (:nicknames :vec3d)
  (:export make
	   x
	   y
	   z
	   p
	   add
	   sub
	   multiply
	   scalar
	   dot
	   set!
	   set-values))

(in-package :vec3d)

(defun make (&key (x 0) (y 0) (z 1))
  (vector x y z))

(defmacro x (vec)
  `(elt ,vec 0))
(defmacro y (vec)
  `(elt ,vec 1))
(defmacro z (vec)
  `(elt ,vec 2))

(defmacro p (vec)
  `(if (and (vectorp ,vec)
	    (eq (length ,vec) 3))
       t
       nil))

(defun vec3d-p (vec)
  (p vec))

(defun add (vec1 vec2)
  (values (+ (x vec1) (x vec2))
	  (+ (y vec1) (y vec2))
	  (+ (z vec1) (z vec2))))

(defun sub (vec1 vec2)
  (values (- (x vec1) (x vec2))
	  (- (y vec1) (y vec2))
	  (- (z vec1) (z vec2))))

(defun multiply (vec1 vec2)
  (values (+ (* (x vec1) (x vec2))
	     (* (x vec1) (y vec2))
	     (* (x vec1) (z vec2)))
	  (+ (* (y vec1) (x vec2))
	     (* (y vec1) (y vec2))
	     (* (y vec1) (z vec2)))
	  (+ (* (z vec1) (x vec2))
	     (* (z vec1) (y vec2))
	     (* (z vec1) (z vec2)))))

(defun scalar (vec1 scalar)
  (values (if (> (x vec1) 0)
	      (floor (* (x vec1) scalar))
	      (ceiling (* (x vec1) scalar)))
	  (if (> (y vec1) 0)
	      (floor (* (y vec1) scalar))
	      (ceiling (* (y vec1) scalar)))
	  (if (> (z vec1) 0)
	      (floor (* (z vec1) scalar))
	      (ceiling (* (z vec1) scalar)))))

(defun dot (vec1 vec2)
  (+ (* (x vec1) (x vec2))
     (* (y vec1) (y vec2))
     (* (z vec1) (z vec2))))

(defun set! (vec &rest vec-vals)
  (loop for el below (length vec-vals)
	do (setf (elt vec el) (elt vec-vals el))))

(defun set-values (vector &optional (function :add) (vec2 1))
  (case function
    (:add (setf (values (x vector) (y vector) (z vector))
	       (add vector vec2)))
    (:subtract (setf (values (x vector) (y vector) (z vector))
		    (sub vector vec2)))
    (:scalar (setf (values (x vector) (y vector) (z vector))
		   (scalar vector vec2)))
    (:multiple (setf (values (x vector) (y vector) (z vector))
		     (multiply vector vec2)))))
