;;Generics and Defaults

(defgeneric object.offscreen (obj-type screen-keyword)
  (:method (obj-type screen-keyword)
    (declare (ignore obj-type screen-keyword))
    nil))

(defgeneric entity.offscreen (entity-type screen-keyword)
  (:method (entity-type screen-keyword)
    (declare (ignore entity-type screen-keyword))
    nil))

(defgeneric entity.handle (entity entity-type)
  (:method (entity entity-type)
    (declare (ignore entity entity-type))
    ;;handle-state
    ;;handle-logic
    ;;handle-render
    nil))

(defgeneric entity.handle-state (entity e-state)
  (:method (entity e-state)
    (declare (ignore entity e-state))
    nil))

(defgeneric entity.render (entity entity-type &optional x y)
  (:method (entity entity-type &optional x y)
    (declare (ignore entity entity-type))
    nil))

(defmethod entity.render (entity (entity-type (eql :tamias-entity)) &optional x y)
  (let ((rend-x x)
	(rend-y y))
    (when (not x)
      (setf rend-x (t.e:entity-x entity)
	    rend-y (t.e:entity-y entity)))
    (if (t.e:entity-sprite-sheet entity)
	(render:cell (t.e:entity-sprite-sheet entity)
		     (t.e:entity-current-cell entity)
		     rend-x
		     rend-y)
	(render:box rend-x
		    rend-y
		    32 32
		    tamias.colors:+cobalt+))))

(defgeneric entity.logic (entity entity-type)
  (:method (entity entity-type)
    (declare (ignore entity entity-type))
    nil))


(defgeneric t-object.handle ( t-object t-object-type )
  (:method ( t-object t-object-type )
    (declare ( ignore t-object t-object-type ))
    ;;handle-state
    ;;handle-logic
    ;;handle-render
    nil))

(defgeneric t-object.render ( t-object t-object-type
			      &optional x y)
  (:method ( t-object t-object-type
	     &optional x y )
    (declare ( ignore t-object t-object-type x y ))
     nil))

(defmethod t-object.render
    (( t-object tamias.entities:t-object ) t-object-type &optional x y)
  (:before
   (print t-object )))

#|
The first part of each entity.render will be to render it as a t-object, using a sprite sheet or textureNOOOOOOOOOOOOOOOOOOOOoo!
DO NOT DO THIS, T-OBJECTS DO NOT HAVE A SENSE OF ENTITY-STATE!!!

It may be okay to do it ***after***, but DEFINITIVELY NOT ***before***

As long as the 'object' is rendered /after/ logic is applied to it, then there shouldn't be a problem

3/5/24

Well, I know that :init and :idle are the minimum values for t-objects and entities
If it ain't :init, and it ain't :idle, then you better have that state supported
|#
(defgeneric t-object.physics ( t-ojbect physic &optional t-map )
  (:method ( t-object physic )
    (declare ( ignore t-object physic ))
    nil))

(defparameter gravity-constant 10)

(defmethod t-object.physics ( t-object ( physic ( eql :gravity )) &optional t-map )
  (incf ( tamias.entities:t-object-y t-object ) gravity-constant ))

(defun t-object.map-test (t-object t-map num)
  (> ( aref t-map ( tamias.entities:t-object-y t-object )
	       (tamias.entities:t-object-y t-object)) num))

(defun t-object-physics.handle (t-object)
  (loop :for physic :in (world-physics current-world)
	:do (if (find physic (t-object-physics t-object))
	       (funcall 't-object.physics t-object physic)))
  (loop :for physic-list :in (scene-physics current-scene)
	:do (let ((phys-map (tamias-map-data (scene-physics-map current-scene)))
		  (phys (car physic-list)))
	      ;;this can be optimized to hell and back lol
	      ;;
	      (when (find phys (t-object-physics t-object))
		(when (t-object.map-test t-object phys-map (cadr physic-list))
		  (funcall 't-object.physics t-object phys phys-map))))))

(defmacro entity-physics.handle (entity)
  `(t-object-physics.handle ,entity))

#|
Examples of some physics systems: :gravity :collision :wind
|#
