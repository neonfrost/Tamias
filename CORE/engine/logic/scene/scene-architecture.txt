Original from bitbucket:

Engine architecture:
State
 ->sub-state
  ->'world' -> 'area'
    ->room\scene
World contains procedures that will affect every object, or every object that inherits a particular class
That is where the physics, render and other engines would be located at
An area is a collection of scenes, so it manages music and other area/level specific things
Scene finally contains everything else. t-objects, entities, collision maps, etc.

I had forgotten about the layers struct, and now I'm not 100% sure how I want to use it (using them for the render engine might be the best, i.e. 



Ids of -1 are the engines understanding that there is no thing there (This will allow for any objects of ID -1 to be displayed with an error image [e.g. purple checkerboard in a number of 3d games]). This will affect all objects within a Tamias instance



rooms/scenes will contain all object-ids located within a scene
rooms/scenes allow for various physics to be present within a scene

Portals are a generic (in non-prog speak) object that allows for a player to move between scenes. 

Areas are meant to be an easier way to manage various resources (i.e. unloading and loading assets)
while the Area struct has a music slot, it is not absolutely necessary for there to be music present (I would recommend it, so that you don't have to worry about the music restarting when going from scene to scene.)

