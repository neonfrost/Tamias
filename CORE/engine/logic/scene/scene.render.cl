(defun render.tile-map ( tile-map )
  (when tile-map
    (let* (( camera ( scene-camera current-scene ))
	   ( t-map-data ( tile-map-data tile-map ))
	   ( t-map-dimensions ( array-dimensions t-map-data ))
	   ( sheet ( tile-map-sheet tile-map ))
	   ( tile-width ( car ( sprite:sheet-cell-size sheet )))
	   ( tile-height ( cadr ( sprite:sheet-cell-size sheet ))))
      (loop :for y :below ( car t-map-dimensions )
	    :do (loop :for x :below ( cadr t-map-dimensions )
		      :do (let (( tile-id ( aref t-map-data y x )))
			    (if ( > tile-id -1 )
				( render:tile sheet tile-id
					      ( * x tile-width ) ( * tile-height y ))
				)))))))

(defun render.scene-queue ()
  (loop :for scene :in ( state-scene-queue tamias:state )
	:do (let (( layer ( gethash l-id ( scene-layers scene ))))
	     (loop :for object :in ( layer-objects layer )
		   :do ( render.tile-map ( layer-tile-map layer ))
		       (let (( t-object ( gethash object ( scene-objects scene ))))
			 (if t-object
			     (if ( entity-p t-object )
				 (let (( entity t-object ))
				   ( entity.render entity entity.type ))
				 ( t-object.render t-object t-object.type ))))))))

