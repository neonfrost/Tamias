(defmacro a<b<c (a n c)
  `(and (> ,n ,a)
	(< ,n ,c)))

(defmacro a<=b<=c (a n c)
  `(and (>= ,n ,a)
	(<= ,n ,c)))

(defmacro a>b>c (a n c)
  `(and (< ,n ,a)
	(> ,n ,c)))

(defmacro a>=b>=c (a n c)
  `(and (<= ,n ,a)
	(>= ,n ,c)))

(defmacro a>b>=c (a n c)
  `(and (< ,n ,a)
	(>= ,n ,c)))
	
(defmacro length> (sequence comparator)
  `(> (length ,sequence) ,comparator))

(defmacro length< (sequence comparator)
  `(< (length ,sequence) ,comparator))

(defmacro length= (sequence comparator)
  `(= (length ,sequence) ,comparator))
