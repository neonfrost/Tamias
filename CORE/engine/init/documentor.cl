(defun tamias-write-strings-to-file (file strs)
  (unless (probe-file file)
    (with-open-file (doc-stream file :direction :output)
      (loop :for str :in strs
	    :do (write-line str doc-stream)))))

(defmacro document (documentation-file-name &rest document-strs)
  `(let ((document-name (concatenate 'string "Documentation/" (write-to-string ',documentation-file-name) ".t-doc")))
     (tamias-write-strings-to-file document-name ',document-strs)
     ))
