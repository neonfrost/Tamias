(ql:quickload '(harmony cl-mixed-vorbis
                #+linux cl-mixed-pulse))
#|
(defpackage tamias-mixer
  (:use :cl :org.shirakumo.fraf.harmony)
  (:nicknames :t.mixer)
  (:export play-music))
|#

(defpackage tamias.audio
  (:use :cl :org.shirakumo.fraf.harmony)
  (:nicknames :t.audio :t.mixer)
  (:export quit-audio
	   play
	   channels
	   khz
	   sounds
	   sounds-list
	   buffer-size
	   make-track
	   define-track
	   track-path
	   track-stream
	   track-loop-point
	   track-duration
	   duration
	   test-music))

(in-package :tamias.audio)

(defstruct sound
  path
  chunk
  channel)
(defvar current-channel -1)
(defvar sfx-volume 128)
(defvar sounds (make-hash-table))
(defvar sounds-list nil)


(defvar channels 2)
(defvar khz 44100)
(defvar buffer-size 512)

(defstruct track
  path
  stream
;;  (timer (timer:make :end 1200 :in-seconds? t))
  (loop-point 0.0)
  (duration 1.0))

(defmacro define-track (track path &key (loop-point 0.0))
  `(defvar ,track (make-track :path ,path :loop-point ,loop-point)))
#|
(defgeneric audio-duration (music-type)
  )
  
(defmethod audio-duration ((music-type 'ogg))
  )
|#
(defun duration ()
  )

