
(defpackage :tamias.aux
  (:use cl)
  (:nicknames :t.aux :aux :t.utils)
  (:export push-init
	   push-quit
	   add-init
	   add-quit
	   CATCH-ONCE
	   catch-values
	   catch-value
	   push-to-end
	   read-string
	   eval-string
	   define-enum
	   cycle-selection
	   to-keyword
	   a<b<c
	   a<=b<=c
	   a>b>c
	   a>=b>=c
	   >a>
	   use-<or>
	   length>
	   length<
	   length=
	   create-id-system
	   define-id-system
	   defun-noerr
	   if-type))
(in-package :tamias.aux)

(defmacro if-type ((obj obj-test-type) &body body)
  `(if (typep ,obj ',obj-test-type)
       ,@body))

(defmacro defun-noerr (func-name vars &body body)
  `(defun ,func-name ,vars (ignore-errors ,@body)))

(defstruct id-system
  (entries (make-hash-table))
  (ids nil :type list)
  (current-id -1 :type integer))

(defmacro create-id-system (id-name)
  `(let ((id-str (write-to-string ',id-name)))
     (let ((id-table-sym ',id-name)
	   (id-sys-entries-sym (read-from-string (concatenate 'string id-str "-entries")))
	   (id-ids-sym (read-from-string (concatenate 'string id-str "-ids")))
	   (id-current-id-sym (read-from-string (concatenate 'string id-str "-current-id")))
	   (id-get (read-from-string (concatenate 'string "get-" id-str)))
	   (id-system-new-data-func-sym (read-from-string (concatenate 'string "new-" id-str))))
       (eval `(defvar ,id-table-sym (make-id-system)))
       (eval `(define-symbol-macro ,id-sys-entries-sym
		  (id-system-entries ,id-table-sym)))
       (eval `(define-symbol-macro ,id-ids-sym 
		  (id-system-ids ,id-table-sym)))
       (eval `(define-symbol-macro ,id-current-id-sym
		  (id-system-current-id ,id-table-sym)))
       (eval `(defun ,id-get (id)
		(gethash id ,id-sys-entries-sym)))
       (eval `(defun ,id-system-new-data-func-sym (data &optional id)
		(incf ,id-current-id-sym)
		(setf (gethash (or id ,id-current-id-sym) ,id-sys-entries-sym)
		      data)
		(push (or id (eval ,id-current-id-sym)) ,id-ids-sym)))
       )))
(defmacro define-id-system (id-name)
  `(create-id-system ,id-name))

(defmacro push-init (&body func-sym-or-body)
  `(push ',@func-sym-or-body cl-user:init-functions))
(defmacro push-quit (&body func-sym-or-body)
  `(push ',@func-sym-or-body cl-user:quit-functions))
(defmacro add-init (&body func-sym-or-body) ;;add-init for when the window is open and mixer is loaded
  `(push ',@func-sym-or-body cl-user:init-functions)) ;;i.e. anything that requires allocating memory with the
(defmacro add-quit (&body func-sym-or-body) ;; backend stuff.
  `(push ',@func-sym-or-body cl-user:quit-functions)) ;;push/add-quit for freeing memory or resources that have to be reset


(defun to-keyword (obj)
  "Creates a keyword from hypothetically any object, unless it's a keyword, which will just return the keyword"
  (if (not (keywordp obj))
      (read-from-string (concatenate 'string ":" (if (not (stringp obj))
						     (remove #\' (write-to-string obj))
						     (remove #\' obj))))
      obj))

(DEFVAR CATCH-ONCE NIL)
(DEFVAR CATCH-VALUES NIL)
(DEFUN CATCH-VALUE (VALUE)
  (unless CATCH-ONCE
      (PUSH VALUE CATCH-VALUES)
      (SETF CATCH-ONCE T)))
(DEFUN CATCH-VALUES (&rest VALUE)
  (PUSH VALUE CATCH-VALUES))

(defmacro push-to-end (obj lst)
  `(if ,lst
       (push ,obj (cdr (last ,lst)))
       (push ,obj ,lst)))
;;Adapted from
;;https://stackoverflow.com/questions/13359025/adding-to-the-end-of-list-in-lisp

(defmacro read-string (str)
  `(read (make-string-input-stream ,str)))
(defmacro eval-string (str)
  `(eval (read-string ,str)))


(defmacro define-enum (&rest consts)
  `(loop :for const :in ',consts
      :do (let ((const-name (car const)) (const-val (cadr const))) (print const-name)
	      (eval `(defparameter ,const-name ,const-val)))))

;;(tam-enum (top-left 0) (top-right 1) (center 2) (bottom-left 3) (bottom-right 4))

;;selector.cl

(defmacro cycle-selection (selection direction &key (min 0) max)
  `(let ((down :down)
	 (up :up)
	 (selection ,selection)
	 ;;selection ,selection because ,selection could be (ui-selector (gethash 1 (gethash :idle (gethash :gui-tester ui-managers)))), and evaluating that multiple times can cause performance issues.
	 (max ,max)
	 (min ,min))
     (if (or (eq ,direction 1)
	     (eq ,direction down))
	 (incf selection)
	 (if (or (eq ,direction -1)
		 (eq ,direction up))
	     (decf selection)))
     (if max
	 (if (> selection max)
	     (setf selection max)))
     (if (< selection min)
	 (setf selection min))
     (setf ,selection selection)))


(defmacro a<b<c (a n c)
  `(and (> ,n ,a)
	(< ,n ,c)))

(defmacro a<b<=c (a n c)
  `(and (> ,n ,a)
	(<= ,n ,c)))

(defmacro a<=b<=c (a n c)
  `(and (>= ,n ,a)
	(<= ,n ,c)))

(defmacro a>b>c (a n c)
  `(and (< ,n ,a)
	(> ,n ,c)))

(defmacro a>=b>=c (a n c)
  `(and (<= ,n ,a)
	(>= ,n ,c)))

(defmacro a>b>=c (a n c)
  `(and (< ,n ,a)
	(>= ,n ,c)))

(defmacro >b> (a n c)
  `(and (< ,n ,a)
	(> ,n ,c)))
	
(defmacro length> (sequence comparator)
  `(if (listp ,comparator)
       (> (length ,sequence) (length ,comparator))
       (> (length ,sequence) ,comparator)))

(defmacro length< (sequence comparator)
  `(if (listp ,comparator)
       (< (length ,sequence) (length ,comparator))
       (< (length ,sequence) ,comparator)))

(defmacro length= (sequence comparator)
  `(if (listp ,comparator)
       (= (length ,sequence) (length ,comparator))
       (= (length ,sequence) ,comparator)))

(defun use-<or> (a b)
  (if (> a b)
      a
      b))
