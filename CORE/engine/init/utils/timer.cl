(defpackage :timer
  (:use :cl)
  (:export timers
	   make
	   current
	   end
	   reset?
	   reset
	   end?
	   incrementor
	   increment
	   increment-timers
	   add
	   process
	   process-all
	   push-timer
	   remove-timer
	   timers-table))
(in-package :timer)
(defvar timers nil)

(defvar timers-table ( make-hash-table ))
(defvar timer-fps 60)
(defstruct timer
  current
  end
  incrementor
  reset?
  timers
  (label "" :type string))

(defun make ( &key ( current 0 ) ( end 1 ) ( incrementor 1 ) reset? in-seconds? (timers t) (label ""))
  (when in-seconds?
    (setf end ( round ( * end tamias:fps ))))
  (when ( < incrementor 0 )
    (when ( > end 0 )
      (setf end ( * end -1 ))))
  (let ((timer (make-timer :current current :end end
			   :incrementor incrementor :reset? reset? :label label
			   :timers (if (not (listp timers))
				       timer:timers
				       timers))))
    (when timers
      (if (not (listp timers))
	  (push timer timer:timers)
	  (push timer timers)))
    timer))

(defmacro current ( timer )
  `(timer-current ,timer ))
(defmacro end ( timer )
  `(timer-end ,timer ))
(defmacro incrementor ( timer )
  `(timer-incrementor  ,timer ))
(defmacro reset? ( timer )
  `(timer-reset? ,timer ))

(defun remove-timer ( timer )
  (when (timer-timers timer)
    (setf (timer-timers timer) (remove timer (timer-timers timer)))))

(defun reset ( timer )
  (setf ( timer-current timer ) 0 )
  (unless (reset? timer)
    (remove-timer timer)))
    
(defmacro increment ( timer )
  `(incf ( timer-current ,timer )
	 ( timer-incrementor ,timer )))

(defun end? ( timer &optional timers? )
  (if ( >= ( timer-incrementor timer ) 0 )
      ( >= ( timer-current timer ) ( timer-end timer ))
      ( <= ( timer-current timer ) ( timer-end timer ))))

(defun process (timer)
  (increment timer)
  (when (end? timer)
    (reset timer)
    t))

(defun process-all (&optional (timers timer:timers))
  (loop for timer in timers
	do (when timer
	     (process timer))))


(defun add ( timer )
  (push timer timer:timers ))

(defun increment-timers ()
  (loop for timer in timers
	do (unless (reset? timer)
	     (if ( >= ( timer-incrementor timer ) 0 )
		 (when ( >= ( timer-current timer ) ( timer-end timer ))
		   ( remove-timer timer ))
		 (when ( <= ( timer-current timer ) ( timer-end timer ))
		   ( remove-timer timer ))))
	   (increment timer)))

"A quick thing about timers: they are generalized. So, you can use a timer for a literal timer (i.e. counting down from 60 to 0) 
or you can use it in combination with a GUI (i.e. counting down from 1000 gold to 25 gold)"
