(define-symbol-macro char-width (car tamias.string:character-size))
(define-symbol-macro char-height (cadr tamias.string:character-size))
(export '(char-width
	  char-height))

(in-package :states)
(export '(states
	  make-state
	  state-p
	  state-symbol
	  state-sub-state
	  state-sub-state-stack
	  state-logic-list
	  state-world
	  state-scene-queue
	  state-render-stack?
	  state-using-gui?
	  state-any-input
	  state-input-table
	  render-list
	  define-state

	  init-input
	  get-state-keys
	  get-state-mouse

	  set-sub-state
	  push-sub-state
	  to-sub-state
	  pop-sub-state
	  up-sub-state
	  get-sub-state
	  previous-sub-state
	  
	  set-state
	  get-state

	  add-mouse
	  mouse-input
	  print-objs

	  get-kb-key
	  tamias-key-handler

	  make-kb-input
	  kb-input-key
	  kb-input-key-state
	  kb-input-ctrl
	  kb-input-alt
	  kb-input-shift
	  
	  convert-to-keyword
	  define-kb-input-var
	  def-kb-var
	  add-action-to-key
	  set-kb-key

	  kb-key-masks
	  
	  remap-kb-key
	  remap-kb-key-vars

	  def-render
	  add-render
	  def-logic
	  add-logic
	  add-process
	  logic
	  render-state

	  to-state
	  ))

(defvar to-state nil)
(defvar states nil)

(defstruct ibox
  (x 0)
  (y 0)
  (width 16)
  (height 16)
  mouse?
  active?)

(defstruct state
  (using-gui? t :type boolean)
  (input-table (make-hash-table) :type hash-table)
  (symbol :nil :type keyword)
  (sub-state cl-user:+idle+ :type keyword)
  (sub-state-stack (list cl-user:+idle+) :type list)
  render-stack?
  world
  scene-queue
  transition
  (any-input (make-hash-table))
  (held-inputs (make-hash-table))
  (ibox (make-ibox))
  (logic-list (make-hash-table))
  (render-list (make-hash-table)))

(defun state-keys (state)
  (gethash :keyboard (state-input-table state)))
(defun state-mouse (state)
  (gethash :mouse (state-input-table state)))

(defun get-state-keys (state sub-state)
  (unless (gethash sub-state (state-keys state))
    (setf (gethash sub-state (state-keys state)) (make-hash-table)))
  (gethash sub-state (state-keys state)))

(defun get-state-mouse (state sub-state)
  (unless (gethash sub-state (state-mouse state))
    (setf (gethash sub-state (state-mouse state)) (make-hash-table)))
  (gethash sub-state (state-mouse state)))

(defmacro render-list (state)
  `(state-render-list ,state))

#|
(defun concatenate-symbols (&rest objects)
  (intern (apply #'concatenate 'string (mapcar #'princ-to-string objects))))
|#

(defvar input-types '(:keyboard :mouse :unsupported-controller))
(export 'input-types)

(defun init-input (state)
  (setf (gethash :keyboard (state-input-table state)) (make-hash-table)
	(gethash :mouse (state-input-table state)) (make-hash-table)))

(defun init-input-type (state input-type)
  (push input-type input-types)
  (setf (gethash input-type (state-input-table state)) (make-hash-table)))

(defun set!-sub-state (sub &optional (state tamias:state))
  (setf (state-sub-state state) sub
	sub-state sub))

(defun push-sub-state (sub-state &optional (state tamias:state))
  (unless (state-p state)
    (setf state (eval state)))
  (push sub-state (state-sub-state-stack state))
  (set!-sub-state sub-state state))
(defmacro to-sub-state (sub-state)
  `(push-sub-state (aux:to-keyword ',sub-state)))

(defun pop-sub-state (state)
  (pop (state-sub-state-stack state))
  (set!-sub-state (car (state-sub-state-stack state))))

(defun sub-state-set (sub-state &optional (state tamias:state))
  (if (eql (cadr (state-sub-state-stack state)) sub-state)
      (pop-sub-state state)
      (push-sub-state sub-state state)))

(defmacro set-sub-state (sub-state &optional state)
  `(sub-state-set (aux:to-keyword ',sub-state) (or ,state tamias:state)))

(defun up-sub-state ()
  (pop-sub-state states:state))

(defun previous-sub-state ()
  (pop-sub-state states:state))

(defun get-sub-state ()
  (state-sub-state tamias:state))

(defun state-set (new-state &optional sub-state)
  (if tamias:started? 
      (setf to-state new-state)
      (setf state new-state))
  (when sub-state
    (push-sub-state sub-state new-state)))
  
(defmacro set-state (new-state &optional sub-state)
  `(state-set ,new-state (if ',sub-state (aux:to-keyword ',sub-state))))

(defmacro define-state (state &optional current-state? sub-state (using-gui? t))
  `(progn
     ;;not sure why, but using a let and binding 'state-keyword' to (aux:to-keyword ',state)
     ;;causes a bunch of errors to occur
     ;;If i had to guess, it's due to how (at least SBCL) let is processed
     ;;Instead of processing it 'statement by statement' it processes it as one big 'chunk'
     ;;instead of (defvar ...) (when ...) (push ...)
     ;;it's ( defvar - when - push - ... )
     ;;so the rest of the let block is 'blind' to the defvar having been processed
     ;;Regardless, progn seems to do just fine, I just like to try to optimize where /I/ can
     ;;I want to avoid processing the same thing multiple times, when it can be processed
     ;;    once and referred to multiple times.
     (defvar ,state (make-state
		     :symbol (aux:to-keyword ',state)))
     (unless ,state
       (setf ,state (make-state
		     :symbol (aux:to-keyword ',state))))
     (push ',state states)
     (init-input ,state)
     (if ,using-gui?
	 (gui:state.init-gui (aux:to-keyword ',state) (state-sub-state ,state))
	 (setf (state-using-gui? ,state) nil))
     (when ,current-state? (set-state ,state ,sub-state))))


(defun get-state ()
  tamias:state)

(define-state tamias-default-state)
(export 'tamias-default-state)

(defun print-objs (&rest objs)
  (loop :for obj :in objs
	:do (print obj)))

(defun render (state sub-state)
  (loop :for fn :in (gethash sub-state (render-list state))
	:do (eval fn)))

(defun render-state (&optional (state tamias:state)
		       (sub-state (elt (state-sub-state-stack tamias:state) 0)))
  (if (listp sub-state)
      (loop :for s-b :in sub-state
	    :do (render state s-b))
      (render state sub-state)))

(defun reverse-render (state sub-state)
  (setf (gethash sub-state (render-list state)) (reverse (gethash sub-state (render-list state)))
	(state-reversed? state) t))

(defmacro def-render ((state sub-state) &rest functions)
  `(loop :for fn :in ',functions
	 :do (aux:push-to-end fn (gethash (aux:to-keyword ',sub-state) (render-list ,state)))))

(defmacro add-render ((state sub-state) &rest functions)
  `(loop :for fn :in ',functions
	 :do (aux:push-to-end fn (gethash (aux:to-keyword ',sub-state) (render-list ,state)))))

(defun logic (state sub-state)
  (loop :for fn :in (gethash sub-state (state-logic-list state))
	:do (eval fn)))

(defmacro def-logic ((state sub-state) &rest functions)
  `(loop :for fn :in ',functions
	 :do (aux:push-to-end fn (gethash (aux:to-keyword ',sub-state) (state-logic-list ,state)))))

(defmacro add-logic ((state sub-state) &rest functions)
  `(loop :for fn :in ',functions
	 :do (aux:push-to-end fn (gethash (aux:to-keyword ',sub-state) (state-logic-list ,state)))))

(defmacro add-process ((state sub-state) &rest functions)
  `(loop :for fn :in ',functions
	 :do (aux:push-to-end fn (gethash (aux:to-keyword ',sub-state) (state-logic-list ,state)))))

(defun reset-logic (state sub-state)
  (setf (gethash sub-state (state-logic-list state)) nil))

#|
(defgeneric activate-interactive-object (tamias-object key-board? key-state ctrl alt shift mouse? mouse-button mouse-state move?)
  (:method (tamias-object key-board? key-state ctrl alt shift mouse? mouse-button mouse-state move?)
    nil))

(defmacro activate-object (tamias-object key-board? (&optional key-state ctrl alt shift) mouse? (&optional mouse-button mouse-state move?) &body body)
  `(defmethod activate-interactive-object ((tamias-object (eql ,tamias-object))
					   (key-board? (eql ,key-board?))
					   (key-state (eql ,key-state))
					   (ctrl (eql ,ctrl))
					   (alt (eql ,alt))
					   (shift (eql ,shift))
					   (mouse? (eql ,mouse?))
					   (mouse-button (eql ,mouse-button))
					   (mouse-state (eql ,mouse-state))
					   (move? (eql ,move?)))
     ,@body))
|#
;;The Idea of a "pass-through" is that an input is passed through the state/sub-state sub-system and is directly
;;passed through to the various components, like UI or In-game objects
;;So that when you press "a" a is passed through to the current context's input system
;;SO the "body" would look something like (current-context-input input)
;;I do want to figure out how to make methods for various structs.
;;
;;I want to have it so that each 'object' can have it's own interaction tree, rather than having to afro-engineer together a solution with the current architecture
;;The reason is fairly simple: it allows for a much more complex interaction system
;;The Downside, however, is increased processing time, ram usage, and storage space.
;;The Upside is simplified complexity, ease of development & use, encapsulation, and better organized code
;;
;;(defun add-pass-through (state sub-state pass-through-type)
;;  (case pass-through-type
;;    ('key ())
;;    ('mouse ())))



