(defun set-clear-color (color)
  (if (not (or (listp color) (vectorp color)))
      (tamias:console.add-message "Invalid value passed to set-clear-color")
      (setf tamias:render-clear-color color)))

(defpackage render
  (:use :cl)
  (:nicknames
   :draw)
  (:export line
	   define-buffer
	   remove-buffer
	   create-texture
	   with-rectangle
	   create-rectangle
	   free-rectangle
	   tex-blit
	   box
	   bg-element
	   rectangle
	   to-buffer
	   reset-text-buffer
	   text-buffer
	   ;;render-buffer
	   text-width
	   text-height
	   text-dimensions
	   render-string
	   text
	   font-color
	   image
	   cell
	   animate
	   tile
	   sprite))
(in-package :render)
#|
==============================================================================
                                  GENERAL
==============================================================================
|#
(defun set-buffer (str buffer)
  )
(defmacro to-buffer (str buffer)
  `(setf ,buffer (set-buffer ,str ,buffer)))

(defun line (x y x2 y2 &key (color (vector 255 255 255 255)))
  (declare (ignorable x y x2 y2 color)))


(defmacro define-text-buffer (buffer)
  `(progn (defvar ,buffer nil)
	  (push ,buffer tamias:text-buffers)))


(defun create-texture (&key format (access 0) (width 16) (height 16) (color tamias.colors:+white+))
  )

(defmacro with-rectangle (name rect-parameters &body body)
  )
#|
usage: (with-rectangle bat-rect (list (bat-x bat) (bat-y bat) (bat-width bat) (bat-height bat))
(render:rectangle bat-rect))
|#

(defmacro create-rectangle (rect-vals)
  )
#|
usage: (defun some-func ()
          (let ((my-rect (create-rectangle '(x y width height))))
            (render:rectangle my-rect)
            (render:free-rectangle my-rect)))
|#
(defmacro free-rectangle (rect)
  )

(defun image (img &optional (x 0) (y 0) w h)
  )

(defun tex-blit (tex &key src dest color angle center (flip :none))
  )

(defun box (x y w h &optional (color (vector 255 255 255 255)) (filled t))
  ) ;;frees memory taken up by the rect. Not doing this will cause a segfault

(defun bg-element (&optional x y w h color)
  (let ((x (or x 0))
	(y (or y 0))
	(w (or w 32))
	(h (or h 32))
	(color (or color tamias.colors:+chalk-white+)))
    (box x y w h color)))


(defun render-rectangle (rect &optional (color (vector 255 255 255 255)) filled)
  )

(defun rectangle (x y w h &optional (color (vector 255 255 255 255)) filled)
  )


(defun pixel (x y &optional (color (vector 255 255 255 255)))
  )


#|
==============================================================================
                              TEXT AND STRINGS
==============================================================================
|#


(defvar font-color (vector 255 255 255 0))

(defmacro reset-text-buffer (buffer)
  )

#|
(defun render-buffer (buffer menu &key color)
  (if color
      (sdl2:set-texture-color-mod buffer (car color) (cadr color) (caddr color)))
  (let ((src (sdl2:make-rect 0
			     0
			     (sdl2:texture-width buffer)
			     (sdl2:texture-height buffer)
			     ))
	(dest (sdl2:make-rect (+ (menu-x menu) 8)
			      (+ (menu-y menu) 8)
			      (- (menu-width menu) 8)
			      (- (menu-height menu) 8)
			      )))
    (sdl2:render-copy tamias:renderer
		      buffer
		      :source-rect src				    
		      :dest-rect dest)
    (sdl2:free-rect src)
    (sdl2:free-rect dest)))

This was for the roguelike I was/am working on
|#

(defun text-width (text)
  (* (length text) (car tamias.string:character-size))
  )
(defun text-height (text)
  (let ((new-line-count (1+ (count #\newline text))))
    (* new-line-count (cadr tamias.string:character-size))))
(defun text-dimensions (text)
  (if (symbolp text)
      (setf text (write-to-string text)))
  (if (stringp text)
      (let ((width (text-width text))
	    (height (text-height text)))
	(list width height))
      (list 0 0)))


(defun render-string (str x y &key width height string-width string-height (rotate 0) (color font-color) anti-alias)
  )

(defun determine-string-width-nlc (str)
  )

(defun text (str x y &key width height (rotate 0) (color font-color) scale origin-is-center)
  )


#|
==============================================================================
                           SPRITES and ANIMATION
==============================================================================
|#

(defpackage :sprite
  (:use :cl)
  (:export make-sheet
	   make
	   sheet-file
	   sheet-width
	   sheet-height
	   sheet-cells
	   sheet-rows
	   sheet-columns
	   sheet-surface
	   sheet-texture
	   sheet-cell-size
	   make-sprite
	   sheet
	   sprite-sheet
	   sprite-cell-timer
	   sprite-current-cell
	   set-sheet-width
	   set-sheet-height
	   set-cells
	   set-sheet-surface
	   set-sheet-texture
	   load-sheet
	   defsheet
	   define-sheet
	   define-sprite
	   free-sheet
	   sheets
	   sheets-images
	   sheets-ids
	   sheets-id-current
	   new-sheet
	   get-sheet
	   default-frame-time))
(in-package :sprite)

(t.utils:create-id-system sheets)
(defmacro new-sheet (sheet)
  `(new-sheets ,sheet))
#|
(defvar sheets (make-hash-table))
(defvar sheet-ids nil)
(defvar sheet-id-current -1)
|#
(defvar sheets-images (make-hash-table))

(defstruct sheet
  cell-size
  file
  (width 0)
  (height 0)
  cells
  rows
  columns
  surface
  texture)

(defvar placeholder-sheet (make-sheet))


#|
;;This is for working with the animation subsystem easier
;;When you do "sprite:defsprite" or "sprite:make" you have to supply an entity-type
;;if the sheet /isn't/ a sheet structure, it'll need to make it at defsprite time
;;So: Sheets are the "technical" part 
;;whereas sprites are the front-end part that are supposed to be easier to manage
;;well maybe
;;I don't know honestly.
;;I feel like it's fine as it is, but I'm not 100% yet
;;

;; Nope, multiple instances of the same "creature" will cause issues
;; if they use the same sheet with the same timers
|#
(defstruct sprite 
  (sheet placeholder-sheet :type sheet)
  ;;note to self: modifying placeholder-sheet will also affect
  ;;any sprite that uses placeholder-sheet
  ;;There was a problem that I was trying to solve that this solves
  (cell-timer (timer:make :end 20 :reset t))
  (current-cell 0))

(defmacro sheet (sprite)
  `(sprite-sheet ,sprite))

(setf (fdefinition 'make) #'make-sprite)

#|
(defmacro optimize-sheet (var)
  )
|#

(defmacro set-sheet-width (sheet width)
  `(setf (sprite:sheet-width ,sheet) ,width))

(defmacro set-sheet-height (sheet height)
  `(setf (sprite:sheet-height ,sheet) ,height))

(defmacro set-cells (sheet)
  `(let* ((tile-size (sprite:sheet-cell-size ,sheet))
	  (rows (floor (/ (sprite:sheet-height ,sheet) (cadr tile-size))))
	  (columns (floor (/ (sprite:sheet-width ,sheet) (car tile-size))))
	  (cells (make-array (* columns rows) :initial-element (make-array 2))))
     (loop :for y :below rows
	   :do (loop :for x :below columns
		     :do (setf (aref  (aref cells (*(1+ y) x))  0) x
			       (aref  (aref cells (*(1+ y) x))  1) y)))
     (setf (sprite:sheet-rows ,sheet) rows
	   (sprite:sheet-columns ,sheet) columns
	   (sprite:sheet-cells ,sheet) cells)))

(defmacro set-sheet-surface (sheet surface)
  `(setf (sprite:sheet-surface ,sheet) ,surface))

(defun load-sheet (sheet &optional surface?)
#|  `(let* ((filename (sheet-file ,sheet))
	  (surface (tamias:load-image filename)))
     (set-sheet-height ,sheet (sdl2:surface-height surface))
     (set-sheet-width ,sheet (sdl2:surface-width surface))
     (set-cells ,sheet)
     (set-sheet-surface ,sheet surface)
     (optimize-sheet ,sheet))
|#
  )

(defun sheet-is-loaded? (sheet-key)
  (let ((sheet (gethash sheet-key sheets-images)))
    (if sheet
	(unless (stringp sheet)
	  t))))
(export 'sheet-is-loaded?)

(defun check-sheet (sheet-key)
  (gethash sheet-key sheets))
(export 'check-sheet)

(defun get-sheet (sheet-key)
  (let ((ret-val (gethash sheet-key sheets)))
    (if (stringp (gethash sheet-key sheets-images))
	(setf (gethash sheet-key sheets-images) (tamias:load-image (sheet-file ret-val))))
#|
    (eval `(t.aux:add-init (setf (gethash ,entity-type sheet-images)
				 (tamias:load-image (sheet-file ,ret-val)))))
    (eval `(t.aux:add-quit (progn (tamias:free-image (gethash ,entity-type sheet-images))
				  (setf (gethash ,entity-type sheet-images) nil))))
|#
    (setf (sheet-texture ret-val) (gethash sheet-key sheets-images))
    (load-sheet ret-val)
    ret-val))

(defvar default-frame-time 6)

(defun defsheet (sheet-key file cell-size)
  (when (find sheet-key sheets-ids)
    (format t "~A sheet already defined." sheet-key))
  
  (unless (find sheet-key sheets-ids)
    (unless (listp cell-size)
      (setf cell-size (list cell-size cell-size)))
    (setf (gethash sheet-key sheets) (make-sheet :file file
						 :cell-size cell-size)
	  (gethash sheet-key sheets-images) file)
    (push sheet-key sheets-ids)
    (incf sheets-id-current)))

(defun free-sheet (sheet-key sheet)
  )

(defmacro define-sheet (sheet-key file cell-size &optional animate? (frame-time default-frame-time))
  `(sprite:defsheet ,sheet-key ,file ',cell-size ,animate? ,frame-time))

(defmacro define-sprite (sheet-key)
  )

#|
usage:
(define-sheet :bat "game/gfx/bat-sheet.png" '(cell-width cell-height))
then (entity-sheet bat) for the bat's sheet
also, the idea is that "entity" is a generalized structure in which all of  your entity structs
are based on, mainly to help in reducing necessary labour time
|#

(in-package :render)

(defun cell (sheet cell render-at-x render-at-y &key width height color (angle 0) center (flip :none))
  (let* ((cells (sprite:sheet-cells sheet))
	 (tile-width   (car   (sprite:sheet-cell-size  sheet)))
	 (tile-height  (cadr  (sprite:sheet-cell-size  sheet)))
	 
	 (src-rect (tamias:make-rect (elt  (elt  cells  cell)  0 )
				     (ett  (elt  cells  cell)  1 )
				     tile-width
				     tile-height))
	 (dest-rect (tamias:make-rect render-at-x
				      render-at-y
				      (or width
					  tile-width)
				      (or height
					  tile-height)))
	 (flip (if (or (eq  flip  :none)
		       (eq  flip  :horizontal)
		       (eq  flip  :vertical))
		   flip
		   :none))
	 (center (or center
		     nil))
	 (angle (if (not (or (integerp  angle)
			     (floatp    angle)))
		    0
		    angle)))
    (tex-blit (sprite:sheet-texture sheet)
	      :src    src-rect  :dest   dest-rect
	      :color  color     :angle  angle
	      :center center    :flip   flip)))

(defun tile (sheet cell render-at-x render-at-y &key width height color (angle 0) center (flip :none))
  (cell sheet cell
	render-at-x render-at-y
	:width width :height height :color color :angle angle :center center :flip flip))

(defun sprite (sprite render-at-x render-at-y &key width height color (angle 0) center (flip :none))
  (cell (sprite:sprite-sheet sprite) (sprite:sprite-current-cell sprite)
	render-at-x render-at-y
	:width width :height height :color color :angle angle :center center :flip flip))

(defun animate (sprite render-at-x render-at-y &key width height (current-row 0) color (angle 0) center (flip :none))
  (when tamias:animation?
    (let ((sheet (sprite:sprite-sheet sprite))
	  (min-max-cell (* current-row (sprite:sheet-columns sheet))))
      (timer:increment (sprite:sprite-cell-timer sprite))
      (incf current-row)
      (when (timer:end? (sprite:sprite-cell-timer sprite))
	(incf (sprite:sprite-current-cell sprite) 1))
      
      (if (>= (sprite:sprite-current-cell sprite)
	      min-max-cell)
	  (setf (sprite:sprite-current-cell sprite)
		(- min-max-cell
		   (sprite:sheet-columns sheet)))
	  (if (< (sprite:sprite-current-cell sprite)
		 (- min-max-cell
		    (sprite:sheet-columns sheet)))
	      (setf (sprite:sprite-current-cell sprite)
		    (- min-max-cell
		       (sprite:sheet-columns sheet)))
	      ;;this second if statment is to ensure that if the player changes their direction
	      ;;(e.g. changing the current-row to be rendered) then it will set it to the 0th cell
	      ;;of the new current-row
	      ))
      (cell sheet (sprite:sprite-current-cell sprite)
	    render-at-x render-at-y
	    :width width :height height :color color :angle angle :center center :flip flip)
      )))

    ;;While the above code is mostly self-explanatory, I do feel it could be confusing
    ;;current-row is simply the row of sprites you want to render from an evenly distributed image
    ;;it will assume the user is calling it using 0-aligned arrays
    ;;So, we first increment the row. We do this because of calculations required in the if block
    ;;Now, we check if the current-cell is greater than or equal to the current-row (plus one) times
    ;;total columns in the sheet
    ;;We do this because if the current cell starts at the next row, it would render the next row
    ;;e.g. Using the following 2d array, if current cell is 3, then it would render 1
    ;;     Therefore, at row '1' times the number of columns (3), then the current cell
    ;;     should never be rendered at 3 [1 0], instead resetting to 0 [0 0], as long as animate
    ;;     is called with :current-row 0
    ;;[ 0 0 0 ]
    ;;[ 1 0 1 ]
    ;;[ 2 0 2 ]
    ;;[ 3 0 3 ]    

