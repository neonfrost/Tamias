(defun emit (obj)
  obj)

(defvar exit-tamias? nil)
(export 'exit-tamias?)

(defmacro remove-nil (lst)
  `(setf ,lst (remove nil ,lst)))

(defun sharp-h (stream subchar arg)
  (declare (ignore subchar arg))
  (let ((var (read stream nil nil)))
    (if var
	(eval `(defvar ,var (make-hash-table)))
	(make-hash-table))))
(set-dispatch-macro-character #\# #\h #'sharp-h)

(defun sharp-f (stream subchar arg)
  (declare (ignore subchar arg))
  (let ((var (read stream nil nil)))
    (if var
	(eval `(defvar ,var 0.0))
	0.0)))
(set-dispatch-macro-character #\# #\f #'sharp-f)

#|
(defun sharp-g (stream subchar arg) 
	   (declare (ignore subchar arg))
	   `(defvar ,(read stream) ,(read stream)))
This is for documentation purposes :)
|#


(defun sharp-v (stream subchar arg)
  (declare (ignore subchar arg))
  (let* ((obj-1 (read stream nil nil))
	 (obj-2 (read stream nil nil)))
    (if obj-2
	`(defvar ,obj-1 ,obj-2)
	`(defvar ,obj-1 nil))))
(set-dispatch-macro-character #\# #\v #'sharp-v)

#|
(defun sharp-v (stream subchar arg) 
	   (declare (ignore subchar arg))
  `(defvar ,(read stream) ,(read stream nil nil)))
This version will take the next object found and use it as the value for your variable
so would result in my-var evaluating to "fuck"
  |
  V
#vmy-var
(print "fuck")
    
|#
;;#vVariable-name-with-unlimited-hyphens-^-^ and-optional-value_otherwise-is-nil


(defconstant +idle+ :idle)
(export '+idle+)





(in-package :tamias)

(defvar current-window-id -1)

(defstruct window
  (x 0 :type integer)
  (y 0 :type integer)
  (width 1 :type integer)
  (height 1 :type integer)
  (ID (incf current-window-id) :type integer)
  renderer
  (title "" :type string)
  (children nil :type list))
(defvar default-window (make-window))
;; tamias window needs id system
(defvar keyboard-type :keyboard-eng)

(defun load-font (&key font-type font-path)
  (declare (ignore font-type font-path)))

(defvar started? nil)

(defvar quad-tree nil)

(defun main ()
  )

(defun main-release ()
  )

(defvar default-window nil)

(defun set-window-size (window width height)
  (declare (ignore window width height)))
(defun load-image (file-path)
  (declare (ignore file-path))) ;;loader function for backends
(defun free-image (img)
  (declare (ignore img))) ;;freer for backend
(defun make-rect (x y width height)
  (declare (ignore x y width height)))
(defun set-default-window-size (&optional width height)
  (set-window-size default-window width height))


(defvar *mouse-x* 0)
(defvar *mouse-y* 0)
(defvar *mouse-velocity-x* 0)
(defvar *mouse-velocity-y* 0)
(defparameter *cursor-size* 5)

(defvar track-volume 125)
(defvar max-volume 125)
(defvar current-track nil)
(defvar new-track nil) ;;needs to be a symbol
(defvar default-volume-state :increasing)

(defvar key-pressed nil)
(defvar screen-surface nil)
(defvar window-width 0)
(defvar window-height 0)
(defvar game-title "WINDOW TITLE TAMIAS - CHANGE BY USING (TAMIAS:SET-WINDOW-TITLE \"YOUR-TITLE\")")
(defun set-window-title (title)
  (declare (ignore title)))

(defvar state nil) ;;specifically an empty state, does nothing...when it's tamias-default-state that is...
(defvar sub-state 'top)
(defvar changing-state nil) ;;setf changing-state to 'state-to-change-to (i.e. main-menu)
(defvar renderer nil)
(defvar accumulator 0)
(defvar selection 0)

(defvar font nil)
(defvar ttf-font nil)
(defvar ttf-font-size 12)
(defvar ttf-font-path "fonts/linux_libertine/LinLibertine_R.ttf")
(setf ttf-font-path "fonts/Vera.ttf")

(defvar max-characters '(40 40))
(defvar text-buffers nil)
(defvar cell-accumulator 0)
(defvar volume-state nil)
(defvar objects nil)
(defvar render-clear-color '(0 0 0))
(defvar resolution 1)
(defvar update-time 5)
(defvar fps 60)
(defvar resolution-list '(#(800 600)
			  #(960 720)
			  #(1024 768)
			  #(1280 720)
			  #(1360 760)
			  #(1280 1024)
			  #(1440 900)
			  #(1680 1050)
			  #(1280 960)
			  #(1440 1080)
			  #(1920 1080)))
(setf window-width (elt (nth resolution resolution-list) 0))
(setf window-height (elt (nth resolution resolution-list) 1))

(defun set-window-dim-by-res-list ()
  (setf window-width (elt (nth resolution resolution-list) 0)
	window-height (elt (nth resolution resolution-list) 1)))

(defun change-resolution (by-method width-or-index &optional (height 1))
  (declare (ignore by-method width-or-index height)))

(defun set-resolution (indice &optional (window default-window))
  (setf resolution indice)
  (set-window-dim-by-res-list)
  (set-window-size window window-width window-height))

(defun enable-ttf-font ()
  (if (not ttf-font)
      (setf ttf-font t)
      (tamias:console.add-message "TTF font is already enabled")))

(defstruct message
  (timer 0)
  text
  buffer)
(defvar message-time-out 360)

(defvar messages (list (make-message :text "Console started")))
(defvar messages-counter 0)
(defvar messages-color '(255 255 255 255))

(defvar using-gui t)

(defun create-exec (&key linking (name "Game") (system :tamias))
  (declare (ignore linking name system)))

(defun fps (target)
  (setf fps target
	update-time (floor (/ 1000 (* 2 target)))))
(defvar screen-resolution '(640 480))

(define-symbol-macro screen-width (car screen-resolution))
(define-symbol-macro screen-height (cadr screen-resolution))

(defvar scale-factor (vector window-width window-height))
(defvar target-resolution scale-factor)
(defvar target-resolution-changed nil)

(defun set-scale-factor (&optional width height)
  (setf scale-factor (vector width height)
	target-resolution-changed nil))

(defun set-target-resolution (&optional (width window-width) (height window-height))
  (setf target-resolution (vector width height)
	target-resolution-changed t))
