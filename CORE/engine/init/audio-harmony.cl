(defmacro music-let (track &body body)
  `(let ((m-stream (track-stream ,track))
	 (m-path (track-path ,track))
	 (m-loop-point (track-loop-point ,track))
	 (m-duration (track-duration ,track)))
    ,@body))

(defun play-sound (sound)
  )

(defun play-music (track)
  (music-let track
     (print m-path)
     (print 'shirakumo-harmony!)
     
	     
	     ))

(defun play (obj)
  (case (type-of obj)
    (sound (play-sound obj))
    (track (play-music obj))))

