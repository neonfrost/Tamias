(in-package :states)

(defun mouse-flags (move? mouse-state double-click m-held?)
  (let ((bit-mask 0))
    (incf bit-mask (gethash (if move? :move :nil) modifiers-table))
    (incf bit-mask (gethash (or mouse-state :nil) modifiers-table))
    (incf bit-mask (gethash (if double-click :double-click :nil) modifiers-table))
    (incf bit-mask (gethash (if m-held? :held :nil) modifiers-table))
    bit-mask))

(defun remap-mouse (state sub-state mouse-a mouse-a-modifier-accessor mouse-b &rest mouse-b-modifiers)
  (remap-input state sub-state :mouse mouse-a mouse-a-modifier-accessor mouse-b mouse-b-modifiers))
(export 'remap-mouse)
    
(defun mouse-input (mouse-button mouse-state state sub-state move? double-click m-held?)
  (let ((flags (mouse-flags move? mouse-state double-click m-held?)))
    (incf flags (kb-key-masks))
    (loop :for fn :in (get-input :mouse
				 mouse-button flags state sub-state)
	  :do (eval fn))))

(defmacro add-mouse ((mouse-button mouse-state &rest flags) (state sub-state) &body body)
  `(let ((flags 0))
     (incf flags (gethash ,mouse-state modifiers-table))
     (loop :for flag :in ',flags
	   :do (incf flags (gethash flag modifiers-table)))
     (let ((KEYP-sub-state  ,sub-state)
	   (KEYP-state      ,state))
       (unless (keywordp ,state)
	 (setf KEYP-state (state-symbol ,state)))
       (unless (keywordp ,sub-state)
	 (setf KEYP-sub-state (aux:to-keyword ',sub-state)))
       (define-input :mouse (KEYP-state KEYP-sub-state) (,mouse-button flags) ',body))))
;;     (push ',@body (gethash flags (gethash ,mouse-state (get-state-mouse ,state (aux:to-keyword ',sub-state))))))

