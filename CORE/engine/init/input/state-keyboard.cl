(in-package :states)
;;should I abandon the states-based input?
;;no
;;I can hypothetically...no I have to keep the state/sub-state structure
;;
(defvar shifted-characters '(#\! #\# #\$ #\% #\& #\* #\( #\) #\+ #\< #\> #\? #\@ #\" #\^ #\_ #\~ #\: #\{ #\}))
(defvar unshifted-characters '(#\1 #\3 #\4 #\5 #\7 #\8 #\9 #\0 #\= #\, #\. #\/ #\2 #\' #\6 #\- #\` #\; #\[ #\]))

;;Instead of using actual 'keys' from the keyboard, should I instead use character based input?
;;like keyword the character...but it becomes a problem for alphabetic characters
;;because anytime the user puts in @, the developer would need to ensure that shift is on for each of them
;;
;;No, when adding in a key, we see if it's a character that has to be shifted for
;;

(defun remap-key (state sub-state key-a key-a-modifiers-accessor key-b &rest key-b-modifiers)
  (remap-input state sub-state :keyboard key-a key-a-modifiers-accessor key-b key-b-modifiers))
(export 'remap-key)

(defmacro add-any-input ((state sub-state) &rest functions)
  `(loop :for fn :in ',functions
	 :do (aux:push-to-end fn (gethash (aux:to-keyword ',sub-state) (state-any-input ,state)))))
(export 'add-any-input)

(defun tamias-key-handler (key key-state state sub-state &optional ctrl alt shift hyper)
  (let ((mod-state 0))
    (when shift
	(incf mod-state (gethash :shift modifiers-table)))
    (when ctrl
	(incf mod-state (gethash :ctrl modifiers-table)))
    (when alt
	(incf mod-state (gethash :alt modifiers-table)))
    (when hyper
	(incf mod-state (gethash :hyper modifiers-table)))
    (incf mod-state (gethash key-state modifiers-table))
#|    (if (not (gethash key (gethash key-state (get-state-keys state sub-state))))
	(setf (gethash key (gethash key-state (get-state-keys state sub-state)))
	      (make-hash-table)))
|#
    ;;(print-objs key key-state state sub-state mod-state)
    ;;    (format t "~A ~A" (get-state-keys state sub-state) key-state)
    (process-top-level-input :keyboard key mod-state)
    (if (gethash sub-state (state-any-input state))
	(loop :for fn :in (gethash sub-state (state-any-input state))
	      :do (eval fn))
	(loop :for t-func :in (get-input :keyboard
					 key mod-state state sub-state)
	      :do  (eval t-func)))))

(defmacro ctrl-flag (ctrl)
  `(if ,ctrl
       :ctrl
       nil))
(defmacro alt-flag (alt)
  `(if ,alt
       :alt
       nil))
(defmacro shift-flag (shift)
  `(if ,shift
       :shift
       nil))

(defmacro get-kb-key (key (state sub-state key-state mod-state))
  `(gethash ,key (get-state-keys ,state ,sub-state)))

(defun kb-internal-def (key key-state state-sym/key sub-state-key mod-state body)
  (define-input :keyboard (state-sym/key sub-state-key)
      (key (list key-state mod-state)) body))

"mod-state is a pseudo binary encoding of the modifier states"


(defmacro define-key (key key-state (state sub-state &optional shift control alt hyper) &body body)
  `(let ((mod-state 0)
	 (key (write-to-string ,key)))
     (when ,shift
       (incf mod-state #b1))
     (unless ,shift
       (when (find (aref key 1) shifted-characters)
	 (setf key (aux:to-keyword (string (elt unshifted-characters
						(position (aref key 1) shifted-characters))))
	       ,shift t)))
     (when ,control
       (incf mod-state #b10))
     (when ,alt
       (incf mod-state #b100))
     (when ,hyper
       (incf mod-state #b1000))
     (kb-internal-def ,key ,key-state
		      (state-symbol ,state)
		      (aux:to-keyword ,sub-state)
		      mod-state
		      ',body)))


(defmacro def-key (key key-state (state sub-state mod-state) &body body)
  `(kb-internal-def ,key ,key-state
		    (state-symbol ,state)
		    (aux:to-keyword ,sub-state)
		    ,mod-state
       ',body))
			     


(defmacro add-state-kb-key (key key-state (state sub-state &optional shift control alt hyper)
			    &body body)
  `(progn
     (let ((shift ,shift))
       (unless shift
	 (when (find (aref key 1) shifted-characters)
	   (setf key (aux:to-keyword (string (elt unshifted-characters
						  (position (aref key 1) shifted-characters))))
		 shift t)))
       (define-input :keyboard ((state-symbol ,state) (aux:to-keyword ',sub-state))
	   (list ,key (list ,key-state (when shift :shift) (when ,control :ctrl)
			    (when ,alt :alt) (when ,hyper :hyper)))
	   ',body))))
(export 'add-state-kb-key)

(defmacro kb.add-input (key key-state
			(state sub-state &optional shift control alt hyper)
			&body body)
  `(progn
     (let ((shift ,shift)
	   (key (write-to-string ,key))
	   (state-dest ,state))
       (unless shift
	 (when (find (aref key 1) shifted-characters)
	   (setf key (aux:to-keyword (string (elt unshifted-characters
						  (position (aref key 1) shifted-characters))))
		 shift t)))
       (unless (keywordp ,state)
	 (setf state-dest (state-symbol ,state)))
       (define-input :keyboard (state-dest (aux:to-keyword ',sub-state))
	   (key (list ,key-state (when shift :shift) (when ,control :ctrl)
		      (when ,alt :alt) (when ,hyper :hyper)))
	   ',body))))
(export 'kb.add-input)


