(defpackage tamias.string
  (:use :cl)
  (:export character-size
	   create-text-buffer
	   ascii-to-string
	   parse-hex-color
	   ;;text-length
	   combine-strings
	   combine-text
	   text-chunker))
(in-package :tamias.string)

;;???????????????????????????
;;(defvar text-length 32)
;;???????????????????????????
;;Ah, this was supposed to be a column limit of sorts

(defvar character-size '(16 16))

(defun ascii-to-string (code)
  (if (integerp code)
      (concatenate 'string "" (list (code-char code)))
      (concatenate 'string "" (string code))))

(defun process-obj-for-comb-string (obj &optional text?)
  (typecase obj
    (string obj)
    (character (list obj))
    (keyword (if text?
		 (write-to-string obj)
		 (symbol-name obj)))
    (t (write-to-string obj))))

(defun combine-text (&rest text)
  (let ((ret ""))
    (loop :for obj
	    :in text
	  :do (setf ret (concatenate 'string ret
				     (process-obj-for-comb-string obj :text))))
    ret))

(defun combine-strings (&rest strings)
  "Used on strings that just need to be concatenated together, but not inserting a newline at the end of each of them."
  (let ((str ""))
    (loop :for obj
	    :in strings
	  :do (setf str (concatenate 'string str
				     (process-obj-for-comb-string obj))))
    str))


(defmacro delimit-inclusive (limiter str &key (modifier 0))
  `(if (characterp ,limiter)
       (subseq ,str (+ (position ,limiter ,str) ,modifier) (length ,str))
       (if (integerp ,limiter)
	   (subseq ,str ,limiter)
	   (let ((dl (aref ,limiter 0)))
	     (subseq ,str (+ (position dl ,str) ,modifier) (length ,str))
	     ))))

(defmacro delimit-exclusive (limiter str &key (modifier 0))
  `(if (characterp ,limiter)
       (subseq ,str (+ (1+ (position ,limiter ,str)) ,modifier) (length ,str))
       (if (integerp ,limiter)
	   (subseq ,str (1+ ,limiter))
	   (let ((dl (aref ,limiter 0)))
	     (subseq ,str (+ (1+ (position dl ,str)) ,modifier) (length ,str))
	     ))))

(defmacro delimit-to-inclusive (lower-limit upper-limit str &key (modifier 0))
  `(if (and (characterp ,lower-limit)
	    (characterp ,upper-limit))
       (subseq ,str (+ (position ,lower-limit ,str) ,modifier) (position ,upper-limit ,str))
       (let ((dl (if (characterp ,lower-limit)
		     ,lower-limit
		     (if (integerp ,lower-limit)
			 (+ ,lower-limit ,modifier)
			 (aref ,lower-limit 0))))
	     (ul (if (characterp ,upper-limit)
		     ,upper-limit
		     (if (integerp ,upper-limit)
			 ,upper-limit
			 (aref ,upper-limit 0)))))
	 (if (and (integerp dl)
		  (integerp ul))
	     (subseq ,str dl ul)
	     (if (integerp dl)
		 (subseq ,str dl (position ul ,str))
		 (if (integerp ul)
		     (subseq ,str (+ (position dl ,str) ,modifier) ul)
		     (subseq ,str (+ (position dl ,str) ,modifier) (position ul ,str))))))))

(defmacro delimit-to-exclusive (lower-limit upper-limit str &key (modifier 0))
  `(if (and (characterp ,lower-limit)
	    (characterp ,upper-limit))
       (subseq ,str (+ (1+ (position ,lower-limit ,str)) ,modifier) (position ,upper-limit ,str))	   
       (let ((dl (if (characterp ,lower-limit)
		     ,lower-limit
		     (if (integerp ,lower-limit)
			 (1+ (+ ,lower-limit ,modifier))
			 (aref ,lower-limit 0))))
	     (ul (if (characterp ,upper-limit)
		     ,upper-limit
		     (if (integerp ,upper-limit)
			 ,upper-limit
			 (aref ,upper-limit 0)))))
	 (princ (integerp dl)) (princ (integerp ul))
	 (if (and (integerp dl)
		  (integerp ul))
	     (subseq ,str dl ul)
	     (if (integerp dl)
		 (subseq ,str dl (position ul ,str))
		 (if (integerp ul)
		     (subseq ,str (+ (1+ (position dl ,str)) ,modifier) ul)
		     (subseq ,str (+ (1+ (position dl ,str)) ,modifier) (position ul ,str))))))))

(defun parse-hex-color (str &key (return-type 'values))
  (if (eq (aref str 0) #\#)
      (setf str (subseq str 1)))
  (let ((r (delimit-to-inclusive 0 2 str))
	(g (delimit-to-inclusive 2 4 str))
	(b (delimit-inclusive 4 str)))
    (setf r (parse-integer r :radix 16)
	  g (parse-integer g :radix 16)
	  b (parse-integer b :radix 16))
    (case return-type
      (values (values r g b 255))
      (list (list r g b 255))
      (otherwise (values r g b 255)))))


(defun text-chunker (str &optional (line-length (/ tamias:screen-width 16)))
  (let ((rope-group nil)
	(tmp-text str)
	(str-pos 0)
	(text-length line-length));;ensures no modification to original string
    ;;originally, text-length was (or line-length (length str))
    ;;however, this ensures that anything put through text-chunker won't be cut off
    ;;due to the /viewport/ being too small, used as screen-width and screen-height
    ;;s-w and s-h will be changed to something more "sensible"
    ;;window-width and screen-width are NOT the same
    ;;screen-width is currently the size of the "render area"
    ;;This is to allow for developers to code for a particular "target size"
    ;;but it being scaled up or down
    ;;This would allow for a game to be coded around a 640x480 resolution
    ;;without having to worry how the code will work at different resolutions
    (loop :while tmp-text
       do (if (> (length tmp-text) text-length)
	      (let ((rem-text (subseq tmp-text str-pos (+ str-pos text-length))))
		(if (find #\newline rem-text)
		    (progn (push (subseq tmp-text str-pos (position #\newline rem-text))
				 rope-group)
			   (setf tmp-text (subseq tmp-text (1+ (position #\newline rem-text)))))
		    (progn (push rem-text rope-group)
			   (setf tmp-text (subseq tmp-text (+ str-pos text-length))))))
	      (if (find #\newline tmp-text)
		  (let ((rem-text (subseq tmp-text str-pos (+ str-pos (length tmp-text)))))
		    (push (subseq tmp-text str-pos (position #\newline rem-text))
			  rope-group)
		    (setf tmp-text (subseq tmp-text (1+ (position #\newline rem-text)))))
		  (progn (push (subseq tmp-text str-pos) rope-group)
			 (setf tmp-text nil)))))
    (reverse rope-group)))

#|
(defun text-chunker (str &optional line-length)
  (if (find #\newline str)
      (text-chunker-with-newline str line-length)
      (let ((rope-group nil)
	    (tmp-text str)
	    (str-pos 0)
	    (text-length (or line-length (length str))));;ensures no modification to original string
	(loop while tmp-text
	   do (if (> (length tmp-text) text-length)
		  (progn (push (subseq tmp-text str-pos (+ str-pos text-length)) rope-group)
			 (setf tmp-text (subseq tmp-text (+ str-pos text-length))))
		  (progn (push (subseq tmp-text str-pos) rope-group)
			 (setf tmp-text nil))))
	(reverse rope-group))))

|#
