(defvar tamias-text-insert nil)

(defun handle-text-input ()
  (let ((text tamias-text-insert))
    (if (eq (length (tamias-text-text current-text-context)) 0)
	(setf (tamias-text-text current-text-context) text)
	(let ((prev-text (subseq (tamias-text-text current-text-context)
				 0 (tamias-text-position current-text-context)))
	      (last-text (subseq (tamias-text-text current-text-context)
				 (tamias-text-position current-text-context))))
	  (setf (tamias-text-text current-text-context)
		(concatenate 'string prev-text
			     text
			     last-text))))
#|		
	(setf 
	      (with-output-to-string (stream)
		(write-string (subseq (tamias-text-text current-text-context)
				      0 (tamias-text-position current-text-context))
			      stream)
		(write-string text stream)
		(write-string (subseq (tamias-text-text current-text-context) (tamias-text-position current-text-context)) stream))))
    ;;				 (combine-strings current-text-context text))
|#
    (incf (tamias-text-position current-text-context) 1)
    (setf tamias-text-insert nil
	  (tamias-text-edited current-text-context) t)
    ))

(defun text-insert (text text-to-insert position-of-insertion)
  (declare (ignore text text-to-insert position-of-insertion))
  )

(defun text-down ()
  (symbol-macrolet ((position (tamias-text-position current-text-context)))
    (let ((text (tamias-text-text current-text-context)))
#|
So, whats going on here? We want to go 'down' one line when we press the down arrow key
What we need:
 - Current position of text
 - Newline positions
In order to not go ballistic with the position of the cursor
      we need the end of the next line as a bounding index for the position 
|#    
    (if (find #\newline text :start position)
	(let ((beginning-of-current-line nil)
	      (incrementer 0)
	      (end-of-current-line (position #\newline text :start position))
	      (end-of-next-line nil))
	  (if (find #\newline text :start (1+ end-of-current-line))
	      (setf end-of-next-line (position #\newline text :start (1+ end-of-current-line)))
	      (setf end-of-next-line (length text)))
	  (if (find #\newline text :end (1- end-of-current-line) :from-end t)
	      (setf beginning-of-current-line (1+ (position #\newline text
							:end position :from-end t)))
	      (setf beginning-of-current-line 0))
	  (setf incrementer (1+ (- position beginning-of-current-line)))
	  ;;to go to the max of the next line, go from 1+ position of newline on current line
	  ;;then check
	  (setf position (+ end-of-current-line incrementer))
	  (if (> position (length text))
	      (setf position
		    (length text))
	      (when (> position end-of-next-line)
		(setf position end-of-next-line))))
	(setf (tamias-text-position current-text-context)
	      (length text)))))
  (setf (tamias-text-edited current-text-context) t))


(defun text-up ()
  (symbol-macrolet ((position (tamias-text-position current-text-context))
		    (text (tamias-text-text current-text-context)))
      #|
So, we take the current position of where we are in the text-context
Then, we use it as the end point for searching for the newline character
from there, we take the cur-pos and subtract it from the nl-position
Then, we get the start of the previous-line, which is 1+ nl-position of prev-line

If newline isn't found, then we set the position to 0

|#
      #|locals: position, text, current-line-begin, prev-line-begin, |#
    (if (find #\newline text :end position :from-end t)
	(let ((current-line-begin (position #\newline text :end position :from-end t))
	      (incrementer 0)
	      (prev-line-begin nil))
	  (if (find #\newline text :end (1- current-line-begin) :from-end t)
	      (setf prev-line-begin (position #\newline text :end (1- current-line-begin) :from-end t))
	      (setf prev-line-begin -1))
	  (setf incrementer (- position current-line-begin)
		position (+ prev-line-begin incrementer))
	  (when (> position current-line-begin)
	    (setf position current-line-begin)))
	(setf position 0
	      (tamias-text-edited current-text-context) t))))

(defun add-newline-to-text ()
  (when (eq *text-input-state* 'edit)
    (setf (tamias-text-text current-text-context)
	  (with-output-to-string (stream)
	    (if (< (tamias-text-position current-text-context)
		   (length (tamias-text-text current-text-context)))
		(progn (write-string (tamias.string:combine-strings
				      (subseq (tamias-text-text current-text-context) 0 (tamias-text-position current-text-context))
				      #\newline
				      (subseq (tamias-text-text current-text-context) (tamias-text-position current-text-context)))
				     stream))
		(progn (write-string (tamias.string:combine-strings (subseq (tamias-text-text current-text-context) 0) #\newline) stream)
		       (fresh-line stream)))))
    (incf (tamias-text-position current-text-context))))

(defun text-backspace ()
  (when (and (>= (tamias-text-position current-text-context) 1)
	     (<= (tamias-text-position current-text-context) (length (tamias-text-text current-text-context))))
    (let ((pre-text (subseq (tamias-text-text current-text-context) 0 (1- (tamias-text-position current-text-context))))
	  (post-text (subseq (tamias-text-text current-text-context) (tamias-text-position current-text-context))))
      (setf (tamias-text-text current-text-context)
	    (concatenate 'string pre-text post-text))
      (decf (tamias-text-position current-text-context) 1))))

(defun text-delete ()
  (when (< (tamias-text-position current-text-context)
	   (length (tamias-text-text current-text-context)))
    (let ((pre-text (subseq (tamias-text-text current-text-context) 0 (tamias-text-position current-text-context)))
	  (post-text (subseq (tamias-text-text current-text-context) (1+ (tamias-text-position current-text-context)))))
      (setf (tamias-text-text current-text-context)
	    (concatenate 'string pre-text post-text)))))

(defun text-end-of-line ()
  (symbol-macrolet ((position (tamias-text-position current-text-context)))
    (let ((text (tamias-text-text current-text-context)))
      (if (find #\newline text :start position)
	  (setf position (position #\newline text :start position))
	  (setf position (length text))))))

(defun text-start-of-line ()
  (symbol-macrolet ((position (tamias-text-position current-text-context)))
    (let ((text (tamias-text-text current-text-context)))
      (if (find #\newline text :end position :from-end t)
	  (setf position (1+ (position #\newline text :end position :from-end t)))
	  (setf position 0)))))

(defun text-modifier-check (key)
  (case key
    (:C "Copy text")
    (:V "paste text")))
     

(defun text-key-check (key)
  (setf (tamias-text-cursor-timer current-text-context) 0
	(tamias-text-cursor-hide current-text-context) nil
	(tamias-text-cursor-blinks current-text-context) 0)
  (case key
    (:delete (text-delete))
    (:return (add-newline-to-text))
    (:backspace (text-backspace))
    (:right (when (< (tamias-text-position current-text-context) (length (tamias-text-text current-text-context)))
	      (incf (tamias-text-position current-text-context) 1)))
    (:left (when (> (tamias-text-position current-text-context) 0)
	       (decf (tamias-text-position current-text-context) 1)))
    (:down (text-down))
    (:up (text-up))
    ;;up and down for text.
    (:end (text-end-of-line))
    (:home (text-start-of-line))
    (:escape (setf current-text-context nil))
    (otherwise (text-modifier-check key))
    ))
