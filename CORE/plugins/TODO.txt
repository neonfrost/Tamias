More in depth explanation of what "plugins" do in the context of Tamias

Essentially, the goal will be that they are "mechanic" specific things. E.g. turn-based-battle.cl will be for RPGs that use a turn based battle system, platformer.cl will be for platformers

However, there will be a focus on delineating these plugins from using any "game specific" code (e.g. movement plugins won't use code from "Neon Frost's MING", but will utilize the Tamias engine and plugin specific variables [i.e. gravity for engine, friction for plugin])