;;Assuming a collision map (2d array) is provided, use some form of standardized "notation"
;;for collision within the array

#|
0000000000000000000000000000
2222222222200000000000000000
0000000000066662200000000000
0000000033333333366662200000
0000333300000000000000000000
3333000000000000000000000222
0000000000002222222225555000
0000000055550000000000000000
2222555500000000000000000000
0000000000000000000000000000

2 disables gravity,
 3 stops your upward momentum or 'says' "Your y-value cannot keep going up here"
  5 says "As your x velocity is +, go up, -, go down",
    and 6 says "As your x velocity is -, go up, +, go down"
|#

;;This is not usable code necessarily
;;It will be improved upon and there will be standardization of collision map values
;;Along with checking *where* the entity is at within a particular tile
;;(e.g. Falling onto a ramp, as of right now, would put it at the top of the tile,
;; rather than falling onto where it should fall)
(defun entity.ramp (entity tile-map)
  ;;Get entity position
  ;;Assume visible dimensions of 32x32
  (let ((entity-direction (entity-velocity-x entity)))
    (if (not (eq entity-direction 0))
	(case (aref (tamias-map-data tile-map)
		    (floor (/ (entity-x entity) 32)) (1+ (floor (/ (entity-y entity) 32))))
	  (0 (entity.move entity))
	  (2 (entity.move entity :gravity))
	  (5 (if (< entity-direction 0)
		 (entity.move entity :y-)
		 (entity.move entity :y+)))
	  (6 (if (> entity-direction 0)
		 (entity.move entity :y-)
		 (entity.move entity :y+)))))))
	  
	
#|
Interesting thing to read: http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/
|#
