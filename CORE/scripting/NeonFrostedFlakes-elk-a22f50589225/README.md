# Elk

Elk is a project that consists of a compiler (rangifer) and an interpreter (caribou) which parse Elk source code.

Elk is a lisp-like language with a focus on being 'close to the metal'. One of the goals of having Elk be lispy is that it should help facilitate ease in creating domain specific languages (DSLs)

Version 0.1 will be a boot-strapped compiler written in Common LISP

#Progress

##Version 0.01

7/30/2019: 3.0%, Elk -> IR mostly done. Still need to work on adding in define support, various hash symbol implementations, etc. ASM stl has been worked on a bit.

3/30/2019: 1.0%, Basic idea has changed within compiler. Instead of trying to work on the file 'all at once', it instead separates a file into blocks, delimited with ( and #. From there, it will work on these blocks to get them to an intermediate representation.

2/10/2019: 0.5%, work started on the assembly side of the stl

1/22/2019: 0.1%, a small amount of the specification has been declared.