stl_library = {}
current_stack_pointer = 0 -- the stack pointer is modified by 8 bytes (default size, is modifiable)
		      	  -- on function call, it sets current_stack_pointer to 8
			  -- on function exit... idk yet

function add_stl_func(name, asm_out)
	 local stl_func = {name, asm_out}
	 local placement = table.maxn(stl_library) + 1
	 table.insert(stl_library,placement,stl_func)
end

--  add_stl_func("GLOBAL_SET", "mov $1, $2")
--  add_stl_func("LOCAL_SET", "push $1") -- check to see if variable is prepended with local_ if not, use GLOBAL_SET

function stl_func_name(stl_index)
	 return stl_library[stl_index][1]
end

function stl_func_asm(stl_index)
	 return stl_library[stl_index][2]
end

variables = {}
unresolved_variables = {} -- if a variable has CALL func_name as it's value, it's pushed to unresolved_variables instead

function add_variable(name, value, scope, function)
	 local v = {name, value, scope, function}
	 local placement table.maxn(variables) + 1
	 table.insert(variables,placement,v)
end

function add_unres_variable(name, value, scope, function)
	 local v = {name, value, scope, function}
	 local placement table.maxn(unresolved_variables) + 1
	 table.insert(unresolved_variables,placement,v)
end

function variable_name(v_index)
	 return variables[v_index][1]
end

function variable_value(v_index)
	 return variables[v_index][2]
end

function variable_scope(v_index)
	 return variables[v_index][3]
end

function variable_function(v_index)
	 return variables[v_index][4] -- will be nil/empty if scope is global
end


function unresolved_variable_name(v_index)
	 return unresolved_variables[v_index][1]
end

function unresolved_variable_value(v_index)
	 return unresolved_variables[v_index][2]
end

function unresolved_variable_scope(v_index)
	 return unresolved_variables[v_index][3]
end

function unresolved_variable_function(v_index)
	 return unresolved_variables[v_index][4] -- will be nil/empty if scope is global
end


functions = {}
resolved_functions = {}

function func(name, dependencies, args, body)--  dependencies is an integer, if a dependency is found in Functions, increase dependencies by 1
	 local func = {name, dependencies, args, body}
	 local placement table.maxn(functions) + 1
	 table.insert(functions,placement,func)
end

function resolved_func(name, args, body)
	 local res_func = {name, args, body}
	 local placement table.maxn(resolved_functions) + 1
	 table.insert(resolved_functions,placement,res_func)
end