(defvar error-emitted nil)
(defvar quote-count 0)
(defvar elk-asm-instructions '("mov"))

(defun elk-error (error-type &rest messages)
  (case error-type
    (character (setf error-emitted (concatenate 'string "Character Error! Compilation choked at character: "
						(write-to-string (car messages))
						" at string position: " (write-to-string (cadr messages)))))
    (integer (setf error-emitted (concatenate 'string "Integer Error. ")))
    (unbalanced-parenthesis (setf error-emitted (concatenate 'string "Unbalanced parenthesis error! Left paren count: "
							     (write-to-string (car messages))
							     " Right paren count: " 
							     (write-to-string (cadr messages)))))
    (type-error (setf error-emitted (concatenate 'string "Error, type error of: " (car messages) " in: " (cadr messages) " of: " (caddr messages) '(#\newline))))
    (unresolved-expression (setf error-emitted (concatenate 'string "Error! Unresolved Expression: " (car messages))))
    (struct-no-parents (setf error-emitted (concatenate 'string "Error encountered while processing structure definition: "
							(car messages))))
    (spaced-function (setf error-emitted (concatenate 'string "Error! Use of a space between left-paren and symbol! " '(#\newline) (car messages))))
    (otherwise (setf error-emitted (concatenate 'string "Undefined error: " (car messages))))))


(defun find-pattern (str pattern &key (context "DEFAULT ARGUMENT"))
  "Returns the position of the beginning and end of the pattern"
  (if (stringp pattern)
      (let ((pattern-length (length pattern))
	    (pattern-position 0)
	    (current-pattern-character 0)
	    (beginning 0)
	    (end 1)
	    (pattern-found? nil)
	    (pattern-failed? nil))
	(loop while (and (< pattern-position (length str))
			 (not pattern-found?))
	   do (if (char= (aref str
			       pattern-position)
			 (aref pattern
			       current-pattern-character))
		  (progn (setf beginning pattern-position
			       pattern-failed nil)
			 (incf current-pattern-character)
			 (loop while (and (< current-pattern-character
					     pattern-length)
					  (not pattern-failed?))
			    do (if (char= (aref str
						pattern-position)
					  (aref pattern
						current-pattern-character))
				   (progn (incf pattern-position)
					  (incf current-pattern-character))
				   (setf pattern-failed? t
					 current-pattern-character 0))
			      (if (>= current-pattern-character
				      pattern-length)
				  (setf pattern-found? t
					end pattern-position))))
		  (incf pattern-position)))
	(if pattern-found?
	    (list beginning end)
	    nil))
      (elk-error 'type-error "NOT A STRING" "Finding Pattern" context)))

