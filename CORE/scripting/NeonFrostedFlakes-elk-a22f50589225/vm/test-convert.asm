find-char-ascii:
	smv 72 rq1, 72		;String tag + addr
	smv 16 rb1, 88		;character
	tmv rb2-t, int-tag	;counter
	mov 8 rb2, 0
	jmp find-char.ascii.compare_char
	.compare_char:
		cmp 8 rb1, [rq1 + 8 + rb2]
		je find-char-ascci.success
		cmp 8 rb2, [rq1]
		je find-char-ascii.fail
		inc rq2, 8
		jmp find-char.ascii.compare_char
	.success:
		jmp find-char-ascii.exit
	.fail:
		mov 64 rq2, 0
		jmp find-char-ascii.ext
	.exit:
		pop rq1
		push rq2
		jmp rq1
	
find-char:
	mov 8 rq1-t, string-tag
	smv 64 rq1, 8
	mov 8 rb1-t, char-tag
	smv 64 rb1, 

;;not elk-asm yet, ignore
find_char:
	mov rbx, [rbp + 8] ;;size
	mov rax, [rbp + 16] ;;string
	mov rcx, [rbp + 24] ;;because it's ascii, compare the byte
	mov rdx, 0
	.compare_char:
		mov ch, [rax + rdx]
		cmp ch, cl
		je find_char.success
		cmp rdx, rbx ;;compare the current index with the size of string
		je find_char.fail ;;if rdx gets to the size of the string, then it means that no characters were found that match the argument
		inc rdx
		jmp find_char.compare_char
	.success:
		jmp find_char.exit
	.fail:
		mov rdx, 0
		jmp find_char.exit
	.exit:
		pop rax	
		push rdx
		jmp rax
