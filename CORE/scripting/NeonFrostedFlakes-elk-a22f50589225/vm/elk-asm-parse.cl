"Long story short, converts elk-asm to machine-asm, i.e. x64, ARM, etc.
Since it's elk-asm -> asm, then we don't do any transformations to the function names.
At this point in time, it will be more like a macro expander than anything else.
i.e.
       mov 64 creature.hp, creature.max-hp
would be
       mov rax, [creature.max-hp]
       mov [creature.hp], rax

And to start, I'll be converting the .inc file from x64 into elk-asm"

(defvar elk-instruction-set (make-hash-table))
(let ((eis elk-instruction-set))
  (setf (gethash 'hlt eis) 0)
  )

(defmacro eis (instruction)
  `(gethash (quote ,instruction) elk-instruction-set))

(defgeneric asm.convert (instruction &optional input-1 input-2 input-3)
  )

(defmethod asm.convert ((instruction (eql 0)) &optional input-1 input-2 input-3)
  "Halts the virtual machine"
  (write-string "ret")
  )

(defmacro asm.elk (instruction &optional input-1 input-2 input-3)
  `(asm.convert (eis ,instruction) ,input-1 ,input-2 ,input-3))

(defun asm.label (label-name &rest instructions)
  (let ((rax nil)
	(rbx nil)
	(rcx nil)
	(rdx nil)
	(mem-loaded nil))
    (loop for instruction in instructions
       do (asm.elk (elt instruction 0) (elt instruction 1) (elt instruction 2) (elt instruction 3)))))
    

#|

stack_print:			;how to use! push last character, ... push first character, push length, call stack_print
	push rbp
	mov rbp, rsp
	mov rdx, [rbp + 16]
	mov rax, 8
	mul rdx
	shl rdx, 32
	mov edx, eax
	lea rsi, [rbp + 24]
	mov rax, 0x1
	mov rdi, rax
	syscall
	pop rbp
	ret

print:				;push address of string, push size of string, call print
	push rbp
	mov rbp, rsp
	sub rsp, 16
	mov rsi, [rbp + 24]
	mov rdx, [rbp + 16]
	mov rax, 0x1
	mov rdi, rax
	syscall
	pop rbp
	ret
|#
