format ELF64 executable 3
	;; Working on getting struct working
	;; currently, need to figure out how to get size of the struct
	;; from end of the struct go back 8 bytes (64 bits) and read 8 bytes to get size of string

	;; have no idea how to do this

struc string str ;struct
{
	.contents db str
	.size = $ - .contents
}

segment readable executable

entry $

	push 10
	push 30
	push 40
	push 3
	call elk_add
	push 1
	call stack_print 	;should output P, not p
	push 256
	push 20
	push 30
	push 40
	push 40
	push 5
	call elk_sub
	push 1
	call stack_print
	push suc_msg.contents
	push suc_msg.size
	push suc2.contents
	push suc2.size
	call compare_strings
	call check_success
	jmp exit

check_success:
	cmp rcx, 1
	je check_success.succ
	jne check_success.fail
	.succ:
		push suc_msg.contents
		push suc_msg.size
		call print
		pop rax
		pop rax
		pop rax
		jmp rax
	.fail:
		push f_msg.contents
		push f_msg.size
		call print
		pop rax
		pop rax
		pop rax
		jmp rax

compare_strings:
	push rbp	;set up stack frame
	mov rbp, rsp
	mov rax, [rbp + 24]	;first get the pointers of the two string structs
	mov rdx, [rbp + 16]	;get the size of the first string
	mov rbx, [rbp + 40]	; string 2
	mov r8, [rbp + 32]	
	cmp rdx, r8
	jne compare_strings.fail_cmp_strings
	mov rdx, 0
 	jmp compare_strings.compare_chars
	.compare_chars:		;I've spent about half an hour debugging this
				;In short, inc rdx was between the je and jmp instructions. It would always successfully run, but would fail every time
		mov rcx, 0	;Why? Because if r8 is 10 and rdx is 9, then it jumps back. However, it didn't actually add a success until after the length was checked
		mov cl, [rax + rdx]  ;put character of first string in lower byte of the rcx register			;So, if a string has 10 characters
		mov ch, [rbx + rdx]  ;put character of second string in upper byte of the rcx register			;You only count to '9', because rax + 0 = first char
		cmp ch, cl		;compare
		jne compare_strings.fail_cmp_strings	;if not equal, jump to fail
		inc rdx
		cmp rdx, r8
		je compare_strings.succeed_cmp_strings  ; if rdx and r8 are equal to one another, then the string comparison is finished and returns t
		jmp compare_strings.compare_chars
	.succeed_cmp_strings:
		mov rcx, 1
		jmp compare_strings.exit_cmp_strings
	.fail_cmp_strings:
		mov rcx, 0
		jmp compare_strings.exit_cmp_strings
	.exit_cmp_strings:
		pop rbp
		pop rax	
		push rcx
		jmp rax

		
	
stack_print:			;how to use! push last character, ... push first character, push length, call stack_print
	push rbp
	mov rbp, rsp
	mov rdx, [rbp + 16]
	mov rax, 8
	mul rdx
	shl rdx, 32
	mov edx, eax
	lea rsi, [rbp + 24]
	mov rax, 0x1
	mov rdi, rax
	syscall
	pop rbp
	ret
	
print:				;push address of string, push size of string, call print
	push rbp
	mov rbp, rsp
	mov rsi, [rbp + 24]
	mov rdx, [rbp + 16]
	mov rax, 0x1
	mov rdi, rax
	syscall
	pop rbp
	pop rax
	jmp rax

elk_add: ;adds numbers on the stack
	push rbp
	mov rbp, rsp
	mov rdx, [rbp + 16] ; get number of numbers
	mov rax, 8
	mul rdx
	shl rdx, 32
	mov edx, eax
	xor rcx, rcx
	jmp elk_add.add_loop
	.add_loop:
		xor rax, rax
		mov rax, [rbp + rdx + 16]
		add rcx, rax
		sub rdx, 8
		cmp rdx, 0
		ja elk_add.add_loop
		pop rbp
		pop rax
		push rcx
		jmp rax
elk_sub:
	push rbp
	mov rbp, rsp
	mov rcx, [rbp + 16] ; get number of numbers
	mov rax, 8
	mul rcx
	shl rdx, 32
	mov edx, eax
	mov rcx, [rbp + rdx + 16]
	sub rdx, 8
	cmp rdx, 0
	ja elk_sub.sub_loop
	jmp elk_sub.sub_exit
	.sub_loop:
		xor rax, rax
		mov rax, [rbp + rdx + 16]
		sub rcx, rax
		sub rdx, 8
		cmp rdx, 0
		ja elk_sub.sub_loop
		jmp elk_sub.sub_exit
	.sub_exit:
		pop rbp
		pop rax
		push rcx
		jmp rax


exit:	
	xor rax, rax
	mov rax, 60
	syscall

	
segment readable writable
suc_msg string 'success!'
suc2 string 'success!'
f_msg string 'help!'
	;; suc_msg db 'success!',0xA,0
	;; suc_msg_len = $ - suc_msg
