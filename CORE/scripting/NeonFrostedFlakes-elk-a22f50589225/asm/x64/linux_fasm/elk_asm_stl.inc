;;NOTE: 0 = fail, unsuccessful, etc.
;;	1 = success, true, etc.
;;
;;Other notes:
;;RAX is primarily used to store the jump location on function exit
;;i.e. pop rax, PUSH [VAL], jmp rax
;;
;;So, any return values will not be stored in rax
;;


find_char:
	mov rbx, [rbp + 8] ;;size
	mov rax, [rbp + 16] ;;string
	mov rcx, [rbp + 24] ;;because it's ascii, compare the byte
	mov rdx, 0
	.compare_char:
		mov ch, [rax + rdx]
		cmp ch, cl
		je find_char.success
		cmp rdx, rbx ;;compare the current index with the size of string
		je find_char.fail ;;if rdx gets to the size of the string, then it means that no characters were found that match the argument
		inc rdx
		jmp find_char.compare_char
	.success:
		jmp find_char.exit
	.fail:
		mov rdx, 0
		jmp find_char.exit
	.exit:
		pop rax	
		push rdx
		jmp rax

compare_strings:
	push rbp	;set up stack frame
	mov rbp, rsp	;call compare_strings like: push a.conts push a.size push b.c push b.s
	mov rax, [rbp + 16]	;first get the pointer for the contents of the two string structs
	mov rdx, [rbp + 24]	;get size of string 2
	mov rbx, [rbp + 32]	; string 1
	mov r8, [rbp + 40]	;size of string 1
	cmp rdx, r8
	jne compare_strings.fail_cmp_strings
	mov rdx, 0
	jmp compare_strings.compare_chars
	.compare_chars:
		mov cl, [rax + rdx]  ;put character of first string in lower byte of the rcx register
		mov ch, [rbx + rdx]  ;put character of second string in upper byte of the rcx register
		cmp ch, cl	     		      	;compare
		jne compare_strings.fail_cmp_strings	;if not equal, jump to fail
		cmp rdx, r8
		je compare_strings.succeed_cmp_strings  ; if rdx and r8 are equal to one another, then the string comparison is finished and exits with success
		inc rdx
		jmp compare_strings.compare_chars
	.succeed_cmp_strings:
		mov rcx, 1
		jmp compare_strings.exit_cmp_strings
	.fail_cmp_strings:
		mov rcx, 0
		jmp compare_strings.exit_cmp_strings
	.exit_cmp_strings:
		pop rbp
		pop rax	
		push rcx
		jmp rax

compare_utf8_strs:
	push rbp
	mov rbp, rsp
	;;pointer1
	;;pointer2
	;;compare_utf8_char
	;;push rcx ;;offset
	;;push current_character
	;;push pointer_1+rcx
	;;push pointer_2+rcx
	call compare_utf8_char
	pop rdx
	pop rax
	pop rax
	pop rax
	pop rcx
	add rcx, rdx
	inc rax
	mov rbx, [rbp + 24] ;;string length
	ret

compare_utf8_char: ;note you fucking idiot, this took way longer to figure out than it should have 
	push rbp ;I'm still not sure how this was supposed to work
	mov rbp, rsp ;as of 7/25/19 it's still untested
	mov r8, [rbp + 16] ;first character   ; you absolute fool
	mov r9, [rbp + 24] ;second character  ; you absolute idiot
	mov rcx, 0	;Why are you like this?
	mov rdx, 4  ;rdx IS LENGTH OF UTF8_CHAR! DOCUMENT YOUR FUCKING CODE! ONLY A FUCKING HOUR TO FIGURE IT OUT! ;Why? Why? Why? Why?
	mov rax, 247  ;so Let's figure this out, shall we? So first, this is supposed to compare 2 utf8 characters
	mov rbx, 240  ;So first, we get pointers to where these are located. Hypothetically, it'll read it as a 64bit value, so as long as the utf_8 char resides on
	jmp compare_utf8_char.get_length    ; 0xFuckmeDaddy then it should read it correctly. When the utf8_char is loaded, it'll load 8 bytes into a register
	.get_length:			    ; Security issue? Don't care at this point. So, it'll load 00111101.01010000.00000000.01010101.11111111.11111111.11111111.11111111
		mov cl, [r8] ;;get the first byte of [pointer]		however, we only care about those first 4 bytes (00111101....)
		cmp cl, al   ;CL is the length of the character!	And AGAIN I'M BRAIN DEAD...It loads the **pointer** not the actual contents of the address. Me Dumb Dumb
		ja compare_utf8_char.fail_check ;al is the max length of a character (4 bytes)   	    		    	    	   You Grug. Grug got my Rock
		jbe compare_utf8_char.test_reg  ;So, if cl is bigger than al, say 248, then it fails, if it's smaller, then it....	   Grug die for Rock
	.test_reg:  				;You know how I said I was a fucking idiot? Well, turns out I can't fucking read either
		cmp cl, bl			;in .dec_test_reg, rbx is shifted by 1 bit to the left (11110 -> 1110x)
		jae compare_utf8_char.reset_reg_c ;and then in this labeled procedure, cl is tested against bl, to see if ....
		jb compare_utf8_char.dec_test_reg ;    	       <--- 	    	       You know, I take my galaxy brain intelligence for granted
	.dec_test_reg:				  	       			       ; A lot. A LOT. so RAX is the max size of the first byte and RBX is the min size
		shl rax, 1 ;From 11101111 to 11011110 OR 11110111 to 11101110  i.e. changing the length from '4' to '3'          so if 240 <= cl <= 247, then it's a 4 byte char
		inc rax, 1 ;from 11011110 to 11011111 OR 11101110 to 11101111       and then comparing it to the value of cl     224 <= cl <= 239, 3 chars, 192 <= cl <= 223, 2 bytes
		shl rbx, 1 ;So what's rbx's role here? fuck if I know. Will probably get rid of this line in the future	  	 192 > cl, 1 byte
		dec rdx, 1  ;since rdx has the 'total' length, we decrease it by 1.    	     	
		cmp cl, al	;Still length!								PLEASE PLEASE PLEASE
		jbe compare_utf8_char.test_reg							;	       DOCUMENT
		cmp al, 191									;	        YOUR
		ja compare_utf8_char.dec_test_reg						;		CODE	This procedure took me an hour and a half to decipher
		jb compare_utf8_char.reset_reg_c	;;;!!!!This is where the relations of rax rbx and rcx to the procedure changes
	.reset_reg_c:
		mov rcx, 0	;;;;rcx then becomes a counter or 'timer' for the current character rcx -> 0 is the first character rcx -> 1 is the second character, and so on
		jmp compare_utf8_char.compare_chars
	.compare_chars:
		mov al, [r8 + rcx]  ;;Me Big Brain
		mov bl, [r9 + rcx]  ;;AL be byte of r9 + rcx and BL be r10 + rcx
		cmp al, bl     	     ;;Me think AL BL get sized. Same size? Win! Not Same size? OFF WITH HEAD!!!
		je compare_utf8_char.check_next
		jne compare_utf8_char.fail_check
	.check_next:
		cmp rcx, rdx  ;;If counter equals size, then pass check
		je compare_utf8_char.pass_check
		inc rcx, 1   ;;if size not equal, increase rcx by 1, compare next chars
		jmp compare_utf8_char.compare_chars
	.fail_check:
		mov rdx, 0
		jmp compare_utf8_char.exit
	.pass_check:
		jmp compare_utf8_char.exit
	.exit:
		pop rbp
		pop rax
		push rdx
		jmp rax
		
	
elk_add: ;adds numbers on the stack
	push rbp
	mov rbp, rsp
	mov rdx, [rbp + 16] ; get number of numbers
	mov rax, 8    ;;So...originally this was going to be 64bit integers. However, it has come to my attention that I will have to include a type check and...jebus chrust
	mul rdx	 ;;multiply the number of numbers by 8. is it because ints at this point were decided to be 64bits long?...yes...they...oh god
	shl rdx, 32  ;;set edx to 0      ;;So right now, I think the best thing to do would be to...uh........Idk? There's too many questions now	    
	mov edx, eax ;;mov eax, result of rdx * rax, into edx. ;;Like, if I'm adding variably sized numbers together, should it...yes it should raise an error...but then it makes it into a pain in the ass
	xor rcx, rcx  ;;I don't want these procedures to get into this massive debate about shit. Like, if I use pointers, then I have to get...well...if I push the value
	jmp elk_add.add_loop	     ;;of the pointer onto the stack, then it won't be a problem. Maybe...maybe I shouldn't worry about it right now? 
	.add_loop:		     ;;This is only version 0.01 of ELK, so I'm only interested in getting it up and running and able to compile a compiler
		xor rax, rax	     ;;made in ELK. 	    Yeah, for the time being, I'll push the values pointed at onto the stack, and leave this alone for the time being
		mov rax, [rbp + rdx + 16]   ;;Eventually, I'll change this procedure to check for the size of the args before it does stuff
		add rcx, rax  	      	    ;;Or, I'll add in labels to 'type check' (ensuring that 127 + 230 are bytes, and return 102)
		sub rdx, 8		    ;;Possibly even having a converter to surround the add/sub/div/etc. functions (i.e. (convert (+ 255 255) 'byte))
		cmp rdx, 0
		ja elk_add.add_loop
		pop rbp
		pop rax
		push rcx
		jmp rax
	
elk_sub:
	push rbp
	mov rbp, rsp
	mov rcx, [rbp + 16] ; get number of numbers
	mov rax, 8
	mul rcx
	shl rdx, 32
	mov edx, eax
	mov rcx, [rbp + rdx + 16]
	sub rdx, 8
	cmp rdx, 0
	ja elk_sub.sub_loop
	jmp elk_sub.sub_exit
	.sub_loop:
		xor rax, rax
		mov rax, [rbp + rdx + 16]
		sub rcx, rax
		sub rdx, 8
		cmp rdx, 0
		ja elk_sub.sub_loop
		jmp elk_sub.sub_exit
	.sub_exit:
		pop rbp
		pop rax
		push rcx
		jmp rax
	

stack_print:			;how to use! push last character, ... push first character, push length, call stack_print
	push rbp
	mov rbp, rsp
	mov rdx, [rbp + 16]
	mov rax, 8
	mul rdx
	shl rdx, 32
	mov edx, eax
	lea rsi, [rbp + 24]
	mov rax, 0x1
	mov rdi, rax
	syscall
	pop rbp
	ret

print:				;push address of string, push size of string, call print
	push rbp
	mov rbp, rsp
	sub rsp, 16
	mov rsi, [rbp + 24]
	mov rdx, [rbp + 16]
	mov rax, 0x1
	mov rdi, rax
	syscall
	pop rbp
	ret

;;;;section below is heavily wip
;;;;a complete overhaul is probably in order
;;;;needs to use malloc for dynamically allocating memory
;;;;or write my own routines
m_alloc:
;;I'm not entirely sure how to proceed with this. Basically, sys_brk needs an address, not number of bytes (sbrk needs bytes)
;;I think I'll just have to use mmap instead of sys_brk, would've been nice to know that from the get go
	push rbp
	mov rbp, rsp
	mov rdi, [rbp + 16] 
	mov rax, 12  	;call sys_brk with number of bytes (popped from stack)
	syscall ;;should return a pointer
	mov rsp, rbp
	pop rbp
	cmp rax, 0
	jl exit
	ret ;on ret, move the value of rax into whatever needed a pointer

free:	;will need to use munmap
	

open_file:
;;;Basically, do the same thing
;;;same thing as what? Why am I being an idiot?
	push rbp
	mov rbp, rsp
;I'm really not sure how to do this.
;like, I don't know how opening a file on linux really works
;i.e. do I need to have a /string/ or can I have the start of an address and point to the address like a string?
;regardless, open_file will need to move stuff onto the stack a byte at a time if I can use the stack
;	mov rcx, [rbp + 16] ;get the size of the file-name
;	mov rax, 8
;	mul rcx
;	shl rdx, 32
;	mov edx, eax
	lea rsi, [rbp + 16] ;get address of string
	mov rax, 2
	mov rsi, 0
	mov rdx, 0664
	syscall
	mov rsp, rbp
	pop rbp
	pop rbx
	push rax
	jmp rbx

read_file:

write_file:

;;load_file: Does not need to be in here. load-file, load-library, load-[X] are all high-level functions
;;what they do is that they load the files into the compiler. Then the compiler compiles them into asm, therefore, load-[X] is not needed
;;actually, I do need to have load_file in here
;;it has to concatenate files together as a string
;;
;;again, why I am being stupid?

;makes a pointer to a string for use with hash tables, so hash-table = { pointer:pointer }, if key-test is successful, returns 2nd pointer, otherwise nil ------- UPDATE: Generalized string structure

struc string str ;struct
{
	.contents db str
	.size = $ - .contents
}
struc sized_string str, size ;struct
{
	.size db size
	.contents db str
}

;;so how does a hash table work?
;;HAHAHAHAHAHAHA
;;So, first you make a hash table
;;this reserves 60 quad words + 1 byte, 60 pointers, 1 byte for the highest index
;;next, you create a hash entry
;;a hash entry has a key, a key-size, the contents, and the size of the contents
;;so a key: "lmno", key size: 4, contents: 248, content size: 1
;;so hash_entry: key = string : "lmno" . 4
;;   		 contents = hash_contents : 1 , 248
;;well, wait, would I need to do that?
;;...no, no I wouldn't.
;;if it's a pointer, then there's no reason for hash_contents to exist
;;
;;Are you plagued with overthinking? Are your friends and family worried about you?
;;Are your projects mammoths?
;;Talk to your doctor today and see if marijuana is right for you!

;;So how does it actually work?
;;Well, it's the same as above, except the pointer (.contents) will change depending on what you do
;;if you just change the value from one int to another int, then it just accessess the pointer and changes the value at that memory address
;;if you are changing from an int to a string (example-wise) then it unmaps the memory for pointer of .contents
;;then it maps new memory, and changes the .contents pointer to the new pointer

;;struc hash_contents reader, contents
;;{
;;	.reader db reader ;;size
;;	.contents db contents
;;}

struc elk_array dimensions, entry_size, num_contents, contents
{
	.dimensions db dimensions
	.reader db entry_size ;;1 byte, 2 byte, etc.
	.size dw num_contents * dimensions ;;add 2 bytes to access contents
	.contents db contents
}
;;so, using the elk_array
;;for 2d:
;;arr elk_array 2, 1, 7, 'ПроблемПроблем'
;;This is technically for variable declaration
;;will study FASM macros to determine a better way to do var decs
;;
;;declaring as a local:
;;push rax ;;hypothetical register used for a dimensional counter
;;push dimensions
;;push entry_size
;;push num_contents
;;push [address of start of contents]
;;call make_array
;;
;;Arrays with bigger than 1 byte reader
;;.insert_value
;;mov [memory_address + mem_addr_offset], [value]
;;inc mem_addr_offset
;;cmp reader, 0
;;jne .is_buffing
;;je .offset_counting
;;
;;.offset_counting
;;dec offset_counter
;;cmp offset_counter, 0
;;je .exit
;;jne .insert_value
;;
;;.buffing	;;NOTEEEEE!!!!!!!!!!!!!! buffering is only necessary for 'empty' arrays. i.e. (make-array '(4 5)) not (make-array '(4 5) '((1 1 1 1 1) (1 1 1 1 1) (1 1 1 1 1) (1 1 1 1 1)))
;;mov [memory_address + mem_addr_offset], 0
;;dec reader
;;inc mem_addr_offset
;;cmp reader, 0
;;jne .buffing
;;mov reader, [rbp + 8]
;;jmp .offset_counting
;;
;;
;;
;;multi-dimensional arrays
;;creation
;;
;;create an array
;;return the pointer
;;
;;[1 2]
;;since a multi-dimensional array would uh...
;;single-dimension:
;;mov rcx, [rbp + 16] ;array pointer
;;mov rdx, [rbp + 24] ;array index
;;mov rax, 8
;;mul rdx
;;shl rdx, 32
;;mov edx, eax  ;;rdx is now offset 1 -> 8, 2 -> 16, 20 -> 160
;;mov rbx, [rcx + 2 + rdx]
;;pop rax
;;push rbx
;;jmp rax
;;
;;array stored like so:
;;[entry, entry, entry, entry] 2d: [[entry, entry, entry] [entry, entry, entry]]
;;1d: index * size_of_entry
;;2d: array of arrays, indice_1 * size_of_inner_array (4 + num_conts * size_of_conts)



struc hash_entry key, key_size, contents_reader, contents ; pointer is dynamically defined, i.e., allocate memory then set pointer
{
	.key dq sized_string key, key_size
	.reader db contents_reader
	.contents db contents
}
struc hash_table ; array? and then loop through the array until 0 is reached?
{
	.keys resq 60
	.highest_empty_index db 0
}

make_hash_key:
	;;notes not finished
	;;first get the string and size of string from stack
	;; get the nth of the hash_table
	;;By the end: dynamically define a hash_entry, set the pointer
	;;
	;;After getting the string and the size of the string from the stack
	;;create the sized_string structure, put the pointer to this string somewhere
	;;next ask f

add_key:
	mov rdx, [rbp + 8] ;;highest_empty_index ;; should be a pointer 
	mov rbx, [rbp + 16] ;;keys array address ;; should be a pointer ;; load hash_table.keys into rbx, i.e. the address at which the table starts
	mov rdx, [rdx]
	cmp rdx, 59 ;;check if max keys have been reached
	ja add_key.fail
	mov rax 8
	mul rdx
	shl rdx, 32
	mov edx, eax	
	cmp [rbx + rdx], nil	;;check if the key at highest_empty_index is actually empty
	je add_key.insert_contents
	cmp [rbx + rdx], 0
	je add_key.insert_contents
	xor rdx, rdx
	;;if not empty then either start at index 0, inc by 8 bytes (1 quadword) until empty index
	;;empty index = nil, i.e. either 0 or points to nil
	mov rcx, [rbx + rdx]
	jmp add_key.check_index
	.check_index:
		cmp rcx, nil ;;compares rcx to the *pointer* of nil
		je add_key.insert_contents ;;
		cmp rcx, 0 ;;compare rcx to 0
		je add_key.insert_contents ;;
		inc rdx
		cmp rdx, 59 ;;check if max keys have been reached
		ja add_key.fail
		;;load hash_table.keys + rax into rcx
		mov rcx, [rbx + rdx * 8]
		jmp add_key.check_index
	.insert_contents:
		;;needs to get the sized string address
		mov rdx, [rbp + 8]
		inc rdx
		jmp add_key.exit
	.fail:
		mov rdx, 0
		jmp add_key.exit
	.exit:
		pop rax
		push rdx
		jmp rax

print_keys:
	mov rax, [rbp + 8]  ;;highest index
	mov rbx, [rbp + 16] ;;keys array
	xor rcx, rcx
	jmp print_keys.get_key_names
	.get_key_names:
		cmp rcx, rax
		jbe print_keys.print
		ja print_keys.exit
	.print:
		push rcx
		mov rbx, [rbx + rcx]
		mov rdx, [rbx+string.contents]
		push rdx
		mov rdx, [rbx+string.size]
		push rdx
		call print_stack
		pop rdx
		pop rdx
		pop rcx
		inc rcx
		jmp print_keys.get_key_names
        .exit:
		pop rax
		jmp rax

make_hash_table:

;;;struct array size, size_of_elements ;s_o_e in byte-length (1, 2, 3...)
;;;       .s_o_e db size_of_elements
;;;       .size dw size
;;;       .contents res[size_of_elements] [size]
;;;ends
;;;struct dynamic_array init_size, size_of_elements
;;;
;;;ends
;;;
;;;;note: fix stack frame stuff
;;;;	  currently, pop {ret}, push {val}, jmp {ret} is not freeing up stack space
;;;;	  Well, actually, maybe not.

nil dq 0
bool_s db 1
char_s db 1
int_s db 4
signed_int_s db 4
big_int_s db 8
key_s db 2
float_s db 10
num_mem_obj db 0
num_pages db 0 ;;every time mmap is called, add 1, every unmap dec 1
mem_used db 0 ;;add size of memory allocated, dec size freed