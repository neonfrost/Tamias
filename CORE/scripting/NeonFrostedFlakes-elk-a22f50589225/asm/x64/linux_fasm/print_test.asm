format ELF64 executable 3

segment readable executable

entry $
	push 0xA
	push 'l'
	push 'l'
	push 'i'
	push 'w'
	push 5
	call print_stack
	push 20
	push 20
	push 20
	push 10
	push 4
	call begin_add
	push 1
	call print_stack
	push 255
	push 128
	push 64
	push 3
	call begin_sub
	push 0xA
	push r8
	push 2
	call print_stack
	jmp exit
	
print_stack:			;how to use! push last character, ... push first character, push length, call print_stack
	push rbp
	mov rbp, rsp
	mov rdx, [rbp + 16]
	mov rax, 8
	mul rdx
	shl rdx, 32
	mov edx, eax
	lea rsi, [rbp + 24]
	mov rax, 0x1
	mov rdi, rax
	syscall
	mov rsp, rbp
	pop rbp
	ret

begin_add:
	push rbp
	mov rbp, rsp
	mov rdx, [rbp + 16] ; get number of numbers
	mov rax, 8
	mul rdx
	shl rdx, 32
	mov edx, eax
	xor rcx, rcx
	jmp add_loop
	
add_loop:
	xor rax, rax
	mov rax, [rbp + rdx + 16]
	add rcx, rax
	sub rdx, 8
	cmp rdx, 0
	ja add_loop
	mov rsp, rbp
	pop rbp
	pop rax
	push rcx
	jmp rax

begin_sub:
	push rbp	; get number of numbers
	mov rbp, rsp
	mov rcx, [rbp + 16]
	mov rax, 8
	mul rcx
	shl rdx, 32
	mov edx, eax
	mov rcx, rdx
	xor r8, r8
	mov r8, [rbp + rcx + 16]
	sub rcx, 8
	cmp rcx, 0
	ja sub_loop
	mov rsp,rbp
	pop rbp
	ret
	
sub_loop:
	xor rax, rax
	mov rax, [rbp + rcx + 16]
	sub r8, rax
	sub rcx, 8
	cmp rcx, 0
	ja sub_loop
	mov rsp,rbp
	pop rbp
	ret


exit:	
	xor rax, rax
	mov rax, 60
	syscall
	
segment readable writable
