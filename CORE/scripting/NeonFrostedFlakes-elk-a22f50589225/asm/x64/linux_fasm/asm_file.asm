format ELF64 executable 3

segment readable executable

entry $
	xor r8, r8
	xor r9, r9
	mov r9b, byte ptr char_len
	mov rax, 2
	lea rdi, [file_name]
	mov rsi, 0
	mov rdx, 0664
	syscall
	mov [file_descriptor], rax
	mov rdi, rax
	mov rax, 0
	mov rsi, file_data
   	xor rdx, rdx
   	mov edx, dword ptr file_len
	syscall
	jmp push_char
	
	
push_char:			
	lea rbx, [file_data]
	add rbx, r8
	mov bl, byte ptr rbx
;;	mov bl, byte [file_data + r8]
	mov byte ptr char_out, bl
	cmp bl, 0
	jb close_exit
	je close_exit
	jmp read_char
	
read_char:
	push char_out
	push r9
	call print
	inc r8
	jmp push_char

print:
	push rbp
	mov rbp, rsp
	sub rsp, 16
	mov rsi, [rbp + 24]
	mov rdx, [rbp + 16]
	mov rax, 0x1
	mov rdi, rax
	syscall
	mov rsp, rbp
	pop rbp
	ret

close_exit:
	mov rax, 3
	mov rdi, [file_descriptor]
	syscall
	jmp exit
exit:
	xor rax, rax
	mov rax, 60
	syscall


segment readable writable
	;; file_name resb 256
file_name db '/srv/ftp/incoming/ASM/fasm proj/rangifer/elk/stl.txt'
file_data rb 65536
file_descriptor rq 1
file_len dd 65536
char_out db 0
char_len db 1
suc_msg db 'success!',0xA,0
suc_msg_len = $ - suc_msg	
