(defvar registers '("rax" "rbx" "rcx" "rdx")) ; pop register, remove first register 
(defvar used-registers nil)                   ; on a register no longer being necessary, pop off used-registers and push it onto registers
(defvar current-stack-pointer 0)              ; the stack pointer is modified by 8 bytes (default size, is modifiable)
					      ; on function call, it sets current-stack-pointer to 8
					      ; on function exit... idk yet
(defvar current-stack-size 0)                 ; functions as a sort of base pointer, primarily used when referring to pre-call variables
(defvar asm-functions (make-hash-table))
(defvar elk-sizes '((integer 4)
		    (character 1)
		    (hashtable 832) ;52 * 9, 1 byte for key, 8 bytes for value
					;note on hashtables: key, value is pointer,pointer, so each hashtable entry needs 16 bytes, therefore 52 * 16 is more appropiate
		    ))
(setf (gethash 'set asm-functions) "mov 1, 2")

(defmacro get-elk-size (symbol)
  `(cadr (assoc ,symbol elk-sizes)))

(defvar resolved-dependencies nil)
;;a list of symbols
;;a function or variable that has a dependency will not resolve
;;until that dependency is found in the resolved-dependencies
;;list. Anything located in the STL will not be considered a
;;dependency (is "void" in C considered a dependency?).

(defun ir-stl-func (func-name &rest args)
  (let ((asm-string "")
	 (asm-ir-string (gethash func-name asm-functions))) ;"mov 1, 2" or "add 1, 2"
     (setf asm-string (concatenate 'string (subseq asm-ir-string 0 (1+ (position #\space asm-ir-string))) "")
	   asm-ir-string (subseq asm-ir-string (1+ (position #\space asm-ir-string))))
     (loop for arg in args
	do (let ((current-arg (write-to-string (1+ (position arg args))))
		 (arg-string (if (not (stringp arg))
				 (write-to-string arg)
				 arg)))
	     (setf asm-string (concatenate 'string asm-string arg-string))
	     (if (find #\space asm-ir-string)
		 (setf asm-string (concatenate 'string asm-string ", ")
		       asm-ir-string (subseq asm-ir-string (1+ (position #\space asm-ir-string))))
		 (if (find #\, asm-ir-string)
		     (setf asm-string (concatenate 'string asm-string ",")
			   asm-ir-string (subseq asm-ir-string (1+ (position #\, asm-ir-string)))) ; Plan: find nearest comma, subseq from there + 1
		     (setf asm-string (concatenate 'string asm-string '(#\newline)))))))
#|	     (if (> (length asm-ir-string) 0)
		 )|#				      
#|     (setf asm-string (concatenate 'string (subseq asm-ir-string 0 (position #\space asm-ir-string))
				   (if (not (stringp (nth 0 ,args)))
				       (write-to-string (nth 0 ,args))
				       (nth 0 ,args))
				   (if arg2
				       (concatenate 'string ", "
						    (if (not (stringp (nth 0 arg2)))
							(write-to-string (nth 0 arg2))
							(nth 0 arg2))
						    
						    '(#\newline)))))
|#
     asm-string))
#|

NEW WAY
Elk source -> IR (i.e. data structures of each function, etc.)
IR resolution
[ Stage 1 - Resolve STL, collect non-STL........Stage 2 - Resolve Non-STL that only has dependencies on STL.........
.....Stage 3 - Resolve Non-STL that has dependencies on Non-STL...........Stage 4 - Repeat Stage 3 until Elk program is resolved]
ASM resolution
[ Stage 1 - Convert Resolved IR into ASM...........Stage 2 - Run ASM through Fasm.........Stage 3 - Done]

So example time!
ELK SRC
(defstruct (parrot
	   :parents Avian Tropical-Animal)
	   (speech-capable t))
CL IR
(elk-structure
:name "PARROT"
:parents (AVIAN TROPICAL-ANIMAL)
:slots "SPEECH-CAPABLE"
:dependencies (AVIAN TROPICAL-ANIMAL))
ASM
begin Parrot, [true_value]
      [Tropical-Animal slots]
      [Avian Slots]
      .speech_capable
end






The below is no longer what I'm doing, exactly




DEFINE variable STRUCTURE_CREATE structure-name field1 field2 fieldn
|
V
[structure call dependent on asm used]
variable structure_name field1,field2,fieldn


Idea is essentially:

SET variable CALL add 1 3 4 10

|
|
V

(let ((register (pop registers)))
  (ir-stl-func ADD "1" register)
  (ir-stl-func ADD "3" register)
  (ir-stl-func ADD "4" register)
  (ir-stl-func ADD "10" register)
  (ir-stl-func SET "variable" register)
  (push register registers))

so essentially, with SET, resolve the value first, then ''SET'' the variable to that value
|#
(defstruct elk-resolved-block
  name
  type ;;Var, Func, Struct, etc. MACROS ARE NOT HERE. Macros are expanded during resolution
  body
  )

(defvar type-sizes '(('string "db") (float "?") (integer "db") (hex "?") (symbol "dq") (char "dd")))


(defun elk-function-call (str)
  (concatenate 'string "call " str "e\newline"))

(defun elk-resolve-structure (elk-block)
  (if (not (elk-block-dependencies elk-block))
      (elk-resolve-defstruct elk-block)
      (let ((dependcies? nil))
        (loop for dependency in (elk-block-dependencies elk-block)
	  (if (not (find dependency resolved-dependencies))
	      (setf dependencies? t)))
	(if (not dependencies?)
	    (elk-resolve-defstruct elk-block)
	    ))))

(defun elk-resolve-defstruct (elk-block)
  (let ((resolved-string "struc ")
       	(slot-type nil))
    (combine-strings resolved-string (elk-block-name elk-block) " ")
    ;;Struc structure: struc STURCT_NAME STRUCT_SLOT_1, STRUCT_SLOT_2, STRUCT_SLOT_N '(#\newline) { (newline) #\TAB .SLOT_1 STRUCT_SLOT_1
    ;;label here? Refer to ASM libs
    (loop for slot in (elk-block-slots elk-block)
    	  while (not elk-error)
      do (combine-strings resolved-string "." (elk-slot-name slot) " ")
         (if (elk-slot-value slot)
             (if (char= (aref (elk-slot-value slot)) #\#)
	         ;;;hash resolution...should it really be here?
		 (setf slot-type (resolve-elk-slot (elk-slot-ovalue slot)))
		 ));;placeholder R-parens
	 (if (not elk-error)
	     (combine-strings resolved-string (cadr (assoc slot-type type-sizes)))));;strings may require a LOT of extra work, need to check (struct in struct)
	 ;;then the 'actual' slot value (convert as need be)
	         
    )


(defun resolve-elk-slot (slot-str)
  (cond ((char= (aref slot-str 0) #\")
  	 'string)
	((find-if #'digit-char-p slot-str)
	 (if (find-if #'alpha-char-p slot-str)
	     (loop for ,,,,,,,,) ;;how do I define hexadecimal, i.e.: 0x1f3d?
	     ;;if there isn't anything past f, or starts with 0x, then it's hex
	     ;;otherwise throw an error
	     (if (find #\. slot-str)
	     	 'float
		 'integer)))
	((char= (aref slot-str 0) #\')
	 'symbol)
	((char= (aref slot-str 0) #\\)
	 'char)
	;;array
	 ))

