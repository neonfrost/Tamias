Dynamic Memory manager in Elk needs several parts:

number of objects in memory
total memory used
total pages used

Types:

 MEM_LAYOUT:
 Variable:
	type - 8 bits
;;;;;;;;	assignment - 8 bits - 0 not assigned (i.e. is returned from a function, used for garbage collection), 
;;;;;;;;  Should assignment actually be used? 
	value - min 1 bit, struct is pointer

Type IDs: 1xxxxxxx is reserved for "Initialized as nil", i.e. (defvar x nil) is [00000000, 0^64] then (setf x 10) is [10000001, 00...1010] while (defvar x 10) is [0...1, 0...1010]

 Array:
	type - Array
	value - pointer to start of array
	      Array type:
	      	    .reader and stuff
	
Structure:
    Tags - pointer to list
    size - 
    slot A - 
    Slot N - 
