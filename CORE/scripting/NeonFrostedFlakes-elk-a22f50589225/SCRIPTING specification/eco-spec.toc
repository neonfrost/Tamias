\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Why Elk?}{2}
\contentsline {section}{\numberline {3}A preamble of sorts}{3}
\contentsline {subsection}{\numberline {3.1}Various notes}{3}
\contentsline {section}{\numberline {4}Syntax}{4}
\contentsline {subsection}{\numberline {4.1}Basic Syntax}{4}
\contentsline {subsection}{\numberline {4.2}Hash Symbols}{4}
\contentsline {subsection}{\numberline {4.3}Syntax Example}{6}
\contentsline {section}{\numberline {5}Software that will be written with Elk}{7}
\contentsline {section}{\numberline {6}Standard Library}{8}
\contentsline {subsection}{\numberline {6.1}types}{8}
\contentsline {subsection}{\numberline {6.2}Functions}{8}
\contentsline {subsection}{\numberline {6.3}regex engine}{9}
\contentsline {subsection}{\numberline {6.4}errors}{9}
