Functions:
+ PLUS
- MINUS
* MULTIPLY
/ DIVIDE
print
read-file (file ''stream'')  -- version 0.01 reads entire file as a character array
write-file (file ''stream'') -- version 0.01 overwrites file
set (var[i] val)

conditionals
if (condition body optional-else)
#|
structured like so:
(if t
    (print "x")
    (add-10 x)
 else (if cond2
          (print x)
	  (free x))
      (if cond3
      	  body3))
essentially, the else goes through the conditions to see which one has been met
|#

array-related:
make-array
array (var i)

hash functions:
make-hash-table - default size of 52 keys
find-key (key table) - returns position if found, nil if not
get-hash (key table)
keys (table)

regex-related functions:
find-pattern (pattern source) - returns a list of points in source where pattern is found
peek (index source &optional peek-amount) - gets the character at index + peek-amount (default 1), of source, returns string
find (character source) - returns position if found
position (character/integer source) - returns integer if character used, returns char if int is used
count (character source) - retruns integer of how many characters found in source

Note:
I was half correct about how to do local variables. For version 0.1, just do what I was going to do. Get the compiler up and running and then rebuild it, better, in Rangifer
essentially you push the values of what the function is being called with
(+ 1 2 3 4 5 6) -> push 6 push 5...push 1 push 7[amount of numbers] call add -> mov rbp, rsp
       	     	   	       		       		   	    	     	length: rbp + 8, 1: rbp + 16, ...etc.
										
associative array lay out:
	    "identifier" -> id-num -> array[i] -> val
 	    (get-hash :fuck my-table) -> #T (indice (find-key :fuck (keys my-table))) T#
	    	      	    	      -> (array my-table indice)
	    	      	    	      -> "big titty bitches!"


quick thought on ecs vs ass-array:
      ecs: [key] -> struct, uses key as an offset of where the ecs entries start
      ass-array: [key] -> pointer -> value, has values everywhere, not sequential
