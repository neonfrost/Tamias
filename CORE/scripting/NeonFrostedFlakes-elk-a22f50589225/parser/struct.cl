(defun esb-process-defstruct (str)
  ;;(defstruct name slots) || (defstruct (name (parent parent-slots:values)) slots)
  (format t "Process structure ~%")
  (let ((string-place (position #\space str))
	(slots-start 0)
	struct-name-position
	elk-struct-slots ;;push the slots into this then reverse that list
	struct-parents)
    ;;first start with the name
    ;;if elk-peek = left-paren, then handle parent
    (if (char= (elk-peek str string-place) #\()
	(progn
	  (incf string-place 2)
	  (if (< (position #\space str :start string-place) (position #\( str :start string-place))
	      (setf struct-name-position (list string-place (position #\space str :start string-place)))
	      (setf struct-name-position (list string-place (position #\( str :start string-place))))
	  (setf string-place (position #\( str :start string-place))
	  (if (or (char= (elk-peek str string-place) #\()
		  (char= (aref str string-place) #\())
	      (let ((paren-count 1)
		    (parent-name-start 0)
		    (parent-name-end 1)
		    parent-slots-end
		    parent-name
		    parents-string
		    (parent-start string-place)
		    parent-end
		    (current-string-pos 0))
		(elk-paren-loop str string-place
		  (incf string-place))
		(setf parent-end string-place)
		(setf parents-string (subseq str parent-start parent-end))
		(loop while (< current-string-pos (length parents-string))
		   do (incf current-string-pos)
		     (setf parent-name-start current-string-pos
			   parent-name-end current-string-pos
			   paren-count 1)
		     (elk-paren-loop parents-string current-string-pos
		       (incf current-string-pos))
		     (loop while (or (alpha-char-p (aref parents-string parent-name-end))
				     (char= (aref parents-string parent-name-end) #\-)
				     (digit-char-p (aref parents-string parent-name-end)))
			do (incf parent-name-end))
		     (print (list parent-name-start parent-name-end))
		     (setf parent-name (subseq parents-string parent-name-start parent-name-end)
			   paren-count 1)
		     (print parent-name)
		     (setf parent-slots-end parent-name-end)
		     (print (subseq parents-string parent-slots-end))
		     (if (not (eq (aref parents-string parent-slots-end) #\)))
			 (elk-paren-loop parents-string parent-slots-end
			   (incf parent-slots-end)))
		     (if struct-parents
			 (setf struct-parents (append struct-parents (list (list parent-name (subseq parents-string parent-name-end parent-slots-end)))))
			 (setf struct-parents (list (list parent-name (subseq parents-string parent-name-end parent-slots-end)))))
		     (if (find #\( parents-string :start current-string-pos)
			 (setf current-string-pos (position #\( parents-string :start current-string-pos))
			 (setf current-string-pos (length parents-string)))
		     (setf paren-count 1)));;;@FIXME: only allows one parent and doesn't do anything with slots but make them into a string
	      (elk-error 'struct-no-parents "error encountered while processing structures parents. Make sure that the parent and it's new slot-values are enclosed in parenthesis.")))
	(progn (incf string-place)
	       (let ((struct-name-start string-place))
		 (loop while (or (alpha-char-p (aref str string-place))
				 (digit-char-p (aref str string-place))
				 (char= (aref str string-place) #\-)
				 (char= (aref str string-place) #\_))
		    do (incf string-place)
		      (if (> string-place (1- (length str)))
			  (return (elk-error 'struct-unparsable "Struct is not able to be parsed. Is there any spaces in there?"))))
		 (setf struct-name-position (list struct-name-start string-place)))));;get name if no parents
    (if (find-if #'alpha-char-p str :start string-place)
	(progn (if (not error-emitted)
		   (setf string-place (position #\( str :start string-place)))
	       (if (not error-emitted)
		   (let ((paren-count 1))
		     (setf slots-start string-place)
		     (elk-paren-loop str string-place
		       (incf string-place)
		       (if (> string-place (length str))
			   (return (elk-error 'unbalanced-parenthesis (count #\( str) (count #\) str))))
		       )))
	       (if (not error-emitted)
		   (let ((slots-end (1- string-place))
			 current-slot-start
			 current-slot-name
			 current-slot-value-start
			 current-slot-value)
		     (print (subseq str slots-start slots-end))
		     (loop for string-position from slots-start to slots-end
			do (loop for str-pos from string-position to slots-end
			      do (if (or (test-l-paren str str-pos)
					 (alpha-char-p (aref str str-pos)))
				     (return (setf current-slot-start str-pos
						   string-position str-pos))))
			  (print string-position)
			  (if (test-l-paren str string-position)
			      (let ((paren-count 1))
				(incf string-position)
				(setf current-slot-name (subseq str string-position (position #\space str :start string-position)))
				(setf string-position (position #\space str :start string-position))
				(incf string-position)
				(setf current-slot-value-start string-position)
				(elk-paren-loop str string-position (incf string-position))
				(setf current-slot-value (subseq str current-slot-value-start (1- string-position))))
			      (let ((slot-name-start string-position))
				(loop while (or (alpha-char-p (aref str string-position))
						(digit-char-p (aref str string-position))
						(char= (aref str string-position) #\-)
						(char= (aref str string-position) #\_))
				   do (incf string-position))
				(setf current-slot-name (subseq str slot-name-start string-position))))
			  (if (> (length current-slot-name) 0)
			      (push (make-elk-slot :slot current-slot-name :default-value current-slot-value) elk-struct-slots))
			  )))))
    (if (not error-emitted)
	(let ((elk-structure (make-elk-block :name (subseq str (car struct-name-position) (cadr struct-name-position))
					     :type 'struct
					     :slots elk-struct-slots ;;(subseq str slots-start (1- string-place))
					     :dependencies struct-parents)))
	  (push-elk-block elk-structure)))
    ))
#|;;;the code below is for an older version of stuff, my first attempt
  ;;;the :include directive will be done in version 0.1
;;delimit from space, if aref str 0 = ( then find what the 'parent' (:include) is
;;then get the position of ) and use that as the string-place, then loop through the slots
;;otherwise delimit from space to #\newline
(loop while (> paren-count 0)
   do (test-braces str string-place)
     (if (find #\newline sub-string)
	 (if (< (position-if #'alpha-char-p sub-string) (position #\newline sub-string))
	     (progn (if (< (position #\space sub-string) (position-if #'alpha-char-p sub-string))
			(progn (incf string-place (position-if #'alpha-char-p sub-string))
			       (setf sub-string (delimit-inclusive (aref sub-string (position-if #'alpha-char-p sub-string)) sub-string))))
		    (if (> (position #\) sub-string) (position #\space sub-string)) ;;slot
			(princ (delimit-to-inclusive (aref sub-string (position-if #'alpha-char-p sub-string)) " " sub-string))
			(progn (decf paren-count 1)
			       (princ (delimit-to-inclusive (aref sub-string (position-if #'alpha-char-p sub-string)) ")" sub-string))))
		    (incf string-place (position #\newline sub-string))
		    (setf sub-string (delimit-exclusive #\newline sub-string))
		    (if (find #\( sub-string)
			(if (< (position #\( sub-string) (position-if #'alpha-char-p sub-string))
			    (incf string-place (1- (position #\( sub-string)))
			    (incf string-place (1- (position-if #'alpha-char-p sub-string))))))
	     (if (< (position #\newline sub-string) (position-if #'alpha-char-p sub-string))
		 (decf paren-count 1)
		 (incf string-place 1)))
	     ;;;;this is for the last slot if it's the end of the file
	 (let ((alpha-char-pos (position-if #'alpha-char-p sub-string)))
	   (if (not alpha-char-pos)
	       (return (elk-error t "Unable to find character in structure definition.")))
	   (if (< (position #\space sub-string) alpha-char-pos)
	       (progn (incf string-place alpha-char-pos)
		      (setf sub-string (delimit-inclusive (aref sub-string alpha-char-pos) sub-string))))
	   (if (find #\space sub-string)
	       (princ (delimit-to-inclusive (aref sub-string alpha-char-pos) " " sub-string))
	       (princ (delimit-to-inclusive (aref sub-string alpha-char-pos) ")" sub-string)))
	   (incf string-place (position #\) sub-string))
	   (delimit-exclusive (aref sub-string (position #\) sub-string)) sub-string)
	   (if (> (length sub-string) 0)
	       (decf paren-count 1)))
	 ))
string-place))
|#
