(defun esb-process-defvar (str)
  (format t "Processing Variable ~%")
  (let ((var-var (make-elk-block :type 'variable))
	(string-place 0)
	(val-start nil))
    (test-braces str string-place)
    (setf val-start (+ (1+ (position #\space str))
		       (1+ (position #\space (delimit-exclusive " " str)))))
    (setf (elk-block-name var-var) (delimit-to-inclusive 0 " " (delimit-exclusive " " str)))
    (setf string-place val-start)
    (loop while (> paren-count 0)
       do (test-braces str string-place)
	 (incf string-place 1))
    (setf (elk-block-value var-var) (subseq str val-start (1- string-place)))
    (push-elk-block var-var)
    string-place))


(defun esb-process-define (str)
  (format t "Processing Define ~%")
  
  )

(defun esb-process-hash-symbol (str)
  #| Note on Hash symbols:
Needs a lookup table of sorts, i.e., get the string for the hash definition then use a table and stuff
|#
  (format t "Processing Hash Symbol ~%")
  (let ((var-type nil)
	(var-var (make-elk-block :type 'variable))
	(value-start (1+ (position #\space str)))
	(var-name nil))
    (setf var-type (elk-case 'char= (elk-peek str 0)
			     '((#\i integer)
			       (#\s string)
			       (#\: character))))
    (setf var-name (concatenate 'string "" (subseq str (position #\- str) (1- (position #\space str)))))
    (setf (elk-block-var-type var-var) var-type)
    (setf (elk-block-name var-var) var-name)
    (setf (elk-block-value var-var) (subseq str value-start))
    (push-elk-block var-var)))

