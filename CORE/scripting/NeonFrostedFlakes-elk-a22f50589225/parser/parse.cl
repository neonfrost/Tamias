"Need an ignore function for the paren-counting and such. i.e. Goes through the file and gets the positions for stuff to ignore, like hash symbols (#\(, #\")"
(defun process-elk-string-block (str)
  ;;If I understand this correctly, a block should be delimited by Hashes (#) or left parens [(]
  ;;Yes, first line, a block should be delimited by # or ( 
  ;;Essentially, blockify 'sorts' an elk file into various blocks
  ;;It's a much more clean way to parse elk source code, as compared to the insanity that I brought into being earlier
  ;;Currently, as of March 30, 2019, I need to rewrite, to an extent, the various process-[thing] functions
  ;;As they currently stand, they will not work with the blockified version of elk source code
  (print str)
  (let ((sub-string (delimit-to-inclusive 0 " " str)))
    (if (char= (aref str 0) #\()
	(PROGN (incf paren-count 1)
	       (cond ((string-equal sub-string "(defun")
		      (esb-process-defun str))
		     ((string-equal sub-string "(defmacro")
		      (esb-process-defmacro str))
		     ((string-equal sub-string "(defstruct")
		      (esb-process-defstruct str))
		     ((string-equal sub-string "(defvar")
		      (esb-process-defvar str))
		     ((string-equal sub-string "(define")
		      (esb-process-define str))
		     ((string-equal sub-string "(")
		      (elk-error 'spaced-function str))
		     (t
		      (esb-process-function-call str))
		     ))
	(esb-process-hash-symbol str))))

(defun process-elk-file (str)
  (if (not error-emitted)
      (blockify str))
  (setf elk-string-blocks (reverse elk-string-blocks))
  (loop while (not error-emitted)
     for elk-string-block in elk-string-blocks
     do (process-elk-string-block elk-string-block))
  (setf elk-string-blocks nil)
  ;;actually will only scrub some comments, only #; for version 0.01, 0.1 will implement full comment support (except #@)
  )

(defun count-parens (str)
  (let ((lp-count 0)
	(rp-count 0))
    (loop for string-pos below (length str)
       do (if (> string-pos 0)
	      (if (not (test-backslash str (1- string-pos)))
		  (if (char= (aref str string-pos) #\")
		      (if (> quote-count 0)
			  (decf quote-count)
			  (incf quote-count))))
	      (if (char= (aref str string-pos) #\")
		  (incf quote-count)))
	 (if (> string-pos 0)
	     (if (< quote-count 1)
		 (if (and (char= (aref str string-pos) #\()
			  (not (test-backslash str (1- string-pos)))
			  (not (char= (aref str (1- string-pos))
				      #\#)))
		     (incf lp-count 1)
		     (if (and (char= (aref str string-pos) #\))
			      (not (test-backslash str (1- string-pos))))
			 (incf rp-count 1))))
	     (if (char= (aref str string-pos) #\()
		 (incf lp-count 1)
		 (if (char= (aref str string-pos) #\))
		     (incf rp-count 1)))))
    (values lp-count rp-count)))

(defun read-elk-file (elk-file)
  (format t "Processing file ~A~%" elk-file)
  (with-open-file (ip elk-file)	
    (let ((elk-file-string (make-string (file-length ip)))
	  (lp-count 0)
	  (rp-count 0))
      (read-sequence elk-file-string ip)
      (format t "Scrubbing comments ~%")
      (setf elk-file-string (scrub-comments elk-file-string))
      (setf (values lp-count rp-count) (count-parens elk-file-string))
      (if (eq lp-count rp-count)
	  (process-elk-file elk-file-string)
	  (elk-error 'unbalanced-parenthesis lp-count rp-count))
      (if error-emitted
	  (format t "Compilation failed! Error! :: ~A~%" error-emitted))
      )))

(defun read-elk-files ()
  (let ((files (rest sb-ext:*posix-argv*)))
    ;;check for program arguments that are not files
    ;;if a file ends in some extension {.elks} then replace teh .elks with each filename in the file, so that {init.elks string.elk} becomes {init.elk test.elk frame.elk string.elk}
    (if files
	(loop for file in files
	   do (if (not error-emitted)
		  (read-elk-file file)
		  (return (format t "~A~%" error-emitted))))
	(setf error-emitted "There's no files friendo! I can't read spunk!")))
  (if error-emitted
      (format t "Compilation failed! Error! :: ~A~%" error-emitted)))	

#|
Note: need to create an std-lib structure/list so that CALL make-list doesn't resolve as a dependency
|#
