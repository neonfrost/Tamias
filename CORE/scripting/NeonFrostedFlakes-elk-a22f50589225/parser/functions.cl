(defun esb-process-defun (str)
  ;;remember (locals ...)
  ;;a possibility of local structures is declaring them as global, but using the original var idea
  ;;so, (defun blah-blah (...) (locals (var-struct string)))
  ;;would be compiled to (asm) var_name_func_name DATA
  ;;and references to it would be var_name_local_func_name.FIELD
  (format t "Processing function ~%")
  (let ((string-place 1)
	(func-var (make-elk-block :type 'function))
	(body-start 0)
	(old-paren 1))
    (loop while (alpha-char-p (aref str string-place))
       do (incf string-place))
    (incf string-place)
    (let ((name-start string-place))
      (loop while (or (alpha-char-p (aref str string-place))
		      (digit-char-p (aref str string-place))
		      (char= (aref str string-place) #\-)
		      (char= (aref str string-place) #\_))
	 do (incf string-place))
      (setf (elk-block-name func-var) (subseq str name-start string-place)))
    (setf string-place (1+ (position #\( str :start string-place)))
    (let ((arg-begin string-place)
	  (paren-count 1))
      (elk-paren-loop str string-place (incf string-place))
      (setf (elk-block-args func-var) (subseq str arg-begin (1- string-place))))
    #|    (loop while (> paren-count 0)
    do (if (and (not (test-backslash str (1- string-place)))
    (char= (aref str string-place) #\"))
    (if (< quote-count 1)
    (incf quote-count 1)
    (decf quote-count 1))) 
    (if (< quote-count 1)
    (test-braces str string-place)) 
    (incf string-place 1)
    (if (and (eq old-paren 2)
    (eq paren-count 1))
    (return t)
    (setf old-paren paren-count)))
    (setf body-start string-place
    paren-count 1
    quote-count 0)|#
    (let ((paren-count 1))
      (incf string-place)
      (setf body-start string-place)
      (elk-paren-loop str string-place
	(incf string-place)
	(if (and (eq string-place (length str))
		 (not (eq paren-count 0)))
	    (return (elk-error 'unbalanced-parenthesis (count #\( str) (count #\) str))))))
    #|    (loop while (> paren-count 0)
    do (if (and (not (test-backslash str (1- string-place)))
    (char= (aref str string-place) #\"))
    (if (< quote-count 1)
    (incf quote-count 1)
    (decf quote-count 1)))
    (if (< quote-count 1)
    (test-braces str string-place))
    (incf string-place 1)
    (if (and (eq string-place (length str))
    (not (eq paren-count 0)))
    (return (elk-error 'unbalanced-parenthesis (count #\( str) (count #\) str)))))|#
    (if (not error-emitted)
        (progn (setf (elk-block-value func-var) (subseq str body-start (1- string-place)))
	       (push-elk-block func-var)))))




;;all process-[DEFINITION] functions return an integer
;;it subseqs the source file code from that point

