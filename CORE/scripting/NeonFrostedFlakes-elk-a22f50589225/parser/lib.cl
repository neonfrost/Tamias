;;(defpackage elk-parser)
#|
Yet another note:
make new type definition easy
i.e. size in bytes, name of the type (symbol), parent, compatible-with
|#


#|

Note on local structs
On compilation:
allocate enough memory on stack for structure
::sub esp, [size of struct]
take current stack pointer and set local var to the address of it
then any calls to the slots will be
::%original_esp + offset

keep a variable called _elk_stack_size that increases every time a local variable is created
on exit of a function, it reduces by the same amount it was increased by

so a reference to a struct on the stack would be esp + ''current_stack_size'' + slot_offsets

so what about structs that are dynamically created at runtime?
HOLY SHIT ME
"Dynamically created structures" require them to be 'named' i.e. TRUCK_A
you can't just create local structures without a goddamn reference you cum-brained idiot

A problem comes into play of freeing the structure from the stack
A possible way is to set all of the 'slots' to 0

functional stacks:

A list
'(1 2 3)
corresponds to
'(0xABC 0xCDE 0x1589)
with 0x111
as the point where the list starts
and 0x111 0x112 0x113 as the addresses of of the list's contents, so that 0x111 would be 0xABC
Then (remove (nth 2 list) list)
would remove 0x112 from the list
and reseize list
either resulting in
0x111->0x113
and then the implicit list terminator
which would correspond to
0x114

|#

#|

NEVER MIND ABOUT THE BELOW, TEST FAILED

WARNING:

ELK CASE NEEDS TO BE RETESTED

SHOULD BE AS EASY AS
(elk-case 'char= (aref str 0)
	  (list #\( (setf str (concatenate 'string str "fuck me daddy")))
	  (list #\# (setf str (concatenate 'string str "cum on my face daddy")))
	  (list #\: (setf str (concatenate 'string str "cummies for me!")))
	  (list #\) (setf str (concatenate 'string str "Yummy cummy!")))
	  )
Personally, this still feels too convoluted, but the only way to do this correctly

|#


#|

Some problems to be dealt with:
recursion: A depedency that requires itself (but all other dependencies are fulfilled) is not technically a dependency. If all other deps check out, then 'compile'

|#

(defvar stl '("SET" "DEFINE"
	      "DEFSTRUCT" "DEFUN" "DEFVAR" "DEFMACRO" ;;I'm not sure if I want to have defmacro in the...CL...compiler...Nevermind. Forgot my goal of "Backwards compatible compilation" or, in other words, Elk compiler version 5.0 can be compiled with the CL compiler. However, an Elk 5.0 program that is not the compiler does not need to be compilable by the CL compiler (AKA teh bootstraped compiler)
	      "ADD" "SUBTRACT" "MULTIPLY" "DIVIDE"
	      "+" "-" "/" "*"
	      "AREF" "ARRAY" "ARR-INDEX" "MAKE-ARRAY"
	      "MAKE-HASH-TABLE" "FIND-KEY" "GET-HASH" "KEYS" ;;keys returns a list of all the keys used with the hash table
	      "FIND-PATTERN" "PEEK" "FIND" "POSITION" "COUNT"
	      "MAKE-LIST" "NTH" "CAR" "CADR" "CADDR" "CDR"
	      "WRITE-FILE" "READ-FILE"
	      "LOOP"))
(defvar error-emitted nil)
(defvar quote-count 0)

(defstruct elk-block
  name
  args
  value ;;value is either the value of a variable or the body of a function
  slots
  type ;;i.e. variable, function, macro, structure, etc.
  var-type ;;i.e. char, {struct}, int, float, string, etc.
  return
  dependencies)

(defstruct elk-slot
  slot
  default-value)

;;The following structs are effectively deprecated
;;unsure of how I feel about symbol table at this point
;;may be a good idea to use it for the conversion of Elk source code to the intermediate representation
#|
(defstruct elk-function
  name
  args
  body
  dependencies ;;what need to be defined first before the function can run
  return-value)
(defstruct elk-variable
  name
  value
  type
  dependec)
(defstruct elk-struct
  name
  slots
  parent
  dependencies)
(defstruct elk-macro
  name
  body
  dependencies)
(defstruct elk-symbol-table
  variables
  structures
  functions
  macros)
|#
(defvar elk-blocks nil)
(defmacro push-elk-block (elk-block)
  `(push ,elk-block elk-blocks))

(defvar elk-string-blocks nil)

#|
parses source code
delimits with brackets
|#	 


#|
(defmacro elk-case (case-function tester &rest bodies)
  (loop for body in bodies
     do (if (eval `(,case-function ,tester ,(car body)))
	    (loop for func in (rest body)
	       do (eval func)))))
|#

(defun elk-case (case-function tester bodies)
  (let ((case-looping t)
	(return-value nil))
    (loop for body in bodies
       while case-looping
       do (if (eval `(,case-function ,tester ,(car body)))
	      (progn (setf case-looping nil)
		     (loop for func in (rest body)
			do (if (symbolp func)
			       (setf return-value func)
			       (setf return-value (eval func)))))))
    return-value))

(defmacro test-backslash (str string-pos)
  `(char= (aref ,str ,string-pos) #\\))

(defmacro test-append (lst-1 lsts)
  `(if ,lst-1
       (setf ,lst-1 (append ,lst-1 ,lsts))
       (setf ,lst-1 ,lsts)))

(defun elk-error (error-type &rest messages)
  (case error-type
    (character (setf error-emitted (concatenate 'string "Character Error! Compilation choked at character: "
						(write-to-string (car messages))
						" at string position: " (write-to-string (cadr messages)))))
    (integer (setf error-emitted (concatenate 'string "Integer Error. ")))
    (unbalanced-parenthesis (setf error-emitted (concatenate 'string "Unbalanced parenthesis error! Left paren count: "
							     (write-to-string (car messages))
							     " Right paren count: " 
							     (write-to-string (cadr messages)))))
    (type-error (setf error-emitted (concatenate 'string "Error, type error of: " (car messages) " in: " (cadr messages) " of: " (caddr messages) '(#\newline))))
    (unresolved-expression (setf error-emitted (concatenate 'string "Error! Unresolved Expression: " (car messages))))
    (struct-no-parents (setf error-emitted (concatenate 'string "Error encountered while processing structure definition: "
							(car messages))))
    (spaced-function (setf error-emitted (concatenate 'string "Error! Use of a space between left-paren and symbol! " '(#\newline) (car messages))))
    (otherwise (setf error-emitted (concatenate 'string "Undefined error: " (car messages))))))

#|
(defvar elk-case-test #\1)
(defun test-elk-case (test-char)
  (elk-case 'char= test-char
	    '((#\! (princ "hi!"))
	      (#\1 (princ "1"))
	      (#\2 (print elk-case-test) (setf elk-case-test #\3))
	      (#\3 (setf elk-case-test #\!) (test-elk-case #\2)))))

(defun test-elk-case-2 (test-str)
  (elk-case 'string-equal test-str
	    '(("123" (print '123))
	      ("Hi" (print "Hello"))
	      ("Fuck daddy" (print "Fuck") (print (reverse '(1 2 3)))))))
|#	    
(defmacro elk-peek (str pos)
  `(aref ,str (1+ ,pos)))

(defmacro delimit-inclusive (limiter str &key (modifier 0))
  "Includes character"
  `(if (characterp ,limiter)
       (subseq ,str (+ (position ,limiter ,str) ,modifier) (length ,str))
       (let ((dl (aref ,limiter 0)))
	 (subseq ,str (+ (position dl ,str) ,modifier) (length ,str))
	 )))

(defmacro delimit-exclusive (limiter str &key (modifier 0))
  "excludes character"
  `(if (characterp ,limiter)
       (subseq ,str (+ (1+ (position ,limiter ,str)) ,modifier) (length ,str))
       (let ((dl (aref ,limiter 0)))
	 (subseq ,str (+ (1+ (position dl ,str)) ,modifier) (length ,str))
	 )))

(defmacro delimit-to-inclusive (lower-limit upper-limit str &key (modifier 0))
  "includes 1st chacrater"
  ;;add in integer support
  `(if (and (characterp ,lower-limit)
	    (characterp ,upper-limit))
       (subseq ,str (+ (position ,lower-limit ,str) ,modifier) (position ,upper-limit ,str))
       (let ((dl (if (characterp ,lower-limit)
		     ,lower-limit
		     (if (integerp ,lower-limit)
			 (aref ,str ,lower-limit)
			 (aref ,lower-limit 0))))
	     (ul (if (characterp ,upper-limit)
		     ,upper-limit
		     (if (integerp ,upper-limit)
			 (aref ,str ,upper-limit)
			 (aref ,upper-limit 0)))))
	 (subseq ,str (+ (position dl ,str) ,modifier) (position ul ,str))
	 )))

(defmacro delimit-to-exclusive (lower-limit upper-limit str &key (modifier 0))
  "excludes 1st character"
  ;;add in integer support
  `(if (and (characterp ,lower-limit)
	    (characterp ,upper-limit))
       (subseq ,str (+ (1+ (position ,lower-limit ,str)) ,modifier) (position ,upper-limit ,str))
       (let ((dl (if (characterp ,lower-limit)
		     ,lower-limit
		     (if (integerp ,lower-limit)
			 (aref ,str ,lower-limit)
			 (aref ,lower-limit 0))))
	     (ul (if (characterp ,upper-limit)
		     ,upper-limit
		     (if (integerp ,upper-limit)
			 (aref ,str ,upper-limit)
			 (aref ,upper-limit 0)))))
	 (subseq ,str (+ (1+ (position dl ,str)) ,modifier) (position ul ,str))
	 )))

(defvar paren-count 0)
(defvar left-braces '(#\( #\{))
(defvar right-braces '(#\) #\}))
(defvar array-braces '(#\[ #\]))

(defun finish-block ()
  (setf paren-count 0))

;;Whenever a left parenthesis is encountered, increase paren-count by 1
;;if paren count = 1, then look at the (0 -> space) word

(defmacro test-l-paren (str string-place)
  `(and (char= (aref ,str ,string-place) #\()
	(if (> ,string-place 0) 
	    (not (test-backslash ,str (1- ,string-place)))
	    t)))

(defmacro test-r-paren (str string-place)
  `(and (char= (aref ,str ,string-place) #\))
	(if (> ,string-place 0) 
	    (not (test-backslash ,str (1- ,string-place)))
	    t)))

(defmacro test-quote (str string-place)
  `(and (char= (aref ,str ,string-place) #\")
	(if (> ,string-place 0)
	    (not (test-backslash ,str (1- ,string-place)))
	    t)))

(defun test-braces (str string-place)
  (if (test-l-paren str string-place)
      (incf paren-count 1)
      (if (test-r-paren str string-place)
      	  (decf paren-count 1))))
#|  (if (find (aref str string-place) left-braces)
(incf paren-count 1)
(if (find (aref str string-place) right-braces)
    (decf paren-count 1))))
|#
(defmacro elk-paren-loop (str str-position &body body)
  `(loop while (> paren-count 0)
      do (if (test-quote ,str ,str-position)
	     (if (= quote-count 0)	
		 (incf quote-count)
		 (decf quote-count)))
	(if (= quote-count 0)
	    (test-braces ,str ,str-position))
	,@body))

(defun find-pattern (str pattern &key (context "DEFAULT ARGUMENT"))
  "Returns the position of the beginning and end of the pattern"
  (if (stringp pattern)
      (let ((pattern-length (length pattern))
	    (pattern-position 0)
	    (current-pattern-character 0)
	    (beginning 0)
	    (end 1)
	    (pattern-found? nil)
	    (pattern-failed? nil))
	(loop while (and (< pattern-position (length str))
			 (not pattern-found?))
	   do (if (char= (aref str
			       pattern-position)
			 (aref pattern
			       current-pattern-character))
		  (progn (setf beginning pattern-position
			       pattern-failed nil)
			 (incf current-pattern-character)
			 (loop while (and (< current-pattern-character
					     pattern-length)
					  (not pattern-failed?))
			    do (if (char= (aref str
						pattern-position)
					  (aref pattern
						current-pattern-character))
				   (progn (incf pattern-position)
					  (incf current-pattern-character))
				   (setf pattern-failed? t
					 current-pattern-character 0))
			      (if (>= current-pattern-character
				      pattern-length)
				  (setf pattern-found? t
					end pattern-position))))
		  (incf pattern-position)))
	(if pattern-found?
	    (list beginning end)
	    nil))
      (elk-error 'type-error "NOT A STRING" "Finding Pattern" context)))


(defun scrub-comments (str)
  "Only supports #; currently"
  (let ((comments? t)
	(current-position 0)
	(scrubbed-str str))
    (if (find #\# scrubbed-str)
	(setf current-position (position #\# scrubbed-str))
	(setf comments? nil))
    (loop while comments?
       do (if (char= (aref scrubbed-str (1+ current-position)) #\;)
	      (if (find #\Newline scrubbed-str :start current-position)
		  (setf scrubbed-str (concatenate 'string (subseq scrubbed-str 0 current-position)
						  (subseq scrubbed-str (position #\Newline scrubbed-str :start current-position))))
		  (setf scrubbed-str (subseq scrubbed-str 0 current-position))))
	 (if (< current-position (1- (length scrubbed-str)))
	     (if (find #\# scrubbed-str :start (1+ current-position))
		 (if (eq (position #\# scrubbed-str :start (1+ current-position))
			 current-position)
		     (setf comments? nil)
		     (setf current-position (position #\# scrubbed-str :start (1+ current-position))))
		 (setf comments? nil))
	     (setf comments? nil)))
    scrubbed-str))

#|
(scrub-comments "Lmno #;This is a comment!
  fuck")
|#

