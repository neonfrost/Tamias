(defun blockify (str)
  (let ((str-len (length str))
	(string-pos 0)
	(block-pos 0)
	(current-context nil)
	(deferred-decrease nil))
    (if (or (eq (aref str block-pos) #\space)
	    (eq (aref str block-pos) #\newline))
	(loop while (and (< block-pos (length str))
			 (or (eq (aref str block-pos) #\space)
			     (eq (aref str block-pos) #\newline)))
	   do (incf block-pos 1))) ;;This loop sets up block-pos to be the actual start of the code
    ;;While most of my source code files don't do this, there are a couple that do
    ;;Plus, scrubbing comments leaves a new line, so this
    ;;#; Blah blah blah
    ;;(define-variable cum-bucket (call "video-downloader site.web/video/cum-bucket"))
    ;;Would become
    ;;
    ;;(define-variable cum-bucket (call "video-downloader site.web/video/cum-bucket"))
    ;;after scrubbing comments, meaning that the start of the actual code is delayed
    (setf string-pos block-pos)
    (if (not (or (eq (aref str block-pos) #\() ;;note need to update this to allow for variable calls
		 (eq (aref str block-pos) #\#)))
	(setf error-emitted (concatenate 'string "Character outside of definition or call! quiting at character: " 
					 (write-to-string (aref str block-pos))
					 " position: " (write-to-string block-pos)))
	(loop for block-pos below str-len
	   do (if (or (eq (aref str block-pos) #\space)
		      (eq (aref str block-pos) #\newline))
		  (loop while (and (< block-pos (length str))
				   (or (eq (aref str block-pos) #\space)
				       (eq (aref str block-pos) #\newline)))
		     do (incf block-pos 1)))
	     (if (>= block-pos (length str))
		 (return t))
	     (setf string-pos block-pos)
	     (setf current-context (elk-case 'char= (aref str block-pos) ;; another possibility with the quotes 
					     ;;is checking if quote-count is greater than 0 and not 
					     ;;increasing or decreasing paren-count during that time
					     '((#\( 'Paren)
					       ;;process function, increase paren-count by 1, loop until paren-count = 0
					       ;;on paren-count = 0, push a string block
					       ;;FUUUUUUCK, just thought of the fact that quote-count will be necessary 
					       ;;with every definition. for instance (defvar xyz "(") would end up bleeding
					       ;;over into another definition/call...no, worse, it'd hit EOF before it'd
					       ;;be a major problem
					       (#\# 'hash))))
	   ;;for hash, find quote, inc quote-count by 1, same with paren count, 
	   ;;but ignores parenthesis......not sure what this note meant
	     (case current-context  ;;;;needs to be refactored
	       (hash (elk-case 'char= (elk-peek str (position #\space str :start block-pos))
			       '((#\( (setf paren-count 1))
				 (#\" (setf quote-count 1))))
		     (setf string-pos (+ (position #\space str :start block-pos) 2))
		     (if (char= (aref str (+ block-pos 1)) #\:);;i.e. #:Var_name " or #:Var_name (
			 (setf paren-count 0
			       quote-count 0))
		     (if (> paren-count 0)
			 (loop while (> paren-count 0)
			    do (if (not (test-backslash str (1- string-pos)))
				   (test-braces str string-pos))
			      (incf string-pos 1))
			 (if (> quote-count 0)
			     (progn (format t "Current string-position and character: ~A ~A ~%" string-pos (aref str string-pos))
				    (loop while (> quote-count 0)
				       do (if (not (test-backslash str (1- string-pos)))
					      (if (char= (aref str string-pos) #\")
						  (decf quote-count)))
					 (format t "\": ~A pos: ~A~%" quote-count string-pos)
					 (incf string-pos 1)))
			     (loop while (not (or (and (char= (aref str string-pos) #\()
						       (not (test-backslash str (1- string-pos))))
						  (and (char= (aref str string-pos) #\#)
						       (not (test-backslash str (1- string-pos))))))
				do (incf string-pos 1))))
		     (setf deferred-decrease t))
	       (paren (incf string-pos 1)
		      (incf paren-count 1)
		      (loop while (> paren-count 0)
			 do (if (char= (aref str string-pos) #\")
				(if (not (test-backslash str (1- string-pos)))
				    (if (> quote-count 0)
					(decf quote-count)
					(incf quote-count))))
			   (if (eq quote-count 0)
			       (test-braces str string-pos))
			   (incf string-pos 1)))
	       )
	     (setf quote-count 0 paren-count 0)
	     (push (subseq str block-pos string-pos) elk-string-blocks)
	     (if deferred-decrease
		 (progn (decf string-pos) (setf deferred-decrease nil)))
	     (setf block-pos string-pos)))))


