(ignore-errors  (import '(tamias.scripts:entity.define-script)))
#|


!!!!!!!!!!!!!!!!!!ONLY FOR USE WITH SCRIPTING FILES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


|#
(defstruct script-strings
  (tamias (make-hash-table))
  (lisp (make-hash-table))
  (accessors '((:tamias) (:lisp))))
(defvar script-strings (make-script-strings))
(defmacro define-script-string (accessor string-type string-replacer)
  `(let ((string-rep (if (stringp ',string-replacer)
			 ',string-replacer
			 (write-to-string ',string-replacer))))
     (define-symbol-macro ,accessor ,string-replacer)
     (aux:push-to-end ',accessor (assoc ,string-type (script-strings-accessors script-strings)))
     (case ,string-type
       (:tamias
	(setf (gethash ',accessor (script-strings-tamias script-strings)) string-rep))
       (:lisp
	(setf (gethash ',accessor (script-strings-lisp script-strings)) string-rep))
       )))

(defmacro get-script-string (accessor)
  `(gethash ,accessor (script-strings-tamias script-strings)))

(define-script-string entity.type :tamias (or (tamias.entities:entity-type entity)
					      :entity))
(define-script-string entity.x :tamias (tamias.entities:entity-x entity))
(define-script-string entity.y :tamias (tamias.entities:entity-y entity))
(define-script-string entity.z :tamias (tamias.entities:entity-z entity))
(define-script-string entity.width :tamias (tamias.entities:entity-width entity))
(define-script-string entity.height :tamias (tamias.entities:entity-height entity))

(define-script-string entity.health :tamias (tamias.entities:entity-health entity))
(define-script-string entity.hp :tamias (tamias.entities:entity-health entity))
(define-script-string entity.state :tamias (tamias.entities:entity-state entity))

(define-script-string entity.alive? :tamias (tamias.entities:entity-alive? entity))
(define-script-string entity.bounding-box :tamias (tamias.entities:entity-bounding-boxes entity))
(define-script-string entity.bounding-box[0] :tamias (elt entity.bounding-box 0))
(define-script-string entity.bounding-box[1] :tamias (elt entity.bounding-box 1))
(define-script-string entity.bounding-box[2] :tamias (elt entity.bounding-box 2))
(define-script-string entity.bounding-box[3] :tamias (elt entity.bounding-box 3))

(define-script-string target.type :tamias (or (tamias.entities:entity-type target)
					       (type-of target)))
(define-script-string target.x :tamias (tamias.entities:entity-x target))
(define-script-string target.y :tamias (tamias.entities:entity-y target))
(define-script-string target.z :tamias (tamias.entities:entity-z target))
(define-script-string target.width :tamias (tamias.entities:entity-width target))
(define-script-string target.height :tamias (tamias.entities:entity-height target))

(define-script-string target.health :tamias (tamias.entities:entity-health target))
(define-script-string target.hp :tamias (tamias.entities:entity-health target))
(define-script-string target.state :tamias (tamias.entities:entity-state target))

(define-script-string target.alive? :tamias (tamias.entities:entity-alive? target))
(define-script-string target.bounding-box :tamias (tamias.entities:entity-bounding-boxes target))
(define-script-string target.bounding-box[0] :tamias (elt target.bounding-box 0))
(define-script-string target.bounding-box[1] :tamias (elt target.bounding-box 1))
(define-script-string target.bounding-box[2] :tamias (elt target.bounding-box 2))
(define-script-string target.bounding-box[3] :tamias (elt target.bounding-box 3))

(define-script-string entity.define-attack :tamias bleharg)
(define-script-string entity.define-attacks :tamias attacks)

(define-script-string t-object.type :tamias (or (tamias.entities:t-object-type t-object)
						:t-object))
