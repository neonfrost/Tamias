5/25/23

- Ui windows have basic implementation at this point, scroll-bars need work

- Scripting subsystem has begun, needs work obviously, but hindered by incomplete entity framework

- Scripts have various extensions, with being able to extend those extensions (i.e. minotaur.entity creates a minotaur structure inheriting from entity, and minotaur-guard.minotaur creates a minotaur-guard structure that inherits from minotaur)

- Entity framework expanded, scenes are in the baby stages of development (using code from the older game [september 2016] and modifying it to work with the backend of Tamias)

- Some generics names changed to be consistent with current 'style' of Tamias codebase. (ui.mouse.click rather than action.click...action.click is extremely vague ui.mouse.click is much more understandable)

- Considering messing around with the string rendering to make it clearer to understand (for myself), would need a refactor.

- Colors can use arrays/vectors rather than lists now

- Text Editor 'widget' development started (not going to be sophisticated, but enough to do some basic text editing, wanting to have syntax highlighting and an autocomplete with the barebones tamias scripting functions/vars/symbols)

5/16/23

- Frame rendering is mostly fixed

- Container rendering and clicking fixed/implemented (container.click is called with each container sub-"class")

- Adding ui elements to frames is still a little finicky iirc.

- Ui windows have begun 'full' implementation (gui subsystem 1.0 goals, windows need 'title bar' (title ... arrow and X)

- More of a note to self, but ui palettes...themes...themes you fucking idiot... will begin development soon (so that the user can ignore setting the color for each and every element)

- Development started on "Text Fields" which are similar to labels. Labels are supposed to be for small bits of text, like "Paint", "Title", "Layer 1" and so on, not to render the contents of a file. Text fields may end up being both a ui-window and a text-field on creation.

- A note about the UI at this point in time (5/16/23): I'm not worried about the look of the UI right now. I'm worried about getting everything working properly. Once everything is working properly, I'll add in more "pretty looking" UI things (like how GTK themes tend to have rounded edges on scroll bars)

5/1/23

- Tamias Documentation subsystem implemented

- Honestly, not entirely sure what I've changed. Added an easier way to document things (at least, for me it's easier). Reimplemented the text buffer system. Will need some work done on higher level stuff. However, it should use less a LOT less CPU to display 'static' text, like a tamias.console message (which needs to have the text buffer implemented on it lol). Working on trying to get the paint 'demo' to work correctly. Currently, crashes with random error messages.

	-Emacs wants to tab these, whatever.

	-Paint 'demo' has issues, randomly crashes with various messages like SDL -1 or SIGABRT in the *inferior-lisp* buffer or something about a texture #FF000000. Sometimes it just crashes for no real reason with the paint demo.

	-Related to the paint demo, more than likely it's just how I'm doing it and not an underlying issue with Tamias or cl-sdl2. t-game/paint/lib.cl - paint.cl - io.cl are the 'offending' files

	-Main function now opens a window and allows slime REPL evaluation to happen without C-c C-c

4/15/23

- Basic text handling mostly finalized (need to implement moving the cursor up, home and end keys, etc.). Text input does not cause a crash anymore

- File Selector GUI started

- GUI subsystem extended and partially debugged

- More widgets have begun to be implemented, such as tables (called 'lister' in tamias)

- SDL2 backend improved

- Text chunker (splits strings based on newline or character-per-line) implemented (think rope data structure)

- Work has started toward re-implementing text buffers, for use with Text input widgets (which may end up using the text chunker to make multiple buffers)

- 2D particles file partially fixed

- loops.cl and render-engine.cl renamed to logic-loops.cl and render-loops.cl respectively

- tamias-init.asd is now deprecated (technically speaking, it was deprecated over a month ago)

1/6/23

- Text handling improved

- Prepended 't-' to tamias folders

- Tamias scene graph ideas started (trying to combine an ECS/ECS-like creator with a Scene Graph)

- Mentally preparing self for creating tamias studio

- A quick aside from the changelog: The code portion of Tamias will be the basis for developing games with Tamias Studio.  
TS 1.0 will coincide with the release of Tamias 1.0, a package deal so to speak.  
I will make several small games with Tamias Studio, MING being among them.  
See: https://bitbucket.org/NeonFrostedFlakes/workspace/snippets/Lz6EEB/tamias-studio for more information  

1/1/23

- Backend has been expanded on

- Bindings for SDL2 clarified

- Able to hypothetically use multiple backends now

31/12/22

- Started work on the actual engine, not just the backend

- Backend folder added to root. Graphics code, Audio Code, and other 'low level' stuff will be put in backend

- Engine folder will be revamped to include tamias specific code (requires tamias, but not SDL and so on)

- Backend could also be considered a kernel for silly purposes


5/20/2020 (2 a.m.)

- Demos need to be updated. I'm not even joking, I need to write 2 very small songs at less than 1 mb each for the audio demo (playing and switching audio)
- Lots of stuff has been changed so far
- For one, I realized that the methodology that I was originally going about with regards to the handling of 'logic' (non-rendered functions) and stuff was basically  
reimplementing the CLOS. I'm not joking. Look through the old code and ask yourself "How similar is this, from a design perspective, to the CL Object System?"
- So, the backend has changed and I have done quite a bit of refactoring.
- Keys have been updated to be more simple to interact with (you don't need to branch for ctrl or shift in the body, it's in a list for the method (... :ctrl t))
- Logic has been shifted to be more easy to interact with, same with rendering.
- The state subsystem has been shifted and changed as a result of these changes,
- get-state will get the current state structure,
- get-state-symbol will get the current state symbol

5/15/2020 

- Started work on fully implementing UI into tamias

5/12/2020 

- Changed how input is handeled. Much more simplified and easier to read (though may be more resource intensive, will need to figure out how to test between the two versions...How about re-using Kitchen Craze's code?)

5/10/2020

- Work resumed

4/9/2020 

- Introduced idea of tamias-types, specifically for use with UI elements.

4/8/2020 

- Changed render-text's dest-width/dest-height to width/height to better facilitate accurate representation of ideas

- Getting the animation system up and running (albeit rigidly) killed my willingness to code until the end of January, where I tried to make a Timeline (authoring program for timelines in stories, ala LOTR) program in Python. After much headaches of trying to get that to work, I gave up on it.

10/29/2019 

- Skeletal animation phase 1 complete (primitive, needs lots of work), Work halted on 3d engine

10/8/2019 

- Work started on Skeletal animation

9/23/2019 

- Work started on 3d engine segment

~7/25/2019 

- Introduced idea of palettes

5/?/2018 

- Completed and released Kitchen Craze

8/?/2017 

- Work started on MING - Acronym for MING Is Not Galaga

9/?/2016 

- Started work on RPG
