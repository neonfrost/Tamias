(defsystem :tamias-gui
  :author "Brandon Blundell | Neon Frost"
  :maintainer "Brandon Blundell | Neon Frost"
  :license "MIT"
  :version "0.75"
  :description "The gui subsystem for tamias."
  :default-component-class cl-source-file.cl
  :entry-point "cl-user::main"
  :depends-on (:tamias)
  :components ((:module "CORE"
		:serial t
		:components
		((:module "GUI"
		  :serial t
		  :components
		  ((:module "lib.base"
		    :serial t
		    :components
			    ((:file "base")
			     (:file "aux")))
		   (:module "lib.manager"
		    :serial t
		    :components
			    ((:file "base")
			     (:file "aux")))
		   (:file "gui-lib.container")
		   (:file "gui-documentation")
		   (:file "lib-generics")
		   (:file "lib-render")
		   (:module "window"
		    :serial t
		    :components
			    ((:file "lib")
			     (:file "render")
			     (:file "input")))
		   (:file "menu-bar-lib")
		   (:file "process-ui")
		   (:file "aux-lib")
		   (:file "frame-lib")
		   (:module "input"
		    :serial t
		    :components
			    ((:file "input-lib")
			     (:file "input-lib-rmb")
			     (:file "menu-bar")
			     (:file "entry")
			     (:file "spin-box")
			     (:file "basic-element")
			     (:file "container")
			     (:file "hover")
			     (:file "lister")))
		   (:module "render"
		    :serial t
		    :components
			    ((:file "hover")
			     (:file "click")
			     (:file "string")
			     (:file "scroll-bar")
			     (:file "container")
			     (:file "label")
			     (:file "button")
			     (:file "menu")
			     (:file "menu-bar")
			     (:file "entry")
			     (:file "spin-box")
			     (:file "frame")
			     (:file "lister")))
		   (:module "file-selector"
		    :serial t
		    :components
			    ((:file "lib")
			     (:file "render")
			     (:file "action")))
		   ))))))
