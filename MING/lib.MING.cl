(states:define-state level) ;;sub-states: idle paused options game-over
(define-track main-menu-music "MING/assets/Title.ogg")
(states:define-state main-menu nil top)
(setf (states:state-render-stack? main-menu) (list (states:state-sub-state main-menu)))
(defvar spawn-timer (timer:make))

(defvar wavelist nil)
(defvar bullets nil)
(defvar player-bullets nil)
(defvar bullet-particles nil)
(defvar enemies '()) ;list of enemies, used as a 'grouping' kind of thing
(defvar spawn? nil)

(defstruct enemy-group
  type
  list
  last?)

(t.e:define-entity bullet
  :type :bullet
  has-particles
  (particles-timer (timer:make :end 5 :reset? t))
  size power
  owner color)

#|
(defstruct (bullet (:include tamias.entities:entity
		    (type :bullet)))
  has-particles ;t or nil
  ;;particles
  (particles-timer (timer:make :end 5 :reset t)) ;;increment, when end?, reset current
  size ;;w h
  power ;= (entity-atk ,entity) + upgrade (if applicable)
  owner ; is :player, :A :B :C or :Boss
  color)
|#

(defmacro bullet-speed-x (bullet)
  `(elt (bullet-velocity ,bullet) 0))
(defmacro bullet-speed-y (bullet)
  `(elt (bullet-velocity ,bullet) 1))
(defmacro bullet-x (bullet)
  `(elt (bullet-position ,bullet) 0))
(defmacro bullet-y (bullet)
  `(elt (bullet-position ,bullet) 1))
#|
(t.e:define-entity ship
  :bounding-boxes (make-bounding-box 0 0 32 32)
  hp
  (atk 1)
  (def 1)
  size
  fire-sound ;make-sample :path "/path/to/sound"
  max-hp
  fire-timer
  hit
  )
|#
#|
(defstruct (ship (:include tamias.entities:entity
		  (bounding-boxes (make-bounding-box 0 0 32 32))))
  hp
  (atk 1)
  (def 1)
  size
  fire-sound ;make-sample :path "/path/to/sound"
  max-hp
  fire-timer
  hit)
|#



(t.e:define-entity ship
  :bounding-boxes (make-bounding-box 0 0 32 32)
  hp
  (atk 1)
  (def 1)
  size
  fire-sound ;make-sample :path "/path/to/sound"
  max-hp
  fire-timer
  hit)

;;(defstruct (enemy (:include ship)))

(t.e:define-entity enemy
    :include ship)

(t.e:define-entity type-a
  :include enemy
  :sprite-sheet (sprite:get-sheet :type-a)
  :type :A
  :atk 1
  :def 1
  :hp 1
  :width 32
  :height 32
  :fire-timer (timer:make :end 15 :reset? t))

#|
(defstruct (type-a (:include enemy
		    (sprite-sheet (sprite:get-sheet :type-a))
		    (type :A)
		    (atk 1)
		    (def 1)
		    (hp 1)
		    (width 32)
		    (height 32)
		    
;;		    (y -64)
;;		    (path '(:edge '(-1 0)))
;;		    (vector (vec-2d:make-vector-2d :x 8 :y 8))
		    ;;			     (sheet "Graphics/Type-A.png")
		    ;;			     (cell-size '(32 32))
		    (fire-timer (timer:make :end 15 :reset t))
		    )))
|#

(t.e:define-entity type-b
  :include enemy
  :sprite-sheet (sprite:get-sheet :type-b)
  :type :B
  :atk 1
  :def 1
  :hp 2
  :width 32
  :height 32
  :bounding-boxes (make-bounding-box 0 0 32 32)
  :fire-timer (timer:make :end 30 :reset? t))

#|
(defstruct (type-b (:include enemy
		    (sprite-sheet (sprite:get-sheet :type-b))
		    (type :B)
		    (atk 1)
		    (def 1)
		    (hp 2)
		    (width 32)
		    (height 32)
;;			     (y -64)
;;			     (path '(:edge '(-2 0)))
;;			     (vector (vec-2d:make-vector-2d :x 16 :y 2))
;;			     (sheet "Graphics/Type-B.png")
		    ;;			     (cell-size '(32 32))
		    (bounding-boxes (make-bounding-box 0 0 32 32))
		    (fire-timer (timer:make :end 30 :reset t))
		    )))
|#

(t.e:define-entity type-c
  :include enemy
  :sprite-sheet (sprite:get-sheet :type-c)
  :state :init
  :atk 2
  :def 1
  :hp 4
  :width 32
  :height 48
  :fire-timer (timer:make :end .5 :reset? t :in-seconds? t)
  (fired 0)
  (slam-state :nil))

#|
(defstruct (type-c (:include enemy
		    (sprite-sheet (sprite:get-sheet :type-c))
		    (type :c)
		    (state :init)
		    (atk 2)
		    (def 1)
		    (hp 4)
		    (width 32)
		    (height 48)
;;		    (y -64)
;;		    (path '(:fired-lasers nil))
;;			     (vector (vec-2d:make-vector-2d :x 0 :y 0))
;;			     (attack-pattern '(:player-x))
;;			     (sheet "Graphics/Type-C.png")
		    ;;		    (cell-size '(32 32))
		    (fire-timer (timer:make :end .5 :reset t :in-seconds? t))
		    ))
  (fired 0)
  (slam-state :nil))
|#

(t.e:define-entity boss
  :include enemy
  :sprite-sheet (sprite:get-sheet :type-d)
  :type :boss
  :width 64
  :height 128
  :fire-timer (timer:make :end 5 :reset? t)
  (missile 10))
#|
(defstruct (boss (:include ship
		  (sprite-sheet (sprite:get-sheet :type-d))
		  (type :boss)
		  (width 64)
		  (height 128)
;;			   (y -128)
;;			   (path '(:follow-player))
;;			   (vector (vec-2d:make-vector-2d :x 0 :y 0))
;;			   (attack-pattern '(:player-x (fire 'boss)))
;;		  (sheet "Graphics/Boss.png")
		  (fire-timer (timer:make :end 5 :reset t))
		  ;;			   (cell-size '(64 128))
		  )) ;on make-boss, it gets the player's current upgrade, weapon and atk (i.e. power of one player 'bullet') and mults it by 50, this is HP I guess
  (missile 10) ;boss missile has a radius of 1/8 screen-width
  )
|#

(t.e:define-entity player
  :include ship
  :sprite-sheet (sprite:get-sheet :player)
  :type :player
  :bounding-boxes (make-bounding-box 0 0 32 32)
  :fire-timer (timer:make :end .1 :reset? t :in-seconds? t)
  (move-state :idle)
  (invincible-timer (timer:make :end 1 :in-seconds? t))
  invincible
  (missile-timer (timer:make :end 2 :in-seconds? t))
  missile-fired?
  (groups 0)
  (kills 0)
  (waves 0)
  (weapon 1)
  (stabilizer 0) ;; up to 3, ensures that the laser will always fire straight, unless player is moving
  (shield 0) ;shield allows for more hits on the player
  (missile 0) ;missile is shot with c or something, has an explosion of radius 1/4 screen-width
  (upgrade 0)) ;upgrade increases the amount of damage the player can do (i.e. (setf (bullet-power playe
  #|
(defstruct (player (:include ship
		    (sprite-sheet (sprite:get-sheet :player))
		    (type :player)
		    (bounding-boxes (make-bounding-box 0 0 32 32))
		    (fire-timer (timer:make :end .1 :reset t :in-seconds? t))
		    ))
  (invincible-timer (timer:make :end 1 :in-seconds? t))
  invincible
  (missile-timer (timer:make :end 2 :in-seconds? t))
  missile-fired?
  (groups 0)
  (kills 0)
  (waves 0)
  (weapon 1)
  (stabilizer 0) ;; up to 3, ensures that the laser will always fire straight, unless player is moving
  (shield 0) ;shield allows for more hits on the player
  (missile 0) ;missile is shot with c or something, has an explosion of radius 1/4 screen-width
  (upgrade 0)) ;upgrade increases the amount of damage the player can do (i.e. (setf (bullet-power player-bullet) (+ (player-upgrade player) (player-atk player)))
|#
#|
(push-init (load-sheet)
(sprite:defsheet :player "MING/assets/player.png" '(32 32))
(push-quit (free-sheet :player))
|#

;;(defun play-sound (&optional a b c d))

(defvar player nil)

(defun start-firing ()
  (if (not (eq (player-state player) :dead))
      (setf (player-state player) :firing)))
(defun stop-firing ()
  (if (not (eq (player-state player) :dead))
      (setf (player-state player) :idle)))


(defmacro player-speed-x ()
  `(elt (player-velocity player) 0))
(defmacro player-speed-y ()
  `(elt (player-velocity player) 1))
(defmacro player-x ()
  `(elt (player-position player) 0))
(defmacro player-y ()
  `(elt (player-position player) 1))

(defmacro ship-speed-x (ship)
  `(elt (ship-velocity ,ship) 0))
(defmacro ship-speed-y (ship)
  `(elt (ship-velocity ,ship) 1))
(defmacro ship-x (ship)
  `(elt (ship-position ,ship) 0))
(defmacro ship-y (ship)
  `(elt (ship-position ,ship) 1))
