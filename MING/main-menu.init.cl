(defvar main-menu-music nil)
(defvar MING-title nil)

(defun load-ming-title ()
  (setf MING-title (tamias:load-image "MING/assets/title.png")))

(defun delete-ming-title ()
  (when MING-title
    (tamias:free-image MING-title))
  (setf MING-title nil))

(states:add-logic (main-menu init)
		  (load-ming-title))

(defun render-title ()
  (render:image MING-title
		(round (/ tamias:screen-width 4))
		8;;just so that it's not right on the edge of the screen
		(round (/ tamias:screen-width 2))
		(round (/ tamias:screen-height 8))
		))
(states:add-render (main-menu top)
		   (render-title))
