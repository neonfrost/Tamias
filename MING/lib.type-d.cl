(defmethod render-ship ((ship boss))
  (render:sprite (ship-sprite-sheet ship) (ship-x ship) (ship-y ship)))

(defmethod ship-fire ((ship boss))
  (when (not (eq (ship-state ship) :init))
    (when (fire-timer (ship-fire-timer ship))
      (play-sound :boss-fire)
      (let ((start-x (round (+ (ship-x ship)   (/ (ship-width ship) 2))))
	    (start-y (round (+ (ship-y ship)   (/ (ship-height ship) 4))))
	    (width 2)   (height 8)
	    (num-bullets 4))
	(if (> (boss-hp ship)
	       (* (boss-max-hp ship) .75))
	    (loop :for x :from start-x   :to (+ start-x 4) :by 3   ;;top guns
		  :do (spawn-boss-bullet ship x (ship-y ship)
					 width height
					 0 -1)))
	(if (> (boss-hp ship)
	       (* (boss-max-hp ship) .4))
	    (loop :for y :from start-y   ;;left guns
		:to (+ start-y (round (/ (ship-height ship) 2))) :by 12
	      :do (spawn-boss-bullet ship (ship-x ship) y
				     height width
				     -1 0)))
	(if (> (boss-hp ship)
	       (* (boss-max-hp ship) .4))
	    (loop :for y :from start-y   ;;right guns
		    :to (+ start-y (round (/ (ship-height ship) 2))) :by 12
		  :do (spawn-boss-bullet ship
					 (+ (ship-x ship) (ship-width ship))
					 y
					 height width
					 1 0)))
	(if (< (boss-hp ship)
	       (* (boss-max-hp ship) .25))
	    (incf num-bullets 4))
	(loop :for x :from start-x   :to (+ start-x num-bullets) :by 3  ;;bottom guns
	      :do (spawn-boss-bullet ship x (+ (ship-y ship) (ship-height ship))
				     width height
				     1 1))))))

(defmethod ship-move ((ship boss))
  (when (eq (ship-state ship) :init)
    (setf (ship-x ship) (round (- (/ tamias:screen-width 2) (/ (ship-width ship) 2)))
	  (ship-speed-y ship) (round (/ tamias:screen-height 100)))
    (when (>= (ship-y ship) 64)
      (setf (ship-state ship) :left
	    (ship-speed-y ship) 0
	    (ship-speed-x ship) (- (round (/ tamias:screen-width tamias:fps 2))))))
  (let ((screen/4 (round (/ tamias:screen-width 4))))
    (case (ship-state ship)
      (:left (if (<= (ship-x ship) screen/4)
		 (setf (ship-state ship) :right
		       (ship-speed-x ship) (- (ship-speed-x ship)))))
      (:right (if (>= (ship-x ship) (* 3 screen/4))
		  (setf (ship-state ship) :left
			(ship-speed-x ship) (- (ship-speed-x ship)))))))
  (bullet-collide? ship)
  (when (<= (ship-hp ship) 0)
    (kill-entity ship))
  (incf (ship-x ship) (ship-speed-x ship))   (incf (ship-y ship) (ship-speed-y ship)))
  ;;:d -Boss (after surviving 5 waves) - Slow, side to side. 

(defmethod kill-entity ((ship boss))
  (generate-explosions (ship-x ship) (ship-y ship) 64 32 (ship-width ship) (ship-height ship))
  (setf (ship-state ship) :dead)
  (incf (player-kills player) 1)
  (incf (player-waves player) 1)
;;;;Causes explosions at 'random' places on the boss
;;  (free-sheet (entity-sheet-surface boss))
;;  (setf boss nil)
  ;;;;Get rid of the entity after all the explosions are processed
;;  (set-state credits)
;;;;sets the state to credits (unless it's an 'endless' mode, then just increases the diff)
  )

(defmethod entity-fire-missile ((ship boss))
  )
