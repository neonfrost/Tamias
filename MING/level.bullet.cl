(defun bullet-dead? (blt)
  (or (> (bullet-y blt) (+ tamias:screen-height 96))
      (< (bullet-y blt) -96)
      (> (bullet-x blt) (+ tamias:screen-width 64))
      (< (bullet-x blt) -64)))

(defun bullet-collision (ship bullet)
  ;;we'll ignore bounding boxes for the initial version, just to make sure things are called
  ;;and things work correctly, before moving onto more complex tasks
  (or (within-area (ship-x ship) (ship-y ship)
		   (bullet-x bullet) (bullet-y bullet)
		   (ship-width ship) (ship-height ship))
      (within-area (bullet-x bullet) (bullet-y bullet)
		   (ship-x ship) (ship-y ship)
		   (bullet-width bullet) (bullet-height bullet))))

(defvar bullet-collider nil)

(defun bullet-collide? (ship)
  (let ((blts (if (player-p ship)
		  bullets
		  player-bullets)))
    (loop :for bullet :in blts
	  :do (when (bullet-collision ship bullet)
	      	(setf (bullet-state bullet) :d)
		(decf (ship-hp ship) (bullet-power bullet))
		(return :t)))))
;;test against player, tests against either player or enemy, if ship is of type player

(defun move-bullet (b)
  (incf (bullet-x b) (vec3d:x (bullet-velocity b)))
  (incf (bullet-y b) (vec3d:y (bullet-velocity b)))
  (if (bullet-dead? b)
      (setf (bullet-state b) :d)))

(defun spawn-bullet-particle (bullet) ;;needs tamias particles systme
  (create-particle ((vector (round (* (/ (bullet-speed-x bullet)
					 (1+ (abs (bullet-speed-x bullet))))
				      (random (1+ (abs (bullet-speed-x bullet))))))
			    (round (/ (bullet-speed-y bullet) (+ 2 (random 5)))))
		    ((bullet-x bullet) (bullet-y bullet)
		     (+ (random (bullet-width bullet)) 2)
		     (+ (random (- (bullet-height bullet) 2)) 2))
		    (round (* .5 tamias:fps))
		    (tamias.colors:offset (bullet-color bullet) -80))
		   bullet-particles))

(defmacro bullet-particle? (b)
  ;;increment, when end?, reset current
  `(symbol-macrolet ((bt (bullet-particles-timer ,b)))
     (timer:increment bt)
     (when (timer:end? bt)
       (dotimes (y 4)
	 (spawn-bullet-particle ,b)))))

(defun render-bullet (bullet)
  (render:box (bullet-x bullet) (bullet-y bullet)
	      (bullet-width bullet) (bullet-height bullet)
	      (bullet-color bullet)))
  

(defun process-bullets ()
  (loop :for bullet :in bullets
	:do (if (eq (bullet-state bullet) :d)
		(setf bullets (remove bullet bullets))
		(move-bullet bullet)
	    	;;(bullet-particle? bullet) just don't really like it :/
		))
  (loop :for bullet :in player-bullets
	:do (if (eq (bullet-state bullet) :d)
		(setf player-bullets (remove bullet player-bullets))
		(move-bullet bullet)
		;;(bullet-particle? bullet)
		))
  ;;(process-particles bullet-particles)
  )

(defun rand-neg ()
  (if (eq (random 2) 0)
      -1
      1))

(defun rand-angle ()
  (/ (random 11) 10))

(defun spawn-player-bullet ()
  (let ((x (round (+ (player-x) (/ (player-width player) 2))))
	(y (player-y))
	(particles (if (> (player-weapon player) 1)
			  t
			  nil))
	(width 2)
	(height 20))
    (play-sound :player-fire)
    (push (make-bullet :has-particles particles
		       :width width
		       :height height
		       :position (vector  (- x width)
					  (- y height))
		       :velocity (vector (round (+ (/ (elt (ship-velocity player) 0) 4)
						   (* 9 (rand-neg) (rand-angle))
						   (* 3 (player-stabilizer player))))
					 (- (round (/ tamias:screen-height 40))))
		       :color (list 0 191 255 255)
		       :power (* (+ (player-upgrade player) (player-atk player))
				 (player-weapon player))
		       :owner :player)
	  player-bullets)))

(defun spawn-enemy-bullet (ship)
  (let ((particles (if (or (eq (ship-type ship) :b)
			   (eq (ship-type ship) :c))
		       t
		       nil))
	(x (round (+ (ship-x ship) (/ (ship-width ship) 2))))
	(y (+ (ship-y ship) (ship-height ship) 2))
	(width 4)
	(height 16))
    (play-sound :enemy-fire)
    (if (type-c-p ship)
	(setf width 10
	      x (- x 12)))
    (push (make-bullet :has-particles particles
		       :width width
		       :height height
		       :position (vector  (+ x width)
					  (+ height y))
		       :velocity (vector (round (/ (elt (ship-velocity ship) 0) 4))
					 (round (/ tamias:screen-height 40)))
		       :color (list 255 100 100 255)
		       :power (ship-atk ship)
		       :owner :enemy)
	  bullets)))


(defun spawn-bullet (ship)
  (if (player-p ship)
      (spawn-player-bullet)
      (spawn-enemy-bullet ship)))


(defun spawn-boss-bullet (ship &optional (x 0) (y 0) (width 1) (height 1) (x-dir 1) (y-dir 1)) 
  (let ((velocity-vector (if (> width height)
			     (vector (round (- (* x-dir
						  (+ (elt (ship-velocity ship) 0)
						     (/ (elt (ship-velocity ship) 0) 4)))
					       (rand-angle)))
				     (round (* y-dir (/ tamias:screen-height 20))))
			     (vector (round (+ (* x-dir (rand-neg)) (rand-angle)))
				     (round (* y-dir (/ tamias:screen-height 80)))))))
    (push (make-bullet :width width
		       :height height
		       :position (vector  (+ x width)
					  (+ height y))
		       :velocity velocity-vector
		       :color (list 255 180 100 255)
		       :power (ship-atk ship)
		       :owner :enemy)
	  bullets)))
