(defvar explosions nil)

(defun generate-explosions (x y num &optional (lifetime (/ tamias:fps 5)) (width 2) (height 2) (color-type :enemy-death))
  (let ((dir 0) ;; 0 = right up, 1 = right down, 2 l-d, 3 u-l
	(color (case color-type
		 (:enemy-death '(200 180 0 255))
		 (:player-hit '(255 60 60 255))
		 (:player-death '(255 0 0 255)))))
    "this text is just to force recompilation by slime"
    (loop for explosion below num
	  do (let ((x-o (+ x 16 (random 4)))
		   (y-o (+ y 16 (random 4))))
	       (create-particle ((vec3d:make :x (if (or (eq dir 0) (eq dir 2))
						    4
						    -4)
					     :y (if (or (eq dir 0) (eq dir 1))
						    4
						    -4))
				 ((+ x-o (random width)) (+ y-o (random height)) 9 9)
				 (round lifetime)
				 color)
				explosions)
	       (if (eq dir 3)
		   (setf dir 0))
	       (incf dir)))))

(defun process-explosions ()
  (process-particles explosions))

#|
(defun spawn-boss-bullet (boss)
  :y-velocity
  (if (boss-p boss)
      24))
|#
(defun fire-timer (timer)
  (timer:increment timer)
  (timer:end? timer))


