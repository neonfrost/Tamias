;;(defvar entity-decelarator
(defvar entity-decelerator .1)
(defvar decelerate entity-decelerator)

(defgeneric gen-entity (entity-type)
  (:method (entity-type)
    (declare (ignore entity-type))
    nil))

(defmethod gen-entity ((entity-type (eql :a)))
  (let* ((x-o (random tamias:screen-width))
	 (x-pos (if (< x-o 110)
		    (player-x)
		    x-o)))
    (let ((max-time (- 4 (round (/ (player-waves player) 20)))))
      (if (< max-time 1)
	  (setf max-time 1))
      (setf spawn-timer (timer:make :end max-time :in-seconds? t)))
    (make-enemy-group
     :type :a
     :list (loop :for x :below 5
		 :collect (let ((ret (make-type-a :position (vector (- x-pos (* x 48))
								    (- -64 (* x 5)))
						  :velocity (vector
							     (round (* (/ tamias:screen-width 8)								       decelerate
								       (1+ (* .1 (round (player-waves player) 10)))
								       ))
							     (round (* (/ tamias:screen-width 16)
								       decelerate
								       (1+ (* .1 (round (player-waves player) 10)))))
							     0) ;;fast y, slow x. Let's use screen width for both.
						  :bounding-boxes (make-bounding-box 0 0 32 32)
						  )))
			    ret)))))
     

(defmethod gen-entity ((entity-type (eql :b)))
  (let* ((x-o (random tamias:screen-width))
	 (x-pos (if (< x-o 110)
		    (ship-x player)
		    x-o)))
    (let ((max-time (- 5 (round (/ (player-waves player) 20)))))
      (if (< max-time 1)
	  (setf max-time 1))
      (setf spawn-timer (timer:make :end max-time :in-seconds? t)))
    (make-enemy-group
     :type :b
     :list (loop :for x :below 3
		 :collect (let ((Ret (make-type-b :position (vector (- x-pos (* x 32))
								    (- -64 (* x 16)))
						  :fire-timer (timer:make :current (- 2 x)
									  :end 30
									  :reset t)
						  :velocity (vector
							     (round (* (/ tamias:screen-width 8)
								       decelerate
								       (1+ (* .1 (round (player-waves player) 10)))))
							     (round (* (/ tamias:screen-width 32)
								       decelerate
								       (1+ (* .1 (round (player-waves player) 10)))))
							     0) ;;fast x slow y
						  :bounding-boxes (make-bounding-box 0 0 32 32)
						  )))
			    ret)))))
     
  
(defmethod gen-entity ((entity-type (eql :c)))
  (let* ((x-o (random tamias:screen-width))
	 (x-pos (if (< x-o 110)
		    (ship-x player)
		    x-o)))
    (let ((max-time (- 10 (round (/ (player-waves player) 20)))))
      (if (< max-time 1)
	  (setf max-time 1))
      (setf spawn-timer (timer:make :end max-time :in-seconds? t)))
    (make-enemy-group
     :type :c
     :list (list (let ((ret (make-type-c :position (vector x-pos -64)
#|  			:path (list :fired-lasers (list (* (+ (player-x player) 240) -1)
			(+ (player-x player) 240)
			(* (+ (player-x player) 240) -1)
			(+ (player-x player) 240)
			
			(player-x player)))
|#
					 :bounding-boxes (make-bounding-box 0 0 32 32))))
		   ret)))))
  


(defmethod gen-entity ((entity-type (eql :boss)))
  (setf spawn-timer (timer:make :end 20 :incrementor 0))
  (make-enemy-group
   :type :boss
   :list (list (make-boss :position (vector (/ tamias:screen-width 2) -128)
			  :hp (* (+ (player-upgrade player)
				    (player-weapon player))
				 50)
			  :max-hp (* (+ (player-upgrade player)
					(player-weapon player))
				     50)))))
  

