#|
Handles changes in the xy coordinates of all entities
Handles deaths
Handles vector changes
Handles particle creation, lifetime, and destruction

Holds structures, macros, classes, etc.

Behavior of different ai
TYPE A: Vectors: +y at a rate of 0.5, +/- x at a rate of 0.25 until edge of the screen then change 'sign'
TYPE B: Inverse of Type A, faster +/- x, slower +y
TYPE C: Slams downward 5 times after firing 3 lasers, follows player; HP = 5 hits
BOSS: Vectors: +/- x, follows player at 1/4, 1/2, 3/4, and 1.0 players speed at 1.0, 3/4, 1/2, 1/4 health, no Y vector; HP=50 hits
|#

(defun gen-group (e &optional is-last?)
  (let ((e-group (gen-entity e)))
    (if is-last?
	(setf (enemy-group-last? e-group) t))
#|  
    (if (not wavelist)
	(push e-group wavelist)
	(aux:push-to-end e-group wavelist))
|#
    e-group))

"Defsheet needs to be added to the logic of the Level/init state/sub-state"

(defun gen-wavelist ()
  (case (1+ (mod (player-waves player) 6))
    (1 (setf wavelist '((gen-group :a) (gen-group :b) (gen-group :a)
			(gen-group :b) (gen-group :b)
			(gen-group :a :last))))
    (2 (setf wavelist '((gen-group :a) (gen-group :b) (gen-group :a)
			(gen-group :b) (gen-group :a)
			(gen-group :b) (gen-group :a)
			(gen-group :c :last))))
    (3 (setf wavelist '((gen-group :a) (gen-group :b) (gen-group :a) (gen-group :b)
			(gen-group :a) (gen-group :b) (gen-group :c) (gen-group :a)
			(gen-group :b) (gen-group :c) (gen-group :b) (gen-group :c)
			(gen-group :c :last))))
    (4 (setf wavelist '((gen-group :a) (gen-group :b) (gen-group :b) (gen-group :c)
			(gen-group :c) (gen-group :c) (gen-group :c) (gen-group :c)
			(gen-group :b) (gen-group :c) (gen-group :c) (gen-group :a)
			(gen-group :a) (gen-group :a) (gen-group :a) (gen-group :c)
			(gen-group :c :last))))
    (5 (setf wavelist '((gen-group :a) (gen-group :a) (gen-group :b) (gen-group :b)
			(gen-group :a) (gen-group :b) (gen-group :b) (gen-group :a)
			(gen-group :b) (gen-group :b) (gen-group :c) (gen-group :b)
			(gen-group :c) (gen-group :a) (gen-group :c) (gen-group :b)
			(gen-group :a) (gen-group :b) (gen-group :c) (gen-group :c)
			(gen-group :c :last))))
    (6 (setf wavelist '((gen-group :boss))))))

(defun spawn-wave ()
  (push (eval (pop wavelist)) enemies))

(defun spawn ()
  (if (not enemies)
      (if wavelist
	  (spawn-wave)
	  (gen-wavelist))
      (when (timer:end? spawn-timer)
	(if (not wavelist)
	    (gen-wavelist))
	(spawn-wave))))


(defun spawn-player ()
  (setf player (make-player :width 32 :height 32
			    :position (vector (- (/ tamias:screen-width 2) 16)
					      (- tamias:screen-height 96) 1)
			    :hp 5 :atk 1 :def 1)))

