(defpackage splash-screen
  (:use :cl)
  (:export splash-screen
	   splash-ticks
	   splash-seconds))
(setf tamias:console.show nil)
(in-package :splash-screen)
(states:define-state splash-screen)
(defvar splash-ticks 0)
(defvar splash-seconds 1)
(in-package :cl-user)

(states:set-state splash-screen:splash-screen init)
(states:def-logic
    (splash-screen:splash-screen init)
    (tamias:set-target-resolution 640 480)
    (tamias:set-window-title "MING")
    (states:set-sub-state idle splash-screen:splash-screen))


(state.init-ui splash-screen idle)

(set-bg-color tamias.colors:+black+)
(tamias:set-resolution 0)

(ui.add-label splash-screen idle (round (/ tamias:screen-width 4)) (round (/ tamias:screen-height 4))
	      "Made with"
	      :width 70 :height 16)

(ui.add-label splash-screen idle (round (/ tamias:screen-width 4))
	      (+ (round (/ tamias:screen-height 4)) char-height 2)
	      "Tamias"
	      :width 400 :height 100
	      :scale-text t)

(ui.add-label splash-screen idle
	      (- 600 (* (length "Game Engine") char-width))
	      (+ (round (/ tamias:screen-height 4)) char-height 2 100)
	      "Game Engine")

(states:def-logic (splash-screen:splash-screen idle)
		  (incf splash-screen:splash-ticks)
		  (when (>= splash-screen:splash-ticks (* splash-screen:splash-seconds tamias:fps))
		    (states:set-state main-menu init)))
