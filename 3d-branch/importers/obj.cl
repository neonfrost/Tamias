(defun parse-obj-vertex (ip-str)
  (let* ((spat-vert (make-array '(3) :initial-contents '(0.0 0.0 0.0)))
	 (sv-verts (subseq ip-str 2))
	 (sv-x (subseq sv-verts 0 (position #\space sv-verts)))
	 (sv-y (subseq sv-verts (1+ (length sv-x)) (position #\space sv-verts :start (1+ (length sv-x)))))
	 (sv-z (subseq sv-verts (+ (length sv-x) (length sv-y) 2))))
    (setf (aref spat-vert 0) (parse-float sv-x)
	  (aref spat-vert 1) (parse-float sv-y)
	  (aref spat-vert 2) (parse-float sv-z))
    spat-vert))

(defun parse-obj-uv (ip-str)
  (let* ((tex-vert (make-array '(2) :initial-contents '(0.0 0.0)))
	 (tv-verts (subseq ip-str 3))
	 (tv-u (subseq tv-verts 0 (position #\space tv-verts)))
	 (tv-v (subseq tv-verts (1+ (position #\space tv-verts)))))
    (setf (aref tex-vert 0) (parse-float tv-u)
	  (aref tex-vert 1) (parse-float tv-v))
    tex-vert))

(defun parse-obj-normal (ip-str)
  (let* ((normal-vert (make-array '(3) :initial-contents '(0.0 0.0 0.0)))
	 (nv-verts (subseq ip-str 3))
	 (nv-x (subseq nv-verts 0 (position #\space nv-verts)))
	 (nv-y (subseq nv-verts (1+ (length nv-x)) (position #\space nv-verts :start (1+ (length nv-x)))))
	 (nv-z (subseq nv-verts (+ (length nv-x) (length nv-y) 2))))
    (setf (aref normal-vert 0) (parse-float nv-x)
	  (aref normal-vert 1) (parse-float nv-y)
	  (aref normal-vert 2) (parse-float nv-z))
    normal-vert))

(defun parse-obj-face (ip-str &key (obj? t))
  (let* ((points-string (subseq ip-str (1+ (position #\space ip-str)))) ;;testing string: "104/262/200 141/216/197 326/263/198"
	 (polygon nil)
	 (points nil)
	 (current-point nil))
    (loop while t
       do (let ((current-point-string (subseq points-string
					      0
					      (or (position #\space points-string)
						  nil)))
		(current-vert nil)
		(verts nil))
	    (if (find #\space points-string)
		(setf points-string (subseq points-string
					    (1+ (position #\space points-string))))
		(setf points-string ""))
	    (setf current-point (make-point))
	    (if (find #\/ current-point-string)
		(progn (loop while t
			  do (setf current-vert (subseq current-point-string 0 (or (position #\/ current-point-string)
										   (length current-point-string))))
			    (if (find #\/ current-point-string)
				(setf current-point-string (subseq current-point-string (1+ (position #\/ current-point-string))))
				(setf current-point-string ""))
			    (if (> (length current-vert) 0)
				(if obj?
				    (push (1- (parse-integer current-vert)) verts)
				    (push (parse-integer current-vert) verts))
				(push 'nil verts))
			    (if (eq (length current-point-string) 0)
				(return t)))
		       (setf (point-normal-index current-point) (car verts)
			     (point-texture-index current-point) (cadr verts)
			     (point-position-index current-point) (caddr verts)))
		(setf (point-position-index current-point) (parse-integer current-point-string)))
	    (push current-point points)
	    (if (eq (length points-string) 0)
		(return t))))
    (setf polygon (make-polygon :points (make-array (list (length points)) :initial-contents (reverse points))))
    polygon))

(defun load-obj (file-name)
  (let ((objs nil)
	(model (make-model))
	(model-info nil)
	(material-library nil)
	(current-object nil)
	(object-name nil)
	(spatial-verts nil)
	(texture-verts nil)
	(normal-verts nil)
	(object-material nil)
	(smoothing nil)
	(polygons nil))
    (with-open-file (ip-stream file-name)
      (loop for current-line = (read-line ip-stream nil)
	 do (if (not current-line)
		(return t))
	 ;;	   (princ current-line) ;;debug
	   (cond ((char= (aref current-line 0) #\#)
		  (push (subseq current-line 1) model-info))
		 ((char= (aref current-line 0) #\s)
		  (if (char= (aref current-line 2) #\1)
		      (setf smoothing t)
		      (setf smoothing nil)))
		 ((char= (aref current-line 0) #\m)
		  (setf material-library (subseq current-line (position #\space current-line))))
		 ((char= (aref current-line 0) #\v)
		  (cond ((char= (aref current-line 1) #\space)
			 ;;spatial verts
			 (push (parse-obj-vertex current-line) spatial-verts))
			((char= (aref current-line 1) #\t)
			 ;;texture verts
			 (push (parse-obj-uv current-line) texture-verts))
			((char= (aref current-line 1) #\n)
			 ;;normal verts
			 (push (parse-obj-normal current-line) normal-verts))))
		 ((char= (aref current-line 0) #\u)
		  (setf object-material (subseq current-line (position #\space current-line)))
		  (fresh-line))
		 ((char= (aref current-line 0) #\f)
		  (push (parse-obj-face current-line) polygons))
		 ((char= (aref current-line 0) #\o)
		  (if model-info
		      (if (listp model-info)
			  (let ((obj-info ""))
			    (loop for info in (reverse model-info)
			       do (setf obj-info (concatenate 'string obj-info info '(#\newline))))
			    (setf model-info obj-info))))
		  (if current-object
		      (progn (setf current-object (make-object :name object-name
							       :info model-info
							       :material object-material
							       :smoothing smoothing
							       :polygons (make-array (length polygons) :initial-contents (reverse polygons))))
			     (push current-object objs)
			     (setf current-object nil
				   object-material nil
				   object-name nil
				   smoothing nil
				   polygons nil)))
		  (setf current-object (make-object)
			object-name (subseq current-line (position #\space current-line)))))))
    (setf current-object (make-object :name object-name
				      :info model-info
				      :material object-material
				      :smoothing smoothing
				      :polygons (make-array (length polygons) :initial-contents (reverse polygons))))
    (push current-object objs)
    (let ((vertices (make-array (list (length spatial-verts))))
	  (normal-vertices (make-array (list (length normal-verts))))
	  (texture-vertices (make-array (list (length texture-verts))))
	  (spatial-verts (reverse spatial-verts))
	  (normal-verts (reverse normal-verts))
	  (texture-verts (reverse texture-verts)))
      (loop for index below (length spatial-verts)
	 for spat-vert in spatial-verts
	 do (setf (aref vertices index) (make-vertex))
	   (setf (vertex-values (aref vertices index)) spat-vert))
      (loop for index below (length normal-verts)
	 for norm-vert in normal-verts
	 do (setf (aref normal-vertices index) (make-vertex))
	   (setf (vertex-values (aref normal-vertices index)) norm-vert))
      (loop for index below (length texture-verts)
	 for tex-vert in texture-verts
	 do (setf (aref texture-vertices index) (make-vertex))
	   (setf (vertex-values (aref texture-vertices index)) tex-vert))
      (setf model (make-model :vertices vertices
			      :normal-vertices normal-vertices
			      :texture-vertices texture-vertices
			      :objects (reverse objs)))
      model)))
