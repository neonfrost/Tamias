26 units = 1 foot
52 - 2 feet
104 = 4 feet
156 = 6 feet

This is based on Nika, who is roughly 90 units tall in blender. "IRL" Nika is 3.5 feet tall


Everything should be normalized to 26 units = 1 foot

For the default camera, it will 'look straight at nika, with Nika standinga few feet from the camera'

As such, Nika, assuming an origin of 0 0 0, should not fill the camera from top to bottom or from left to right (such as when she turns)

Nika should take up 3/4 of the camera space, vertically.

To achieve this attempt, a plane that is 26x26x1 units will be created
Eventually, a cube will be put in the place of the plane to ensure accuracy of perspective transformation and unit measurements


11/18/21
Height looks correct at total of 26 units when camera Z pos is set to 3.5

