#|
Initialize OpenGL, initialize window, initialize audio, initialize engine

Make a new package that facilitates handling 3D drawing and math

Note: main function code adapted from sdl2-examples "basic.lisp"

Upon further development of the 3D packages of Tamias (which will be named Canis, the genus that the wolf belongs to), there will be two "branches" mantained
the 2D branch and the 3D branch
However, I will be reorganizing and redeveloping the code base so that it's "3D only", that is, the use of SDL2 primitives is either nonexistant, or will be minimized as much as possible
The goal will be to make it so that any 2D stuff can be done through the "3D branch"
Once everything is fixed, reorganized, etc. If you want to develop 3D stuff, load Canis, if you want to develop 2D games, load Tamias
Tamias will support 3D things without needing to conenct to or load Canis specific stuff, in particular 3D particles that cast light or Shaders
|#

(ql:quickload :cl-opengl)
(ql:quickload :sdl2)
(ql:quickload :sdl2-image)
(ql:quickload :png)
;;(ql:quickload :rtg-math)


;;So, the blender pose matrix may not be applying the parent transformation


#|
Not sure if it's somewhere else in these files, but note:
Enable sharpness, hue and saturation (i.e. TV settings) in the game
|#

(defvar tamias-3d-messages nil)
(load "lib/math-lib.cl")
(load "lib/math-gfx-lib.cl")
(load "lib/bone-math.cl")
(load "lib/lib.cl")
(load "lib/vao.cl")
(load "assets/plane.cl")
(load "assets/cube.cl")
(load "lib/drawing-lib.cl")
(load "3d-variables.cl")

(load "input/3d-base.cl")


(defun print-gl-projection-mat ()
  (let ((projection nil))
    (gl:matrix-mode :projection)
    (gl:push-matrix)
    (gl:ortho (+ -400 world-scale) (+ 400 world-scale) (+ -400 world-scale) (+ 400 world-scale) (+ -800 world-scale) (+ 800 world-scale))
    (setf projection (gl:get-float :projection-matrix))
    (print projection)
    (gl:pop-matrix)))

(defun test-opengl-draw ()
  (gl:clear :color-buffer :depth-buffer)
  (gl:flush)
  (if rotating
      (progn (incf (aref world-rotation 1) 1)
	     ))
	     ;;(incf (aref world-rotation 2) 1)))
  (if rotate-z
      (incf (aref world-rotation 2) 2))
  (loop for n below 3
     for inc in (list world-rotation-inc-x
		      world-rotation-inc-y
		      0)
     do (incf (aref world-rotation n) inc)
       (if (>= (aref world-rotation n) 360)
	   (setf (aref world-rotation n) 0)
	   (if (< (aref world-rotation n) 0)
	       (setf (aref world-rotation n) 360))))
  (setf world-rotation-inc-x 0
	world-rotation-inc-y 0)
#|  (gl:matrix-mode :projection)
  (gl:load-identity)
  (let ((asp (/ 300 400.0))) ;;asp = width/height
    (gl:frustum -1.0 1.0 (- asp) asp (+ 1.0 world-scale) 10000.0))
  (gl:matrix-mode :modelview)
  (gl:load-identity)|#
  (update-camera-pos)
  (let ((camera (camera-pos t-camera)))
    (setf (aref view-cube 0 3) (aref camera 0)
	  (aref view-cube 1 3) (aref camera 1)
	  (aref view-cube 2 3) (aref camera 2)))
  (draw-vaos plane plane-2 cube)
  )

#|
(defun set-nika-values ()
  (setf (model-scale-scalar alt-nika) 10.0)
  (setf model-key 0
	(model-key alt-nika) model-key
	(model-key nika) model-key
	(model-key nika-roller) model-key)
  
  (setf (model-x alt-nika) 20.0
	(model-x nika-roller) -20.0
	(model-x nika) -20.0)
  
  (setf (model-z test-tower) 90)
  (setf (model-scale-scalar test-tower) 4.0)
  (setf (model-rotation-x test-tower) 0.0)
  (setf (model-x socra) 60)
  (setf (model-key nika) 0))
|#
;;(set-nika-values)


(defun main ()
  (sdl2:with-init (:everything)
    (format t "Using SDL Library Version: ~D.~D.~D~%"
            sdl2-ffi:+sdl-major-version+
            sdl2-ffi:+sdl-minor-version+
            sdl2-ffi:+sdl-patchlevel+)
    (finish-output)
;;    (sdl2:gl-set-attr :doublebuffer 1)
    (sdl2:with-window (win :flags '(:shown :opengl))
      (sdl2:with-gl-context (gl-context win)
	#|        (let ((controllers ())
	(haptic ()))|#
	;; basic window/gl setup
	(format t "Setting up window/gl.~%")
	(finish-output)
	(sdl2:gl-make-current win gl-context)
	(sdl2:set-window-size win 700 700)
	(gl:viewport 0 0 700 700)
;	(gl:shade-model :smooth)
	(gl:matrix-mode :projection)
	(gl:load-identity)
	(gl:ortho -200 200 -200 200 -200 200)
	;;	(gl:cull-face :back)
	(gl:light :light0 :position '(0.0 0.0 -50.0 1.0))
	(gl:light :light0 :diffuse (vector light-r light-g light-b 1))
;;	(gl:light-model :light-model-ambient '(.5 .5 .5 1.0))
;;	(gl:light-model :light-model-local-viewer 1)
	(gl:enable :normalize :depth-test :lighting :light0)
	(gl:enable :texture-2d)
	(gl:enable :blend)
;;	(gl:disable :dither)
	(gl:polygon-mode :front-and-back :fill)
	(gl:matrix-mode :modelview)
	(gl:load-identity)
	(gl:clear-color 0.2 0.2 0.2 1.0)
	(gl:clear :color-buffer :depth-buffer)
	(gl:front-face :cw)

	#|          (format t "Opening game controllers.~%")
	(finish-output)
	;; open any game controllers
	(loop :for i :upto (- (sdl2:joystick-count) 1)
	:do (when (sdl2:game-controller-p i)
	(format t "Found gamecontroller: ~a~%"
	(sdl2:game-controller-name-for-index i))
	(let* ((gc (sdl2:game-controller-open i))
	(joy (sdl2:game-controller-get-joystick gc)))
	(setf controllers (acons i gc controllers))
	(when (sdl2:joystick-is-haptic-p joy)
	(let ((h (sdl2:haptic-open-from-joystick joy)))
	(setf haptic (acons i h haptic))
	(sdl2:rumble-init h))))))|#

	;; main loop

	(format t "Beginning main loop.~%")
	(finish-output)
	(let ((vs (gl:create-shader :vertex-shader))
	      (fs (gl:create-shader :fragment-shader))
	      (shader-prog nil))
	  (gl:shader-source vs *vert-shader*)
	  (gl:compile-shader vs)
	  (gl:shader-source fs *frag-shader*)
	  (gl:compile-shader fs)
	  ;; If the shader doesn't compile, you can print errors with:
	  (print (gl:get-shader-info-log vs))
	  (print (gl:get-shader-info-log fs))
	  
	  (setf shader-prog (gl:create-program))
	  ;; You can attach the same shader to multiple different programs.
	  (gl:attach-shader shader-prog vs)
	  (gl:attach-shader shader-prog fs)
	  (gl:bind-attrib-location shader-prog 0 "vertexPosition_modelspace")
	  ;; Don't forget to link the program after attaching the
	  ;; shaders. This step actually puts the attached shader together
	  ;; to form the program.
	  (gl:link-program shader-prog)
	  ;; If we want to render using this program object, or add
	  ;; uniforms, we need to use the program. This is similar to
	  ;; binding a buffer.
	  (gl:use-program shader-prog)
	  (create-spatial-vao plane)
	  (create-spatial-vao plane-2)
	  (create-spatial-vao cube)
;;	  (sdl2:hide-cursor)
          (sdl2:with-event-loop (:method :poll)
            (:keydown (:keysym keysym)
                      (let ((scancode (sdl2:scancode-value keysym))
                            ;;(sym (sdl2:sym-value keysym))
                            ;;(mod-value (sdl2:mod-value keysym))
			    )
			(key-input scancode)
			#|                        (cond
			((sdl2:scancode= scancode :scancode-w) (format t "~a~%" "WALK"))
			((sdl2:scancode= scancode :scancode-s) (sdl2:show-cursor))
			((sdl2:scancode= scancode :scancode-h) (sdl2:hide-cursor)))
			
                        (format t "Key sym: ~a, code: ~a, mod: ~a~%"
                                sym
                                scancode
                                mod-value)))|#
			))
            (:keyup (:keysym keysym)
                    (when (sdl2:scancode= (sdl2:scancode-value keysym) :scancode-escape)
                      (sdl2:push-event :quit)))
            (:mousemotion (:x x :y y :xrel xrel :yrel yrel :state state)
			  (if (eq state 4)
			      (incf world-scale (/ yrel -10.0)))
			  (if (< world-scale 0.05)
			      (setf world-scale 0.05))
			  (if (eq state 2)
			      (progn (decf (aref (camera-rot t-camera) 0) yrel)
				     (incf (aref (camera-rot t-camera) 1) xrel)
				     (if (> (aref (camera-rot t-camera) 0) 180)
					 (setf (aref (camera-rot t-camera) 0) 180))
				     (if (< (aref (camera-rot t-camera) 0) 0)
					 (setf (aref (camera-rot t-camera) 0) 0))))
;;			  (if move-socra?
;;			      (incf (model-x socra) (/ xrel 5.0)))
			  
;;                          (format t "Mouse motion abs(rel): ~a (~a), ~a (~a)~%Mouse state: ~a~%"
;;				  x xrel y yrel state)
			  )
	    #|            (:controlleraxismotion
	    (:which controller-id :axis axis-id :value value)
	    (format t "Controller axis motion: Controller: ~a, Axis: ~a, Value: ~a~%"
	    controller-id axis-id value))

            (:controllerbuttondown (:which controller-id)
	    (let ((h (cdr (assoc controller-id haptic))))
	    (when h
	    (sdl2:rumble-play h 1.0 100))))
	    |#
            (:idle ()
		   (test-opengl-draw)
		   (sdl2:gl-swap-window win)
		   (incf garbage-timer)
		   (if (> garbage-timer 1000)
		       (progn (setf garbage-timer 0)
			      (gc :full t)))
		   (sdl2:delay 30)
		   )
            (:quit ()
		   (gl:delete-shader vs)
		   (gl:delete-shader fs)
		   (gl:delete-program shader-prog)
		   t)))
	(format t "Closing opened game controllers.~%")
	(finish-output)))))


(defun print-messages ()
  (loop for msg in (reverse tamias-3d-messages)
     do (princ msg)
       (fresh-line)))
(print-messages)

