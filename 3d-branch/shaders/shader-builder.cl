"So, I'm not sure how to make it so that you can use multiple shaders as a source.
However, I do know how to do it on the CL end."

"....................
#include file.glsl"

#|

So, building a shader is fairly simple

WHat we want to do is have it so that we can combine multiple sources and use any functions defined in 'other' shaders into the 'current' shader

This allows code reuse without copy+pasting

A shader in the cl end of things should be constructed as so:

(Shader name)
(shader-var "blah")
(shader-dependencies '(x y z))
(shader-main "main() { ...}"

and oh boy, we're getting freaky with it now

We'll need to build functions for each shader
So, each shader assumes that all input data is equal

essentially:
(shader-lisp-source) -> (shader-analyzer) -> (vao-function-builder)

Vao-function-builder will more than likely use methods to facilitate this.

|#

"
SHADER INFRASTRUCTURE:

SHADER PROGRAM:
:VERTEX SHADER
:GEOMETRY SHADER
:FRAG SHADER

"

"Some of the necessary infrastructure is already done in shaders.cl, primarily the tamias-shader"

(defgeneric vao-program (object program-id)
  (:method (object program-id)
    nil))

"Body will need to be made programatically"

"And I need some more helper functions"
"I need to separate the vao-shader-builder into about 4 separate functions"
"First is building the vertex shader, fragment shader, and any other shaders"
"Second, for each type of shader, they need to be binded to a program. This program is what will need to be reference in vao-shader-builder, not the shader ID"
"I have the images of how this is to work in my head, but it's difficult to explain it."
(defmacro vao-shader-builder (target-object shader-name shader-type shader-source &body body)
  `(let ((shader (create-shader ,shader-name ,shader-type ,shader-source))
	 (shader-program-id 0))
     (defmethod vao-program (object (program-id (eql shader-program-id)))
       ,@body
       )))

(defun render.model-group (model-group)
  (let ((num-objects (length (model-objects (car model-group)))))
    (loop for obj below num-objects
       do (loop for model in model-group
	     do (let ((object (elt obj (model-objects model))))
		  (vao-shader object (object-shader-program object)))
	       ))))

(defgeneric shader-parse (parse-type command-list)
  (:method (parse-type command-list)
    nil))

(defmacro cl-key->glsl (cl-key)
  `(string-downcase (write-to-string ,cl-key)))

(defmacro defvars (var-vals)
  `(loop for pair in ,var-vals
      do (let ((name (car pair))
	       (value (cadr pair)))
	   (eval `(defvar ,name ,value)))))
	

(defvars '((version 'version) (include 'include)
	   (shader-include 'shader-include) (shader-function 'shader-function)
	   (shader-variable 'shader-variable) (shader-version 'shader-version)))




(defmethod shader-parse ((parse-type (eql shader-variable)) command-list)
  (let ((shader-type (cadr command-list))
	(shader-text (caddr command-list)))
    (setf shader-type (subseq (cl-key->glsl (cadr command-list))
			      1))
    (loop while (find #\- shader-type)
       do (setf (aref shader-type (position #\- shader-type))
		#\space))
    (if (not (find #\; shader-text))
	(tamias.string:combine-text shader-type " " shader-text ";" #\newline)
	(tamias.string:combine-text shader-type " " shader-text #\newline))))

(defmethod shader-parse ((parse-type (eql 'variable)) command-list)
  (shader-parse shader-variable command-list))


(defmethod shader-parse ((parse-type (eql 'function))
			 command-list)
  (let ((ret-type nil))
    (if (or (eq (cadr command-list) :nil)
	    (eq (cadr command-list) nil)
	    (eq (cadr command-list) :void)
	    (eq (cadr command-list) 'void))
	(setf ret-type "void")
	(subseq (cl-key->glsl (cadr command-list)) 1))
    (tamias.string:combine-text ret-type " " (caddr command-list))))

(defmethod shader-parse ((parse-type (eql shader-function)) command-list)
  (shader-parse 'function command-list))


(defmethod shader-parse ((parse-type (eql include))
			 command-list)
  (get-shader-string (cadr command-list)))
   

(defmethod shader-parse ((parse-type (eql version)) 
			 command-list)
  (tamias.string:combine-text "#version " (cadr command-list) #\newline))

(defmethod shader-parse ((parse-type (eql shader-version))
			 command-list)
  (tamias.string:combine-text "#version " (cadr command-list) #\newline))
			 
			 
(defun shader-parser (shader-commands)
  (let ((return-str "")
	(variables nil))
    (loop for command-list in shader-commands
       do
	 (setf return-str (tamias.string:combine-text return-str
						     (shader-parse (car command-list) command-list))))
    (values return-str variables)))
	 

The image in my head:

'((shader-name 'shader-name)
  (shader-version "1.3")
  (shader-include 'shader-name-0)
  (shader-variable :in-vec3 "vertexPosition")
  (shader-variable :uniform-mat4 "bone_matrices" src)
  (shader-variable :uniform-vec3 "translate_vec" src)
  (shader-variable :varying-vec3 "camera" src)
  (shader-variable :vec3 "dingus bingus")
  (shader-function :nil "main () { gl_Position = vec4(translate_vec, 1.0); }"))
GL_ID = 8 (example only)
|
V
"#version 1.3

in vec3 vertexPosition;
in normal;
in uv;
in weights;
in bone_indices;
uniform vec3 rotation_vec;
uniform vec3 translate_vec;
uniform mat4 perspective_mat;

main (){ gl_Position = vec4(translate_vec, 1.0);
}"
|
V
{

(loop for objects in (model-object model)
     (load-program [PROGRAM_0])
     (gl:uniform-matrix  (gl:get-uniform-location shader-prog "bone_matrices") (* 4 (length (bone-matrices model)))  (vector (bone-matrices model)))
     (gl:uniformfv (gl:get-uniform-location shader-prog "translate_vec") (vec3+vec3 (model-position model) camera))
     (gl:uniformfv (gl:get-uniform-location shader-prog "camera") (vector camera))
     (gl:bind-vertex-array (object-vao object))
     (%gl:draw-elements :triangles (object-idx-length object) :unsigned-short 0)
     (gl:bind-vertex-array 0)
     (load-program [Program_1])
     [PROGRAM VARIABLES]
     (gl:bind-vertex-array (object-vao object))
     (%gl:draw-elements :triangles (object-idx-length object) :unsigned-short 0)
     (gl:bind-vertex-array 0))     

}

