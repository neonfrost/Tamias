(defsystem :MING
  :description "MING is a 'galaga'-like game. MING stands for 'Ming Is Not Galaga'."
  :version "0.2.5"
  :author "Brandon Blundell"
  :license "GPL v2"
  :default-component-class cl-source-file.cl
  :serial t
  ;;:depends-on (:tamias-gui :tamias-sdl2) ;;only include when these *aren't* loaded in
  :components ((:module "MING"
		:serial t
		:components
		((:file "lib.MING")
		 (:file "splash")
		 (:file "main-menu.init")
		 (:file "main-menu.logic")
	 	 (:file "main-menu.help")
		 (:file "init.level")
		 (:file "level.bullet")
		 (:file "level.lib.ship")
		 (:file "level.fire")
		 (:file "level.gen-entity")
		 (:file "level.spawn")
		 (:file "level.logic")
		 (:file "level.pause")
		 (:file "lib.player")
		 (:file "lib.type-a")
		 (:file "lib.type-b")
		 (:file "lib.type-c")
		 (:file "lib.type-d")
		 (:file "kb")
		 (:file "credits")))))
